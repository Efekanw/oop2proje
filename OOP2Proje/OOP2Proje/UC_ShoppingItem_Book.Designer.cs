﻿
namespace OOP2Proje
{
    partial class UC_ShoppingItem_Book
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblBookCamo = new System.Windows.Forms.Label();
            this.LblBookCPrice = new System.Windows.Forms.Label();
            this.LblBookCCount = new System.Windows.Forms.Label();
            this.LblBookCUP = new System.Windows.Forms.Label();
            this.LblBookCTot = new System.Windows.Forms.Label();
            this.LblBookCbirim = new System.Windows.Forms.Label();
            this.BtnBookCAdd = new System.Windows.Forms.Button();
            this.BtnBookCRemove = new System.Windows.Forms.Button();
            this.LblCartBookName = new System.Windows.Forms.Label();
            this.BtnBookCDelete = new System.Windows.Forms.Button();
            this.PBBookC = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBBookC)).BeginInit();
            this.SuspendLayout();
            // 
            // LblBookCamo
            // 
            this.LblBookCamo.AutoSize = true;
            this.LblBookCamo.BackColor = System.Drawing.Color.Transparent;
            this.LblBookCamo.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCamo.ForeColor = System.Drawing.Color.Black;
            this.LblBookCamo.Location = new System.Drawing.Point(155, 63);
            this.LblBookCamo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCamo.Name = "LblBookCamo";
            this.LblBookCamo.Size = new System.Drawing.Size(41, 20);
            this.LblBookCamo.TabIndex = 1;
            this.LblBookCamo.Text = "Adet";
            // 
            // LblBookCPrice
            // 
            this.LblBookCPrice.AutoSize = true;
            this.LblBookCPrice.BackColor = System.Drawing.Color.Transparent;
            this.LblBookCPrice.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCPrice.ForeColor = System.Drawing.Color.Black;
            this.LblBookCPrice.Location = new System.Drawing.Point(366, 63);
            this.LblBookCPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCPrice.Name = "LblBookCPrice";
            this.LblBookCPrice.Size = new System.Drawing.Size(96, 20);
            this.LblBookCPrice.TabIndex = 2;
            this.LblBookCPrice.Text = "Toplam Fiyat";
            // 
            // LblBookCCount
            // 
            this.LblBookCCount.BackColor = System.Drawing.Color.Transparent;
            this.LblBookCCount.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCCount.ForeColor = System.Drawing.Color.Black;
            this.LblBookCCount.Location = new System.Drawing.Point(155, 86);
            this.LblBookCCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCCount.Name = "LblBookCCount";
            this.LblBookCCount.Size = new System.Drawing.Size(36, 30);
            this.LblBookCCount.TabIndex = 3;
            this.LblBookCCount.Text = "0";
            this.LblBookCCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblBookCUP
            // 
            this.LblBookCUP.AutoSize = true;
            this.LblBookCUP.BackColor = System.Drawing.Color.Transparent;
            this.LblBookCUP.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCUP.ForeColor = System.Drawing.Color.Black;
            this.LblBookCUP.Location = new System.Drawing.Point(257, 63);
            this.LblBookCUP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCUP.Name = "LblBookCUP";
            this.LblBookCUP.Size = new System.Drawing.Size(86, 20);
            this.LblBookCUP.TabIndex = 4;
            this.LblBookCUP.Text = "Birim Fiyatı";
            // 
            // LblBookCTot
            // 
            this.LblBookCTot.BackColor = System.Drawing.Color.Transparent;
            this.LblBookCTot.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCTot.ForeColor = System.Drawing.Color.Black;
            this.LblBookCTot.Location = new System.Drawing.Point(370, 93);
            this.LblBookCTot.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCTot.Name = "LblBookCTot";
            this.LblBookCTot.Size = new System.Drawing.Size(92, 20);
            this.LblBookCTot.TabIndex = 5;
            this.LblBookCTot.Text = "0";
            this.LblBookCTot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblBookCbirim
            // 
            this.LblBookCbirim.BackColor = System.Drawing.Color.Transparent;
            this.LblBookCbirim.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCbirim.ForeColor = System.Drawing.Color.Black;
            this.LblBookCbirim.Location = new System.Drawing.Point(261, 93);
            this.LblBookCbirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCbirim.Name = "LblBookCbirim";
            this.LblBookCbirim.Size = new System.Drawing.Size(82, 20);
            this.LblBookCbirim.TabIndex = 6;
            this.LblBookCbirim.Text = "0";
            this.LblBookCbirim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnBookCAdd
            // 
            this.BtnBookCAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBookCAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookCAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnBookCAdd.Location = new System.Drawing.Point(195, 86);
            this.BtnBookCAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnBookCAdd.Name = "BtnBookCAdd";
            this.BtnBookCAdd.Size = new System.Drawing.Size(30, 30);
            this.BtnBookCAdd.TabIndex = 7;
            this.BtnBookCAdd.Text = "+";
            this.BtnBookCAdd.UseVisualStyleBackColor = true;
            this.BtnBookCAdd.Click += new System.EventHandler(this.BtnBookCAdd_Click);
            // 
            // BtnBookCRemove
            // 
            this.BtnBookCRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBookCRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookCRemove.ForeColor = System.Drawing.Color.Red;
            this.BtnBookCRemove.Location = new System.Drawing.Point(121, 86);
            this.BtnBookCRemove.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnBookCRemove.Name = "BtnBookCRemove";
            this.BtnBookCRemove.Size = new System.Drawing.Size(30, 30);
            this.BtnBookCRemove.TabIndex = 8;
            this.BtnBookCRemove.Text = "-";
            this.BtnBookCRemove.UseVisualStyleBackColor = true;
            this.BtnBookCRemove.Click += new System.EventHandler(this.BtnBookCRemove_Click);
            // 
            // LblCartBookName
            // 
            this.LblCartBookName.AutoEllipsis = true;
            this.LblCartBookName.BackColor = System.Drawing.Color.Transparent;
            this.LblCartBookName.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblCartBookName.ForeColor = System.Drawing.Color.Black;
            this.LblCartBookName.Location = new System.Drawing.Point(121, 0);
            this.LblCartBookName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblCartBookName.Name = "LblCartBookName";
            this.LblCartBookName.Size = new System.Drawing.Size(383, 63);
            this.LblCartBookName.TabIndex = 10;
            this.LblCartBookName.Text = "Kitap";
            this.LblCartBookName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnBookCDelete
            // 
            this.BtnBookCDelete.BackgroundImage = global::OOP2Proje.Properties.Resources.trash_book;
            this.BtnBookCDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnBookCDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnBookCDelete.FlatAppearance.BorderSize = 0;
            this.BtnBookCDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBookCDelete.ForeColor = System.Drawing.Color.Bisque;
            this.BtnBookCDelete.Location = new System.Drawing.Point(474, 98);
            this.BtnBookCDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnBookCDelete.Name = "BtnBookCDelete";
            this.BtnBookCDelete.Size = new System.Drawing.Size(30, 32);
            this.BtnBookCDelete.TabIndex = 9;
            this.BtnBookCDelete.UseVisualStyleBackColor = true;
            this.BtnBookCDelete.Click += new System.EventHandler(this.BtnBookCDelete_Click);
            // 
            // PBBookC
            // 
            this.PBBookC.ErrorImage = null;
            this.PBBookC.Location = new System.Drawing.Point(16, 16);
            this.PBBookC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PBBookC.Name = "PBBookC";
            this.PBBookC.Size = new System.Drawing.Size(92, 100);
            this.PBBookC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBBookC.TabIndex = 0;
            this.PBBookC.TabStop = false;
            // 
            // UC_ShoppingItem_Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.LblCartBookName);
            this.Controls.Add(this.BtnBookCDelete);
            this.Controls.Add(this.BtnBookCRemove);
            this.Controls.Add(this.BtnBookCAdd);
            this.Controls.Add(this.LblBookCbirim);
            this.Controls.Add(this.LblBookCTot);
            this.Controls.Add(this.LblBookCUP);
            this.Controls.Add(this.LblBookCCount);
            this.Controls.Add(this.LblBookCPrice);
            this.Controls.Add(this.LblBookCamo);
            this.Controls.Add(this.PBBookC);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.Name = "UC_ShoppingItem_Book";
            this.Size = new System.Drawing.Size(506, 132);
            ((System.ComponentModel.ISupportInitialize)(this.PBBookC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBBookC;
        private System.Windows.Forms.Label LblBookCamo;
        private System.Windows.Forms.Label LblBookCPrice;
        private System.Windows.Forms.Label LblBookCCount;
        private System.Windows.Forms.Label LblBookCUP;
        private System.Windows.Forms.Label LblBookCTot;
        private System.Windows.Forms.Label LblBookCbirim;
        private System.Windows.Forms.Button BtnBookCAdd;
        private System.Windows.Forms.Button BtnBookCRemove;
        private System.Windows.Forms.Button BtnBookCDelete;
        private System.Windows.Forms.Label LblCartBookName;
    }
}
