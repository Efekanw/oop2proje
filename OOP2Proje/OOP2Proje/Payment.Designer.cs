﻿
namespace OOP2Proje
{
    partial class Payment
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Payment));
            this.Btn_order_placed = new System.Windows.Forms.Button();
            this.BtnBack = new System.Windows.Forms.Button();
            this.FLPaid = new System.Windows.Forms.FlowLayoutPanel();
            this.FLnotPaid = new System.Windows.Forms.FlowLayoutPanel();
            this.FLPaidItems = new System.Windows.Forms.FlowLayoutPanel();
            this.PayTypeGrpBX = new System.Windows.Forms.GroupBox();
            this.cvv_txtBX = new System.Windows.Forms.MaskedTextBox();
            this.cvv_label = new System.Windows.Forms.Label();
            this.credit_no_label = new System.Windows.Forms.Label();
            this.credit_no_txtBX = new System.Windows.Forms.MaskedTextBox();
            this.pay_cash_rdBT = new System.Windows.Forms.RadioButton();
            this.pay_credit_rdBT = new System.Windows.Forms.RadioButton();
            this.invoiceTypeGrpBX = new System.Windows.Forms.GroupBox();
            this.payTel_txtBX = new System.Windows.Forms.MaskedTextBox();
            this.mail_radioBT = new System.Windows.Forms.RadioButton();
            this.sms_radioBT = new System.Windows.Forms.RadioButton();
            this.timer_payment = new System.Windows.Forms.Timer(this.components);
            this.Lbl_PaymentDate = new System.Windows.Forms.Label();
            this.Lbl_PaymentTimer = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.PayTypeGrpBX.SuspendLayout();
            this.invoiceTypeGrpBX.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // Btn_order_placed
            // 
            this.Btn_order_placed.BackColor = System.Drawing.Color.LemonChiffon;
            this.Btn_order_placed.Font = new System.Drawing.Font("Palatino Linotype", 13.74545F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Btn_order_placed.ForeColor = System.Drawing.Color.Teal;
            this.Btn_order_placed.Location = new System.Drawing.Point(1276, 653);
            this.Btn_order_placed.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Btn_order_placed.Name = "Btn_order_placed";
            this.Btn_order_placed.Size = new System.Drawing.Size(236, 75);
            this.Btn_order_placed.TabIndex = 0;
            this.Btn_order_placed.Text = "Ödeme Yap";
            this.Btn_order_placed.UseVisualStyleBackColor = false;
            this.Btn_order_placed.Click += new System.EventHandler(this.Btn_order_placed_Click);
            // 
            // BtnBack
            // 
            this.BtnBack.BackColor = System.Drawing.Color.LemonChiffon;
            this.BtnBack.Font = new System.Drawing.Font("Palatino Linotype", 13.74545F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBack.ForeColor = System.Drawing.Color.Teal;
            this.BtnBack.Location = new System.Drawing.Point(47, 663);
            this.BtnBack.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(256, 75);
            this.BtnBack.TabIndex = 1;
            this.BtnBack.Text = "Sepete Dön";
            this.BtnBack.UseVisualStyleBackColor = false;
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // FLPaid
            // 
            this.FLPaid.AutoScroll = true;
            this.FLPaid.Location = new System.Drawing.Point(47, 38);
            this.FLPaid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FLPaid.Name = "FLPaid";
            this.FLPaid.Size = new System.Drawing.Size(421, 352);
            this.FLPaid.TabIndex = 2;
            // 
            // FLnotPaid
            // 
            this.FLnotPaid.AutoScroll = true;
            this.FLnotPaid.Location = new System.Drawing.Point(940, 38);
            this.FLnotPaid.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FLnotPaid.Name = "FLnotPaid";
            this.FLnotPaid.Size = new System.Drawing.Size(572, 550);
            this.FLnotPaid.TabIndex = 3;
            // 
            // FLPaidItems
            // 
            this.FLPaidItems.AutoScroll = true;
            this.FLPaidItems.Location = new System.Drawing.Point(501, 38);
            this.FLPaidItems.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.FLPaidItems.Name = "FLPaidItems";
            this.FLPaidItems.Size = new System.Drawing.Size(411, 352);
            this.FLPaidItems.TabIndex = 4;
            // 
            // PayTypeGrpBX
            // 
            this.PayTypeGrpBX.Controls.Add(this.cvv_txtBX);
            this.PayTypeGrpBX.Controls.Add(this.cvv_label);
            this.PayTypeGrpBX.Controls.Add(this.credit_no_label);
            this.PayTypeGrpBX.Controls.Add(this.credit_no_txtBX);
            this.PayTypeGrpBX.Controls.Add(this.pay_cash_rdBT);
            this.PayTypeGrpBX.Controls.Add(this.pay_credit_rdBT);
            this.PayTypeGrpBX.Font = new System.Drawing.Font("Palatino Linotype", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.PayTypeGrpBX.ForeColor = System.Drawing.SystemColors.Info;
            this.PayTypeGrpBX.Location = new System.Drawing.Point(47, 396);
            this.PayTypeGrpBX.Margin = new System.Windows.Forms.Padding(4);
            this.PayTypeGrpBX.Name = "PayTypeGrpBX";
            this.PayTypeGrpBX.Padding = new System.Windows.Forms.Padding(4);
            this.PayTypeGrpBX.Size = new System.Drawing.Size(421, 250);
            this.PayTypeGrpBX.TabIndex = 5;
            this.PayTypeGrpBX.TabStop = false;
            this.PayTypeGrpBX.Text = "Ödeme Türünü Seçiniz";
            // 
            // cvv_txtBX
            // 
            this.cvv_txtBX.Location = new System.Drawing.Point(40, 155);
            this.cvv_txtBX.Margin = new System.Windows.Forms.Padding(4);
            this.cvv_txtBX.Mask = "000";
            this.cvv_txtBX.Name = "cvv_txtBX";
            this.cvv_txtBX.Size = new System.Drawing.Size(51, 33);
            this.cvv_txtBX.TabIndex = 5;
            this.cvv_txtBX.Visible = false;
            // 
            // cvv_label
            // 
            this.cvv_label.AutoSize = true;
            this.cvv_label.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.cvv_label.Location = new System.Drawing.Point(36, 129);
            this.cvv_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.cvv_label.Name = "cvv_label";
            this.cvv_label.Size = new System.Drawing.Size(48, 23);
            this.cvv_label.TabIndex = 4;
            this.cvv_label.Text = "CVV";
            this.cvv_label.Visible = false;
            // 
            // credit_no_label
            // 
            this.credit_no_label.AutoSize = true;
            this.credit_no_label.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.credit_no_label.Location = new System.Drawing.Point(35, 65);
            this.credit_no_label.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.credit_no_label.Name = "credit_no_label";
            this.credit_no_label.Size = new System.Drawing.Size(157, 22);
            this.credit_no_label.TabIndex = 3;
            this.credit_no_label.Text = "Kredi Kartı Numarası";
            this.credit_no_label.Visible = false;
            // 
            // credit_no_txtBX
            // 
            this.credit_no_txtBX.Location = new System.Drawing.Point(39, 90);
            this.credit_no_txtBX.Margin = new System.Windows.Forms.Padding(4);
            this.credit_no_txtBX.Mask = "0000-0000-0000-0000";
            this.credit_no_txtBX.Name = "credit_no_txtBX";
            this.credit_no_txtBX.Size = new System.Drawing.Size(263, 33);
            this.credit_no_txtBX.TabIndex = 2;
            this.credit_no_txtBX.Visible = false;
            // 
            // pay_cash_rdBT
            // 
            this.pay_cash_rdBT.AutoSize = true;
            this.pay_cash_rdBT.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.pay_cash_rdBT.Location = new System.Drawing.Point(40, 129);
            this.pay_cash_rdBT.Margin = new System.Windows.Forms.Padding(4);
            this.pay_cash_rdBT.Name = "pay_cash_rdBT";
            this.pay_cash_rdBT.Size = new System.Drawing.Size(74, 27);
            this.pay_cash_rdBT.TabIndex = 1;
            this.pay_cash_rdBT.TabStop = true;
            this.pay_cash_rdBT.Text = "Nakit";
            this.pay_cash_rdBT.UseVisualStyleBackColor = true;
            this.pay_cash_rdBT.CheckedChanged += new System.EventHandler(this.pay_cash_rdBT_CheckedChanged);
            // 
            // pay_credit_rdBT
            // 
            this.pay_credit_rdBT.AutoSize = true;
            this.pay_credit_rdBT.Font = new System.Drawing.Font("Palatino Linotype", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.pay_credit_rdBT.Location = new System.Drawing.Point(40, 32);
            this.pay_credit_rdBT.Margin = new System.Windows.Forms.Padding(4);
            this.pay_credit_rdBT.Name = "pay_credit_rdBT";
            this.pay_credit_rdBT.Size = new System.Drawing.Size(123, 30);
            this.pay_credit_rdBT.TabIndex = 0;
            this.pay_credit_rdBT.TabStop = true;
            this.pay_credit_rdBT.Text = "Kredi Kartı";
            this.pay_credit_rdBT.UseVisualStyleBackColor = true;
            this.pay_credit_rdBT.CheckedChanged += new System.EventHandler(this.pay_credit_rdBT_CheckedChanged);
            // 
            // invoiceTypeGrpBX
            // 
            this.invoiceTypeGrpBX.Controls.Add(this.payTel_txtBX);
            this.invoiceTypeGrpBX.Controls.Add(this.mail_radioBT);
            this.invoiceTypeGrpBX.Controls.Add(this.sms_radioBT);
            this.invoiceTypeGrpBX.Font = new System.Drawing.Font("Palatino Linotype", 11.12727F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.invoiceTypeGrpBX.ForeColor = System.Drawing.SystemColors.Info;
            this.invoiceTypeGrpBX.Location = new System.Drawing.Point(501, 396);
            this.invoiceTypeGrpBX.Margin = new System.Windows.Forms.Padding(4);
            this.invoiceTypeGrpBX.Name = "invoiceTypeGrpBX";
            this.invoiceTypeGrpBX.Padding = new System.Windows.Forms.Padding(4);
            this.invoiceTypeGrpBX.Size = new System.Drawing.Size(411, 250);
            this.invoiceTypeGrpBX.TabIndex = 6;
            this.invoiceTypeGrpBX.TabStop = false;
            this.invoiceTypeGrpBX.Text = "Fatura Yöntemini Seçiniz";
            this.invoiceTypeGrpBX.Visible = false;
            // 
            // payTel_txtBX
            // 
            this.payTel_txtBX.Location = new System.Drawing.Point(40, 78);
            this.payTel_txtBX.Mask = "0(000) 000 00 00";
            this.payTel_txtBX.Name = "payTel_txtBX";
            this.payTel_txtBX.Size = new System.Drawing.Size(173, 33);
            this.payTel_txtBX.TabIndex = 9;
            // 
            // mail_radioBT
            // 
            this.mail_radioBT.AutoSize = true;
            this.mail_radioBT.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.mail_radioBT.Location = new System.Drawing.Point(40, 142);
            this.mail_radioBT.Margin = new System.Windows.Forms.Padding(4);
            this.mail_radioBT.Name = "mail_radioBT";
            this.mail_radioBT.Size = new System.Drawing.Size(80, 27);
            this.mail_radioBT.TabIndex = 1;
            this.mail_radioBT.TabStop = true;
            this.mail_radioBT.Text = "E-mail";
            this.mail_radioBT.UseVisualStyleBackColor = true;
            // 
            // sms_radioBT
            // 
            this.sms_radioBT.AutoSize = true;
            this.sms_radioBT.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.sms_radioBT.Location = new System.Drawing.Point(40, 32);
            this.sms_radioBT.Margin = new System.Windows.Forms.Padding(4);
            this.sms_radioBT.Name = "sms_radioBT";
            this.sms_radioBT.Size = new System.Drawing.Size(66, 27);
            this.sms_radioBT.TabIndex = 0;
            this.sms_radioBT.TabStop = true;
            this.sms_radioBT.Text = "SMS";
            this.sms_radioBT.UseVisualStyleBackColor = true;
            this.sms_radioBT.CheckedChanged += new System.EventHandler(this.sms_radioBT_CheckedChanged);
            // 
            // timer_payment
            // 
            this.timer_payment.Tick += new System.EventHandler(this.timer_payment_Tick);
            // 
            // Lbl_PaymentDate
            // 
            this.Lbl_PaymentDate.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Lbl_PaymentDate.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Lbl_PaymentDate.Location = new System.Drawing.Point(648, 10);
            this.Lbl_PaymentDate.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_PaymentDate.Name = "Lbl_PaymentDate";
            this.Lbl_PaymentDate.Size = new System.Drawing.Size(264, 28);
            this.Lbl_PaymentDate.TabIndex = 8;
            this.Lbl_PaymentDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // Lbl_PaymentTimer
            // 
            this.Lbl_PaymentTimer.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.Lbl_PaymentTimer.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.Lbl_PaymentTimer.Location = new System.Drawing.Point(1347, -2);
            this.Lbl_PaymentTimer.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.Lbl_PaymentTimer.Name = "Lbl_PaymentTimer";
            this.Lbl_PaymentTimer.Size = new System.Drawing.Size(221, 28);
            this.Lbl_PaymentTimer.TabIndex = 9;
            this.Lbl_PaymentTimer.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 11.78182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.ForeColor = System.Drawing.SystemColors.Info;
            this.label1.Location = new System.Drawing.Point(42, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(189, 27);
            this.label1.TabIndex = 10;
            this.label1.Text = "SİPARİŞ GEÇMİŞİ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Palatino Linotype", 11.78182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label2.ForeColor = System.Drawing.SystemColors.Info;
            this.label2.Location = new System.Drawing.Point(496, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 27);
            this.label2.TabIndex = 11;
            this.label2.Text = "ÜRÜNLER";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Palatino Linotype", 11.78182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label3.ForeColor = System.Drawing.SystemColors.Info;
            this.label3.Location = new System.Drawing.Point(935, 9);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(217, 27);
            this.label3.TabIndex = 12;
            this.label3.Text = "MEVCUT SEPETİNİZ";
            // 
            // pictureBox1
            // 
            this.pictureBox1.ErrorImage = null;
            this.pictureBox1.Image = global::OOP2Proje.Properties.Resources.delivery444;
            this.pictureBox1.Location = new System.Drawing.Point(940, 622);
            this.pictureBox1.Margin = new System.Windows.Forms.Padding(4);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(329, 148);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 7;
            this.pictureBox1.TabStop = false;
            // 
            // Payment
            // 
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.None;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.ClientSize = new System.Drawing.Size(1527, 783);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.Lbl_PaymentTimer);
            this.Controls.Add(this.Lbl_PaymentDate);
            this.Controls.Add(this.pictureBox1);
            this.Controls.Add(this.invoiceTypeGrpBX);
            this.Controls.Add(this.PayTypeGrpBX);
            this.Controls.Add(this.FLPaidItems);
            this.Controls.Add(this.FLnotPaid);
            this.Controls.Add(this.FLPaid);
            this.Controls.Add(this.BtnBack);
            this.Controls.Add(this.Btn_order_placed);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.MaximizeBox = false;
            this.Name = "Payment";
            this.Text = "Siparişlerim";
            this.Load += new System.EventHandler(this.Payment_Load);
            this.PayTypeGrpBX.ResumeLayout(false);
            this.PayTypeGrpBX.PerformLayout();
            this.invoiceTypeGrpBX.ResumeLayout(false);
            this.invoiceTypeGrpBX.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button Btn_order_placed;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.FlowLayoutPanel FLPaid;
        private System.Windows.Forms.FlowLayoutPanel FLnotPaid;
        private System.Windows.Forms.FlowLayoutPanel FLPaidItems;
        private System.Windows.Forms.GroupBox PayTypeGrpBX;
        private System.Windows.Forms.RadioButton pay_cash_rdBT;
        private System.Windows.Forms.RadioButton pay_credit_rdBT;
        private System.Windows.Forms.GroupBox invoiceTypeGrpBX;
        private System.Windows.Forms.RadioButton mail_radioBT;
        private System.Windows.Forms.RadioButton sms_radioBT;
        private System.Windows.Forms.MaskedTextBox cvv_txtBX;
        private System.Windows.Forms.Label cvv_label;
        private System.Windows.Forms.Label credit_no_label;
        private System.Windows.Forms.MaskedTextBox credit_no_txtBX;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Timer timer_payment;
        private System.Windows.Forms.Label Lbl_PaymentDate;
        private System.Windows.Forms.Label Lbl_PaymentTimer;
        private System.Windows.Forms.MaskedTextBox payTel_txtBX;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
    }
}