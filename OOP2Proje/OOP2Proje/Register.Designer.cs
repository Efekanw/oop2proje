﻿
namespace OOP2Proje
{
    partial class Register
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Register));
            this.txtBoxRegisterName = new System.Windows.Forms.TextBox();
            this.txtBoxRegisterAddress = new System.Windows.Forms.TextBox();
            this.txtBoxRegisterEmail = new System.Windows.Forms.TextBox();
            this.txtBoxRegisterUsername = new System.Windows.Forms.TextBox();
            this.txtBoxRegisterPassword = new System.Windows.Forms.TextBox();
            this.txtBoxRegisterConfirmPassword = new System.Windows.Forms.TextBox();
            this.lblRegisterTitle = new System.Windows.Forms.Label();
            this.lblRegisterName = new System.Windows.Forms.Label();
            this.lblRegisterUsername = new System.Windows.Forms.Label();
            this.lblRegisterEmail = new System.Windows.Forms.Label();
            this.lblRegisterAddress = new System.Windows.Forms.Label();
            this.lblRegisterPassword = new System.Windows.Forms.Label();
            this.lblRegisterConfirmPassword = new System.Windows.Forms.Label();
            this.btnRegister = new System.Windows.Forms.Button();
            this.lnkLblRegister = new System.Windows.Forms.LinkLabel();
            this.picBoxRegister = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRegister)).BeginInit();
            this.SuspendLayout();
            // 
            // txtBoxRegisterName
            // 
            this.txtBoxRegisterName.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxRegisterName.Location = new System.Drawing.Point(392, 187);
            this.txtBoxRegisterName.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxRegisterName.Name = "txtBoxRegisterName";
            this.txtBoxRegisterName.Size = new System.Drawing.Size(129, 26);
            this.txtBoxRegisterName.TabIndex = 0;
            // 
            // txtBoxRegisterAddress
            // 
            this.txtBoxRegisterAddress.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxRegisterAddress.Location = new System.Drawing.Point(392, 249);
            this.txtBoxRegisterAddress.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxRegisterAddress.Multiline = true;
            this.txtBoxRegisterAddress.Name = "txtBoxRegisterAddress";
            this.txtBoxRegisterAddress.Size = new System.Drawing.Size(129, 70);
            this.txtBoxRegisterAddress.TabIndex = 1;
            // 
            // txtBoxRegisterEmail
            // 
            this.txtBoxRegisterEmail.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxRegisterEmail.Location = new System.Drawing.Point(392, 354);
            this.txtBoxRegisterEmail.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxRegisterEmail.Multiline = true;
            this.txtBoxRegisterEmail.Name = "txtBoxRegisterEmail";
            this.txtBoxRegisterEmail.Size = new System.Drawing.Size(129, 52);
            this.txtBoxRegisterEmail.TabIndex = 2;
            // 
            // txtBoxRegisterUsername
            // 
            this.txtBoxRegisterUsername.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxRegisterUsername.Location = new System.Drawing.Point(392, 443);
            this.txtBoxRegisterUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxRegisterUsername.Name = "txtBoxRegisterUsername";
            this.txtBoxRegisterUsername.Size = new System.Drawing.Size(129, 26);
            this.txtBoxRegisterUsername.TabIndex = 3;
            // 
            // txtBoxRegisterPassword
            // 
            this.txtBoxRegisterPassword.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxRegisterPassword.Location = new System.Drawing.Point(392, 505);
            this.txtBoxRegisterPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxRegisterPassword.Name = "txtBoxRegisterPassword";
            this.txtBoxRegisterPassword.PasswordChar = '*';
            this.txtBoxRegisterPassword.Size = new System.Drawing.Size(129, 26);
            this.txtBoxRegisterPassword.TabIndex = 4;
            // 
            // txtBoxRegisterConfirmPassword
            // 
            this.txtBoxRegisterConfirmPassword.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxRegisterConfirmPassword.Location = new System.Drawing.Point(392, 565);
            this.txtBoxRegisterConfirmPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxRegisterConfirmPassword.Name = "txtBoxRegisterConfirmPassword";
            this.txtBoxRegisterConfirmPassword.PasswordChar = '*';
            this.txtBoxRegisterConfirmPassword.Size = new System.Drawing.Size(129, 26);
            this.txtBoxRegisterConfirmPassword.TabIndex = 5;
            // 
            // lblRegisterTitle
            // 
            this.lblRegisterTitle.AutoSize = true;
            this.lblRegisterTitle.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterTitle.Font = new System.Drawing.Font("Palatino Linotype", 26.25F);
            this.lblRegisterTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblRegisterTitle.Location = new System.Drawing.Point(269, 28);
            this.lblRegisterTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterTitle.Name = "lblRegisterTitle";
            this.lblRegisterTitle.Size = new System.Drawing.Size(394, 118);
            this.lblRegisterTitle.TabIndex = 6;
            this.lblRegisterTitle.Text = "SİGMA KİTABEVİ\r\n      KAYIT OL";
            // 
            // lblRegisterName
            // 
            this.lblRegisterName.AutoSize = true;
            this.lblRegisterName.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterName.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblRegisterName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblRegisterName.Location = new System.Drawing.Point(309, 186);
            this.lblRegisterName.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterName.Name = "lblRegisterName";
            this.lblRegisterName.Size = new System.Drawing.Size(59, 26);
            this.lblRegisterName.TabIndex = 7;
            this.lblRegisterName.Text = "İsim :";
            // 
            // lblRegisterUsername
            // 
            this.lblRegisterUsername.AutoSize = true;
            this.lblRegisterUsername.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterUsername.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblRegisterUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblRegisterUsername.Location = new System.Drawing.Point(233, 442);
            this.lblRegisterUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterUsername.Name = "lblRegisterUsername";
            this.lblRegisterUsername.Size = new System.Drawing.Size(129, 26);
            this.lblRegisterUsername.TabIndex = 8;
            this.lblRegisterUsername.Text = "Kullanıcı Adı :";
            // 
            // lblRegisterEmail
            // 
            this.lblRegisterEmail.AutoSize = true;
            this.lblRegisterEmail.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterEmail.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblRegisterEmail.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblRegisterEmail.Location = new System.Drawing.Point(297, 353);
            this.lblRegisterEmail.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterEmail.Name = "lblRegisterEmail";
            this.lblRegisterEmail.Size = new System.Drawing.Size(70, 26);
            this.lblRegisterEmail.TabIndex = 9;
            this.lblRegisterEmail.Text = "Email :";
            // 
            // lblRegisterAddress
            // 
            this.lblRegisterAddress.AutoSize = true;
            this.lblRegisterAddress.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterAddress.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblRegisterAddress.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblRegisterAddress.Location = new System.Drawing.Point(296, 247);
            this.lblRegisterAddress.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterAddress.Name = "lblRegisterAddress";
            this.lblRegisterAddress.Size = new System.Drawing.Size(72, 26);
            this.lblRegisterAddress.TabIndex = 10;
            this.lblRegisterAddress.Text = "Adres :";
            // 
            // lblRegisterPassword
            // 
            this.lblRegisterPassword.AutoSize = true;
            this.lblRegisterPassword.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterPassword.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblRegisterPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblRegisterPassword.Location = new System.Drawing.Point(307, 503);
            this.lblRegisterPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterPassword.Name = "lblRegisterPassword";
            this.lblRegisterPassword.Size = new System.Drawing.Size(59, 26);
            this.lblRegisterPassword.TabIndex = 11;
            this.lblRegisterPassword.Text = "Şifre :";
            // 
            // lblRegisterConfirmPassword
            // 
            this.lblRegisterConfirmPassword.AutoSize = true;
            this.lblRegisterConfirmPassword.BackColor = System.Drawing.Color.Transparent;
            this.lblRegisterConfirmPassword.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblRegisterConfirmPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblRegisterConfirmPassword.Location = new System.Drawing.Point(213, 564);
            this.lblRegisterConfirmPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblRegisterConfirmPassword.Name = "lblRegisterConfirmPassword";
            this.lblRegisterConfirmPassword.Size = new System.Drawing.Size(147, 26);
            this.lblRegisterConfirmPassword.TabIndex = 12;
            this.lblRegisterConfirmPassword.Text = "Şifreyi Doğrula :";
            // 
            // btnRegister
            // 
            this.btnRegister.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.btnRegister.ForeColor = System.Drawing.Color.Green;
            this.btnRegister.Location = new System.Drawing.Point(392, 626);
            this.btnRegister.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnRegister.Name = "btnRegister";
            this.btnRegister.Size = new System.Drawing.Size(131, 43);
            this.btnRegister.TabIndex = 13;
            this.btnRegister.Text = "Kayıt Ol";
            this.btnRegister.UseVisualStyleBackColor = true;
            this.btnRegister.Click += new System.EventHandler(this.btnRegister_Click);
            // 
            // lnkLblRegister
            // 
            this.lnkLblRegister.AutoSize = true;
            this.lnkLblRegister.BackColor = System.Drawing.Color.Transparent;
            this.lnkLblRegister.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lnkLblRegister.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lnkLblRegister.Location = new System.Drawing.Point(325, 694);
            this.lnkLblRegister.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lnkLblRegister.Name = "lnkLblRegister";
            this.lnkLblRegister.Size = new System.Drawing.Size(228, 26);
            this.lnkLblRegister.TabIndex = 14;
            this.lnkLblRegister.TabStop = true;
            this.lnkLblRegister.Text = "Zaten bir hesabın var mı ?";
            this.lnkLblRegister.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblRegister_LinkClicked);
            // 
            // picBoxRegister
            // 
            this.picBoxRegister.BackgroundImage = global::OOP2Proje.Properties.Resources.sigma;
            this.picBoxRegister.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBoxRegister.ErrorImage = null;
            this.picBoxRegister.InitialImage = null;
            this.picBoxRegister.Location = new System.Drawing.Point(87, 28);
            this.picBoxRegister.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.picBoxRegister.Name = "picBoxRegister";
            this.picBoxRegister.Size = new System.Drawing.Size(105, 116);
            this.picBoxRegister.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxRegister.TabIndex = 15;
            this.picBoxRegister.TabStop = false;
            // 
            // Register
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.ClientSize = new System.Drawing.Size(868, 720);
            this.Controls.Add(this.picBoxRegister);
            this.Controls.Add(this.lnkLblRegister);
            this.Controls.Add(this.btnRegister);
            this.Controls.Add(this.lblRegisterConfirmPassword);
            this.Controls.Add(this.lblRegisterPassword);
            this.Controls.Add(this.lblRegisterAddress);
            this.Controls.Add(this.lblRegisterEmail);
            this.Controls.Add(this.lblRegisterUsername);
            this.Controls.Add(this.lblRegisterName);
            this.Controls.Add(this.lblRegisterTitle);
            this.Controls.Add(this.txtBoxRegisterConfirmPassword);
            this.Controls.Add(this.txtBoxRegisterPassword);
            this.Controls.Add(this.txtBoxRegisterUsername);
            this.Controls.Add(this.txtBoxRegisterEmail);
            this.Controls.Add(this.txtBoxRegisterAddress);
            this.Controls.Add(this.txtBoxRegisterName);
            this.ForeColor = System.Drawing.Color.Black;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(886, 767);
            this.MinimumSize = new System.Drawing.Size(886, 767);
            this.Name = "Register";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Kayıt Ekranı";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Register_FormClosing);
            this.Load += new System.EventHandler(this.Register_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxRegister)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtBoxRegisterName;
        private System.Windows.Forms.TextBox txtBoxRegisterAddress;
        private System.Windows.Forms.TextBox txtBoxRegisterEmail;
        private System.Windows.Forms.TextBox txtBoxRegisterUsername;
        private System.Windows.Forms.TextBox txtBoxRegisterPassword;
        private System.Windows.Forms.TextBox txtBoxRegisterConfirmPassword;
        private System.Windows.Forms.Label lblRegisterTitle;
        private System.Windows.Forms.Label lblRegisterName;
        private System.Windows.Forms.Label lblRegisterUsername;
        private System.Windows.Forms.Label lblRegisterEmail;
        private System.Windows.Forms.Label lblRegisterAddress;
        private System.Windows.Forms.Label lblRegisterPassword;
        private System.Windows.Forms.Label lblRegisterConfirmPassword;
        private System.Windows.Forms.Button btnRegister;
        private System.Windows.Forms.LinkLabel lnkLblRegister;
        private System.Windows.Forms.PictureBox picBoxRegister;
    }
}