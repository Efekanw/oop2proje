﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 28.05.2021
*  @brief   : This class includes necessary purchasing item's functions.
*/
namespace OOP2Proje
{
    public class ItemToPurchase
    {
        private Product product;
        private int _quantity = 1;
        private int cartid;
        private string productType;
        /// <summary>
        /// getter setter for ItemToPurchase
        /// </summary>
        public Product Product
        {
            get { return this.product; }
            set
            {
                if (value is Book)
                {
                    product = new Book();
                    product = value;
                }
                else if (value is Magazine)
                {
                    product = new Magazine();
                    product = value;
                }
                else if (value is MusicCD)
                {
                    product = new MusicCD();
                    product = value;
                }
            }
        }
        /// <summary>
        /// Getter setter for quantity
        /// </summary>
        public int Quantity { get { return this._quantity; } set { this._quantity = value; } }
        /// <summary>
        /// Getter setter for Cart ID
        /// </summary>
        public int Cartid { get { return this.cartid; } set { this.cartid = value; } }
        /// <summary>
        /// Getter setter for Product Type
        /// </summary>
        public string ProductType { get { return this.productType; } set { this.productType = value; } }
    }
}
