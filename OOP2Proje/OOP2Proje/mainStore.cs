﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;
/*
 * @author: Efekan Sargın
 * @number: 152120181049
 * @mail: efekansarginn@hotmail.com
 * @date: 25.05.2021
 * @brief: This class includes mainStore's functions and events
 */
namespace OOP2Proje
{
    public partial class mainStore : Form
    {
        DataBase db = new DataBase();
        List<Product> bestproducts = new List<Product>();
        List<Product> products = new List<Product>();
        List<ShoppingCart> carts = new List<ShoppingCart>();
        List<ItemToPurchase> items = new List<ItemToPurchase>();
        ShoppingCart cart = new ShoppingCart();
        public static int cartid;
        /// <summary>
        /// Events used in filtering functions are started
        /// </summary>
        public mainStore()
        {
            InitializeComponent();
            timerMain.Start();
            BookFilterEvent();
            MagazineFilterEvent();
            MusicFilterEvent();
            BestSellers();
        }
        /// <summary>
        /// This main store load event gets customer's cart from Database or creates new one
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mainStore_Load(object sender, EventArgs e)
        {
            cart._cst_id = SingletonLoginedCustomer.getInstance().Customer.CustomerID;
            if (db.getCartsPaymentFromDB(0, cart._cst_id).Count > 0)
            {
                carts = db.getCartsPaymentFromDB(0, cart._cst_id);
                cart = carts[0];
                cartid = cart._cartid;
            }
            else
            {
                carts = db.getAllCarts();
                for (int t = 0; t < carts.Count + 1; t++)
                {
                    if (!db.checkCartid(t))
                    {
                        cartid = t;
                        cart._cartid = t;
                        db.addtoCartDB(cart);
                        return;
                    }
                }
            }
        }
        /// <summary>
        /// This function allows bestsellers to appear when the form is first opened
        /// </summary>
        private void BestSellers()
        {
            FlowLMain.Controls.Clear();
            bestproducts = db.initialScreen();
            while (bestproducts.Count > 0)
            {
                var random = new Random();
                int index = random.Next(bestproducts.Count);
                if (bestproducts[index] is Book)
                {
                    FlowLMain.Controls.Add(new UserControlBook((Book)bestproducts[index]));
                }
                else if (bestproducts[index] is Magazine)
                {
                    FlowLMain.Controls.Add(new UserControlMagazine((Magazine)bestproducts[index]));
                }
                else if (bestproducts[index] is MusicCD)
                {
                    FlowLMain.Controls.Add(new UserControlMusic((MusicCD)bestproducts[index]));
                }
                bestproducts.RemoveAt(index);
            }
        }
        /// <summary>
        /// This function filters books with book's filter buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="type"></param>
        private void FilterBooks(object sender, EventArgs e, string type)
        {
            FlowLMain.Controls.Clear();
            products.Clear();
            db.setBooksByType(type);
            products = db.getListofBooks();
            foreach (Book book in products)
            {
                FlowLMain.Controls.Add(new UserControlBook(book));
            }
        }
        /// <summary>
        /// This function filters magazines with magazine's filter buttons
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="type"></param>
        private void FilterMagazines(object sender, EventArgs e, string type)
        {
            FlowLMain.Controls.Clear();
            products.Clear();
            db.setMagazinesByType(type);
            products = db.getListofMagazines();
            foreach (Magazine mag in products)
            {
                FlowLMain.Controls.Add(new UserControlMagazine(mag));
            }
        }
        /// <summary>
        /// This function filters MusicCDs with MusicCD's filter buttons.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// <param name="type"></param>
        private void FilterMusics(object sender, EventArgs e, string type)
        {
            FlowLMain.Controls.Clear();
            products.Clear();
            db.setMusicByType(type);
            products = db.getListofMusicCD();
            foreach (MusicCD music in products)
            {
                FlowLMain.Controls.Add(new UserControlMusic(music));
            }
        }
        /// <summary>
        /// Sets the events of book's filter buttons
        /// </summary>
        private void BookFilterEvent()
        {
            BtnBookDT.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Dünya Tarihi"); };
            BtnBookDram.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Dram"); };
            BtnBookKorku.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Korku"); };
            BtnBookKG.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Kişisel Gelişim"); };
            BtnBookPolisiye.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Polisiye"); };
            BtnBookDistopya.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Distopya"); };
            BtnBookDK.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Dünya Klasikleri"); };
            BtnBookFelsefe.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Felsefe"); };
            BtnBookFantastik.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Fantastik"); };
            BtnBookBilim.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Bilim"); };
            BtnBookBK.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Bilim Kurgu"); };
            BtnBookCR.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Çizgi Roman"); };
            BtnBookRoman.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Roman"); };
            BtnBookCocuk.Click += delegate (object sender, EventArgs e) { FilterBooks(sender, e, "Çocuk"); };
        }
        /// <summary>
        /// Sets the events of magazine's filter buttons
        /// </summary>
        private void MagazineFilterEvent()
        {
            BtnMagBilim.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Bilim"); };
            BtnMagMizah.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Mizah"); };
            BtnMagCog.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Coğrafya"); };
            BtnMagPsiko.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Psikoloji"); };
            BtnMagBT.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Bilim&Teknoloji"); };
            BtnMagSpor.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Spor"); };
            BtnMagModa.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Moda"); };
            BtnMagEdebiyat.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Edebiyat"); };
            BtnMagCocuk.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Çocuk"); };
            BtnMagKS.Click += delegate (object sender, EventArgs e) { FilterMagazines(sender, e, "Kültür&Sanat"); };
        }
        /// <summary>
        /// Sets the events of musicCD's filter buttons
        /// </summary>
        private void MusicFilterEvent()
        {
            BtnMusicPop.Click += delegate (object sender, EventArgs e) { FilterMusics(sender, e, "Pop"); };
            BtnMusicRock.Click += delegate (object sender, EventArgs e) { FilterMusics(sender, e, "Rock"); };
            BtnMusicTSM.Click += delegate (object sender, EventArgs e) { FilterMusics(sender, e, "Türk Sanat Müziği"); };
            BtnMusicArabesk.Click += delegate (object sender, EventArgs e) { FilterMusics(sender, e, "Arabesk"); };
        }
        /// <summary>
        /// This function allows magazines to be displayed on the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMagazine_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Dergi sekmesine tıklandı");
            GBBook.Visible = false;
            GBMusic.Visible = false;
            GBMag.Visible = true;
            TxtSearch.Visible = true;
            LblSearch.Visible = true;
            FlowLMain.Controls.Clear();
            products.Clear();
            db.setListofMagazinesFromDB();
            products = db.getListofMagazines();
            foreach (Magazine magazine in products)
            {
                FlowLMain.Controls.Add(new UserControlMagazine(magazine));
            }
        }
        /// <summary>
        /// This function allwos books to be displayed on the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBook_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Kitap sekmesine tıklandı");
            GBMag.Visible = false;
            GBMusic.Visible = false;
            GBBook.Visible = true;
            TxtSearch.Visible = true;
            LblSearch.Visible = true;
            FlowLMain.Controls.Clear();
            products.Clear();
            db.setListBooksFromDB();
            products = db.getListofBooks();
            foreach (Book book in products)
            {
                FlowLMain.Controls.Add(new UserControlBook(book));
            }
        }
        /// <summary>
        /// This function allows musicCDs to be displayed on the screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMusic_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Müzik sekmesine tıklandı");
            GBMag.Visible = false;
            GBBook.Visible = false;
            GBMusic.Visible = true;
            TxtSearch.Visible = true;
            LblSearch.Visible = true;
            FlowLMain.Controls.Clear();
            products.Clear();
            db.setListMusicCDFromDB();
            products = db.getListofMusicCD();
            foreach (MusicCD music in products)
            {
                FlowLMain.Controls.Add(new UserControlMusic(music));
            }
        }
        /// <summary>
        /// When the customer 'çok satanlar' label is clicek , this function calls BestSellers function to display bestsellers on screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LblSatanlar_Click(object sender, EventArgs e)
        {
            BestSellers();
        }
        /// <summary>
        /// This function allows to search product
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TxtSearch_TextChanged(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(TxtSearch.Text) == false)
            {
                FlowLMain.Controls.Clear();
                for (int i = 0; i < products.Count; i++)
                {
                    string tempstr = products[i].Name.ToLower();
                    if (tempstr.StartsWith(TxtSearch.Text.ToLower()))
                    {
                        if (products[i] is Book)
                        {
                            FlowLMain.Controls.Add(new UserControlBook((Book)products[i]));
                        }
                        else if (products[i] is Magazine)
                        {
                            FlowLMain.Controls.Add(new UserControlMagazine((Magazine)products[i]));
                        }
                        else if (products[i] is MusicCD)
                        {
                            FlowLMain.Controls.Add(new UserControlMusic((MusicCD)products[i]));
                        }
                    }
                }
            }
            else
            {
                FlowLMain.Controls.Clear();
                for (int i = 0; i < products.Count; i++)
                {
                    if (products[i].Name.StartsWith(TxtSearch.Text))
                    {
                        if (products[i] is Book)
                        {
                            FlowLMain.Controls.Add(new UserControlBook((Book)products[i]));
                        }
                        else if (products[i] is Magazine)
                        {
                            FlowLMain.Controls.Add(new UserControlMagazine((Magazine)products[i]));
                        }
                        else if (products[i] is MusicCD)
                        {
                            FlowLMain.Controls.Add(new UserControlMusic((MusicCD)products[i]));
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Opens the Login Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBack_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Çıkış sekmesine tıklandı");
            Login loginform = new Login();
            this.Hide();
            loginform.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// Opens the Cart Form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void PBCart_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Sepet sekmesine tıklandı");
            items = db.getItemsInCart(cartid);
            double totalprice = 0;
            int amount = 0;
            foreach (ItemToPurchase item in items)
            {
                totalprice += item.Product.Price * item.Quantity;
                amount += item.Quantity;
            }
            cart._price = totalprice;
            cart._amount = amount;
            db.updateShoppingCart(cart);
            Cart formcart = new Cart();
            this.Hide();
            formcart.ShowDialog();
        }
        /// <summary>
        /// Shows DateTime in mainStore screen
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerMain_Tick(object sender, EventArgs e)
        {
            DateTime datetime = DateTime.Now;
            LblDateTime.Text = datetime.ToLongDateString();
            LblTime.Text = datetime.ToLongTimeString();
        }
    }
}
