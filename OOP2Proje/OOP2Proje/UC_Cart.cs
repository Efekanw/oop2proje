﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 29.05.2021
*  @brief   : This class includes PaidItem's features
*/
namespace OOP2Proje
{
    public partial class UC_Cart : UserControl
    {
        DataBase db = new DataBase();
        ShoppingCart cart = new ShoppingCart();
        List<ItemToPurchase> paidItems = new List<ItemToPurchase>();

        /// <summary>
        /// Constructor which shows items' features
        /// </summary>
        /// <param name="cart">Takes cart</param>
        public UC_Cart(ShoppingCart cart)
        {
            InitializeComponent();
            this.cart = cart;
            LblAmount.Text = this.cart._amount.ToString();
            LblCartid.Text = this.cart._cartid.ToString();
            string temp = String.Format("{0:0.00}", this.cart._price).ToString();
            LblPrice.Text = temp + " ₺";
        }
        /// <summary>
        /// Shows paid items order by items numbers
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnListele_Click(object sender, EventArgs e)
        {
            FlowLayoutPanel flp = (ParentForm.Controls["FLPaidItems"] as FlowLayoutPanel);
            flp.Controls.Clear();
            paidItems = db.getItemsInCart(cart._cartid);
            foreach (ItemToPurchase item in paidItems)
            {
                flp.Controls.Add(new UC_PaidItem(item));
            }
        }
    }
}
