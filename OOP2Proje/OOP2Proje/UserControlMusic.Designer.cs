﻿
namespace OOP2Proje
{
    partial class UserControlMusic
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.BtnAddToCart = new System.Windows.Forms.Button();
            this.LblMusicSinger = new System.Windows.Forms.Label();
            this.LblMusicPrice = new System.Windows.Forms.Label();
            this.LblMusicName = new System.Windows.Forms.Label();
            this.PBoxMusic = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBoxMusic)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnAddToCart
            // 
            this.BtnAddToCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnAddToCart.BackgroundImage = global::OOP2Proje.Properties.Resources.kisspng_computer_icons_trade_basket_add_to_cart_button_5ac189387dd9d7_3184996315226330165155_2;
            this.BtnAddToCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnAddToCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAddToCart.FlatAppearance.BorderColor = System.Drawing.Color.Black;
            this.BtnAddToCart.FlatAppearance.BorderSize = 0;
            this.BtnAddToCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.BtnAddToCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddToCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnAddToCart.ForeColor = System.Drawing.SystemColors.ControlText;
            this.BtnAddToCart.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.BtnAddToCart.Location = new System.Drawing.Point(319, 357);
            this.BtnAddToCart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnAddToCart.Name = "BtnAddToCart";
            this.BtnAddToCart.Size = new System.Drawing.Size(80, 60);
            this.BtnAddToCart.TabIndex = 16;
            this.BtnAddToCart.UseVisualStyleBackColor = false;
            this.BtnAddToCart.Click += new System.EventHandler(this.BtnAddToCart_Click);
            // 
            // LblMusicSinger
            // 
            this.LblMusicSinger.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblMusicSinger.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicSinger.ForeColor = System.Drawing.Color.Black;
            this.LblMusicSinger.Location = new System.Drawing.Point(0, 281);
            this.LblMusicSinger.Name = "LblMusicSinger";
            this.LblMusicSinger.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblMusicSinger.Size = new System.Drawing.Size(400, 36);
            this.LblMusicSinger.TabIndex = 12;
            this.LblMusicSinger.Text = "author";
            this.LblMusicSinger.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMusicPrice
            // 
            this.LblMusicPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LblMusicPrice.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicPrice.ForeColor = System.Drawing.Color.Black;
            this.LblMusicPrice.Location = new System.Drawing.Point(3, 373);
            this.LblMusicPrice.Name = "LblMusicPrice";
            this.LblMusicPrice.Size = new System.Drawing.Size(136, 39);
            this.LblMusicPrice.TabIndex = 11;
            this.LblMusicPrice.Text = "Price";
            this.LblMusicPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMusicName
            // 
            this.LblMusicName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblMusicName.AutoEllipsis = true;
            this.LblMusicName.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicName.ForeColor = System.Drawing.Color.Black;
            this.LblMusicName.Location = new System.Drawing.Point(0, 224);
            this.LblMusicName.Name = "LblMusicName";
            this.LblMusicName.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LblMusicName.Size = new System.Drawing.Size(400, 57);
            this.LblMusicName.TabIndex = 10;
            this.LblMusicName.Text = "Name";
            this.LblMusicName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBoxMusic
            // 
            this.PBoxMusic.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PBoxMusic.ErrorImage = null;
            this.PBoxMusic.Location = new System.Drawing.Point(109, 2);
            this.PBoxMusic.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBoxMusic.Name = "PBoxMusic";
            this.PBoxMusic.Size = new System.Drawing.Size(180, 220);
            this.PBoxMusic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBoxMusic.TabIndex = 9;
            this.PBoxMusic.TabStop = false;
            // 
            // UserControlMusic
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.Controls.Add(this.BtnAddToCart);
            this.Controls.Add(this.LblMusicSinger);
            this.Controls.Add(this.LblMusicPrice);
            this.Controls.Add(this.LblMusicName);
            this.Controls.Add(this.PBoxMusic);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UserControlMusic";
            this.Size = new System.Drawing.Size(400, 418);
            ((System.ComponentModel.ISupportInitialize)(this.PBoxMusic)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnAddToCart;
        private System.Windows.Forms.Label LblMusicSinger;
        private System.Windows.Forms.Label LblMusicPrice;
        private System.Windows.Forms.Label LblMusicName;
        private System.Windows.Forms.PictureBox PBoxMusic;
    }
}
