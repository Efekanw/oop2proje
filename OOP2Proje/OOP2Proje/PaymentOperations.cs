﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 29.05.2021
*  @brief   : This class includes Payment's strategy design
*/
namespace OOP2Proje
{
    class PaymentOperations
    {
        private IPayment _payment;
        /// <summary>
        /// Takes payment and equals it to our payment
        /// </summary>
        /// <param name="payment"> Takes payment</param>
        public PaymentOperations(IPayment payment)
        {
            this._payment = payment;
        }
        /// <summary>
        /// Makes payment operation
        /// </summary>
        public void make_payment()
        {
            this._payment.make_payment();
        }
    }
}
