﻿
namespace OOP2Proje
{
    partial class UC_Cart
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblCartid = new System.Windows.Forms.Label();
            this.LblAmount = new System.Windows.Forms.Label();
            this.LblPrice = new System.Windows.Forms.Label();
            this.LblAdet = new System.Windows.Forms.Label();
            this.LblFiyat = new System.Windows.Forms.Label();
            this.BtnListele = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // LblCartid
            // 
            this.LblCartid.Font = new System.Drawing.Font("Microsoft Sans Serif", 7.854546F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblCartid.Location = new System.Drawing.Point(263, 17);
            this.LblCartid.Name = "LblCartid";
            this.LblCartid.Size = new System.Drawing.Size(107, 16);
            this.LblCartid.TabIndex = 0;
            this.LblCartid.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblAmount
            // 
            this.LblAmount.AutoSize = true;
            this.LblAmount.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblAmount.Location = new System.Drawing.Point(165, 63);
            this.LblAmount.Name = "LblAmount";
            this.LblAmount.Size = new System.Drawing.Size(66, 22);
            this.LblAmount.TabIndex = 1;
            this.LblAmount.Text = "Amount";
            // 
            // LblPrice
            // 
            this.LblPrice.AutoSize = true;
            this.LblPrice.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblPrice.Location = new System.Drawing.Point(165, 106);
            this.LblPrice.Name = "LblPrice";
            this.LblPrice.Size = new System.Drawing.Size(48, 23);
            this.LblPrice.TabIndex = 2;
            this.LblPrice.Text = "price";
            // 
            // LblAdet
            // 
            this.LblAdet.AutoSize = true;
            this.LblAdet.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblAdet.Location = new System.Drawing.Point(3, 62);
            this.LblAdet.Name = "LblAdet";
            this.LblAdet.Size = new System.Drawing.Size(99, 23);
            this.LblAdet.TabIndex = 3;
            this.LblAdet.Text = "Ürün Adeti:";
            // 
            // LblFiyat
            // 
            this.LblFiyat.AutoSize = true;
            this.LblFiyat.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblFiyat.Location = new System.Drawing.Point(3, 106);
            this.LblFiyat.Name = "LblFiyat";
            this.LblFiyat.Size = new System.Drawing.Size(119, 23);
            this.LblFiyat.TabIndex = 4;
            this.LblFiyat.Text = "Toplam Fiyat: ";
            // 
            // BtnListele
            // 
            this.BtnListele.BackColor = System.Drawing.Color.LemonChiffon;
            this.BtnListele.Font = new System.Drawing.Font("Palatino Linotype", 7.854546F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnListele.ForeColor = System.Drawing.Color.Crimson;
            this.BtnListele.Location = new System.Drawing.Point(348, 70);
            this.BtnListele.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnListele.Name = "BtnListele";
            this.BtnListele.Size = new System.Drawing.Size(170, 59);
            this.BtnListele.TabIndex = 5;
            this.BtnListele.Text = "Ürünleri Listele";
            this.BtnListele.UseVisualStyleBackColor = false;
            this.BtnListele.Click += new System.EventHandler(this.BtnListele_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 9.818182F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(4, 12);
            this.label1.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(194, 23);
            this.label1.TabIndex = 6;
            this.label1.Text = "Sipariş Numarası ------>";
            // 
            // UC_Cart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.Controls.Add(this.label1);
            this.Controls.Add(this.BtnListele);
            this.Controls.Add(this.LblFiyat);
            this.Controls.Add(this.LblAdet);
            this.Controls.Add(this.LblPrice);
            this.Controls.Add(this.LblAmount);
            this.Controls.Add(this.LblCartid);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UC_Cart";
            this.Size = new System.Drawing.Size(521, 151);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label LblCartid;
        private System.Windows.Forms.Label LblAmount;
        private System.Windows.Forms.Label LblPrice;
        private System.Windows.Forms.Label LblAdet;
        private System.Windows.Forms.Label LblFiyat;
        private System.Windows.Forms.Button BtnListele;
        private System.Windows.Forms.Label label1;
    }
}
