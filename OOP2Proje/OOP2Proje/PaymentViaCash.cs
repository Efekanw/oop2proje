﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 29.05.2021
*  @brief   : This class includes PaymentviaCash functions
*/
namespace OOP2Proje
{
    class PaymentViaCash : IPayment
    {
        private ShoppingCart cart;
        /// <summary>
        /// Takes cart and equals it to our cart
        /// </summary>
        /// <param name="crt">Takes cart</param>
        public PaymentViaCash(ShoppingCart crt)
        {
            this.cart = crt;
        }
        /// <summary>
        /// Makes payment type as "Nakit"
        /// </summary>
        public void make_payment()
        {
            this.cart._payment_type = "Nakit";
        }
    }
}
