﻿
namespace OOP2Proje
{
    partial class UC_PaymentItem_MusicCD
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPaymentMusicCDAdet = new System.Windows.Forms.Label();
            this.lblPaymentMusicCDToplam = new System.Windows.Forms.Label();
            this.lblPaymentMusicCDCount = new System.Windows.Forms.Label();
            this.lblPaymentMusicCDBirim = new System.Windows.Forms.Label();
            this.lblPaymentMusicCDBirimFiyat = new System.Windows.Forms.Label();
            this.lblPaymentMusicCDToplamFiyat = new System.Windows.Forms.Label();
            this.lblPaymentMusicCDName = new System.Windows.Forms.Label();
            this.PBPaymentMusicCD = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBPaymentMusicCD)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPaymentMusicCDAdet
            // 
            this.lblPaymentMusicCDAdet.AutoSize = true;
            this.lblPaymentMusicCDAdet.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDAdet.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDAdet.Location = new System.Drawing.Point(155, 63);
            this.lblPaymentMusicCDAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDAdet.Name = "lblPaymentMusicCDAdet";
            this.lblPaymentMusicCDAdet.Size = new System.Drawing.Size(41, 20);
            this.lblPaymentMusicCDAdet.TabIndex = 12;
            this.lblPaymentMusicCDAdet.Text = "Adet";
            // 
            // lblPaymentMusicCDToplam
            // 
            this.lblPaymentMusicCDToplam.AutoSize = true;
            this.lblPaymentMusicCDToplam.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDToplam.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDToplam.Location = new System.Drawing.Point(366, 63);
            this.lblPaymentMusicCDToplam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDToplam.Name = "lblPaymentMusicCDToplam";
            this.lblPaymentMusicCDToplam.Size = new System.Drawing.Size(96, 20);
            this.lblPaymentMusicCDToplam.TabIndex = 13;
            this.lblPaymentMusicCDToplam.Text = "Toplam Fiyat";
            // 
            // lblPaymentMusicCDCount
            // 
            this.lblPaymentMusicCDCount.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDCount.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDCount.Location = new System.Drawing.Point(155, 86);
            this.lblPaymentMusicCDCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDCount.Name = "lblPaymentMusicCDCount";
            this.lblPaymentMusicCDCount.Size = new System.Drawing.Size(36, 30);
            this.lblPaymentMusicCDCount.TabIndex = 14;
            this.lblPaymentMusicCDCount.Text = "0";
            this.lblPaymentMusicCDCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPaymentMusicCDBirim
            // 
            this.lblPaymentMusicCDBirim.AutoSize = true;
            this.lblPaymentMusicCDBirim.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDBirim.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDBirim.Location = new System.Drawing.Point(257, 63);
            this.lblPaymentMusicCDBirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDBirim.Name = "lblPaymentMusicCDBirim";
            this.lblPaymentMusicCDBirim.Size = new System.Drawing.Size(86, 20);
            this.lblPaymentMusicCDBirim.TabIndex = 15;
            this.lblPaymentMusicCDBirim.Text = "Birim Fiyatı";
            // 
            // lblPaymentMusicCDBirimFiyat
            // 
            this.lblPaymentMusicCDBirimFiyat.AutoSize = true;
            this.lblPaymentMusicCDBirimFiyat.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDBirimFiyat.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDBirimFiyat.Location = new System.Drawing.Point(289, 93);
            this.lblPaymentMusicCDBirimFiyat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDBirimFiyat.Name = "lblPaymentMusicCDBirimFiyat";
            this.lblPaymentMusicCDBirimFiyat.Size = new System.Drawing.Size(17, 20);
            this.lblPaymentMusicCDBirimFiyat.TabIndex = 16;
            this.lblPaymentMusicCDBirimFiyat.Text = "0";
            // 
            // lblPaymentMusicCDToplamFiyat
            // 
            this.lblPaymentMusicCDToplamFiyat.AutoSize = true;
            this.lblPaymentMusicCDToplamFiyat.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDToplamFiyat.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDToplamFiyat.Location = new System.Drawing.Point(404, 93);
            this.lblPaymentMusicCDToplamFiyat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDToplamFiyat.Name = "lblPaymentMusicCDToplamFiyat";
            this.lblPaymentMusicCDToplamFiyat.Size = new System.Drawing.Size(17, 20);
            this.lblPaymentMusicCDToplamFiyat.TabIndex = 17;
            this.lblPaymentMusicCDToplamFiyat.Text = "0";
            // 
            // lblPaymentMusicCDName
            // 
            this.lblPaymentMusicCDName.AutoEllipsis = true;
            this.lblPaymentMusicCDName.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMusicCDName.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMusicCDName.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentMusicCDName.Location = new System.Drawing.Point(121, 0);
            this.lblPaymentMusicCDName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMusicCDName.Name = "lblPaymentMusicCDName";
            this.lblPaymentMusicCDName.Size = new System.Drawing.Size(383, 63);
            this.lblPaymentMusicCDName.TabIndex = 21;
            this.lblPaymentMusicCDName.Text = "Müzik";
            this.lblPaymentMusicCDName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBPaymentMusicCD
            // 
            this.PBPaymentMusicCD.ErrorImage = null;
            this.PBPaymentMusicCD.Location = new System.Drawing.Point(16, 16);
            this.PBPaymentMusicCD.Margin = new System.Windows.Forms.Padding(2);
            this.PBPaymentMusicCD.Name = "PBPaymentMusicCD";
            this.PBPaymentMusicCD.Size = new System.Drawing.Size(92, 100);
            this.PBPaymentMusicCD.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBPaymentMusicCD.TabIndex = 11;
            this.PBPaymentMusicCD.TabStop = false;
            // 
            // UC_PaymentItem_MusicCD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblPaymentMusicCDName);
            this.Controls.Add(this.PBPaymentMusicCD);
            this.Controls.Add(this.lblPaymentMusicCDAdet);
            this.Controls.Add(this.lblPaymentMusicCDToplam);
            this.Controls.Add(this.lblPaymentMusicCDCount);
            this.Controls.Add(this.lblPaymentMusicCDToplamFiyat);
            this.Controls.Add(this.lblPaymentMusicCDBirim);
            this.Controls.Add(this.lblPaymentMusicCDBirimFiyat);
            this.Name = "UC_PaymentItem_MusicCD";
            this.Size = new System.Drawing.Size(504, 130);
            ((System.ComponentModel.ISupportInitialize)(this.PBPaymentMusicCD)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBPaymentMusicCD;
        private System.Windows.Forms.Label lblPaymentMusicCDAdet;
        private System.Windows.Forms.Label lblPaymentMusicCDToplam;
        private System.Windows.Forms.Label lblPaymentMusicCDCount;
        private System.Windows.Forms.Label lblPaymentMusicCDBirim;
        private System.Windows.Forms.Label lblPaymentMusicCDBirimFiyat;
        private System.Windows.Forms.Label lblPaymentMusicCDToplamFiyat;
        private System.Windows.Forms.Label lblPaymentMusicCDName;
    }
}
