﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Yunus Akyol
*  @number  : 152120181045
*  @mail    : yunusakyol5@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class provides to implement singleton design pattern.
*/
namespace OOP2Proje
{
    class SingletonLoginedCustomer
    {
        private Customer customer;

        private static SingletonLoginedCustomer singletonLoginedCustomer;
        public Customer Customer { get => customer; set => customer = value; }
        ///<summary>
        ///This function checks that if user is already exist.  
        ///</summary>
        ///
        public static SingletonLoginedCustomer getInstance()
        {
            if (singletonLoginedCustomer == null)
            {
                singletonLoginedCustomer = new SingletonLoginedCustomer();
            }
            return singletonLoginedCustomer;
        }
    }
}
