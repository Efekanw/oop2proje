﻿
namespace OOP2Proje
{
    partial class UserControlBook
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblBookName = new System.Windows.Forms.Label();
            this.LblBookPrice = new System.Windows.Forms.Label();
            this.LblBookAuthor = new System.Windows.Forms.Label();
            this.LblBookISBN = new System.Windows.Forms.Label();
            this.LblBookPublisher = new System.Windows.Forms.Label();
            this.LblBookPage = new System.Windows.Forms.Label();
            this.BtnAddToCart = new System.Windows.Forms.Button();
            this.PBoxBook = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBoxBook)).BeginInit();
            this.SuspendLayout();
            // 
            // LblBookName
            // 
            this.LblBookName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblBookName.AutoEllipsis = true;
            this.LblBookName.BackColor = System.Drawing.Color.Transparent;
            this.LblBookName.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookName.ForeColor = System.Drawing.Color.Black;
            this.LblBookName.Location = new System.Drawing.Point(1, 224);
            this.LblBookName.Name = "LblBookName";
            this.LblBookName.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LblBookName.Size = new System.Drawing.Size(397, 79);
            this.LblBookName.TabIndex = 2;
            this.LblBookName.Text = "Name";
            this.LblBookName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblBookPrice
            // 
            this.LblBookPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LblBookPrice.BackColor = System.Drawing.Color.Transparent;
            this.LblBookPrice.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookPrice.ForeColor = System.Drawing.Color.Black;
            this.LblBookPrice.Location = new System.Drawing.Point(3, 383);
            this.LblBookPrice.Name = "LblBookPrice";
            this.LblBookPrice.Size = new System.Drawing.Size(117, 32);
            this.LblBookPrice.TabIndex = 3;
            this.LblBookPrice.Text = "Price";
            this.LblBookPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblBookAuthor
            // 
            this.LblBookAuthor.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblBookAuthor.BackColor = System.Drawing.Color.Transparent;
            this.LblBookAuthor.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LblBookAuthor.ForeColor = System.Drawing.Color.Black;
            this.LblBookAuthor.Location = new System.Drawing.Point(-1, 293);
            this.LblBookAuthor.Name = "LblBookAuthor";
            this.LblBookAuthor.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.LblBookAuthor.Size = new System.Drawing.Size(403, 43);
            this.LblBookAuthor.TabIndex = 4;
            this.LblBookAuthor.Text = "author";
            this.LblBookAuthor.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblBookISBN
            // 
            this.LblBookISBN.Font = new System.Drawing.Font("Palatino Linotype", 7.8F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookISBN.Location = new System.Drawing.Point(85, 2);
            this.LblBookISBN.Name = "LblBookISBN";
            this.LblBookISBN.Size = new System.Drawing.Size(20, 222);
            this.LblBookISBN.TabIndex = 5;
            this.LblBookISBN.Text = "ISBN";
            this.LblBookISBN.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // LblBookPublisher
            // 
            this.LblBookPublisher.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblBookPublisher.BackColor = System.Drawing.Color.Transparent;
            this.LblBookPublisher.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookPublisher.ForeColor = System.Drawing.Color.Black;
            this.LblBookPublisher.Location = new System.Drawing.Point(-1, 336);
            this.LblBookPublisher.Name = "LblBookPublisher";
            this.LblBookPublisher.Size = new System.Drawing.Size(403, 31);
            this.LblBookPublisher.TabIndex = 6;
            this.LblBookPublisher.Text = "Publisher";
            this.LblBookPublisher.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblBookPage
            // 
            this.LblBookPage.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LblBookPage.BackColor = System.Drawing.Color.Transparent;
            this.LblBookPage.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookPage.ForeColor = System.Drawing.Color.Black;
            this.LblBookPage.Location = new System.Drawing.Point(-1, 354);
            this.LblBookPage.Name = "LblBookPage";
            this.LblBookPage.Size = new System.Drawing.Size(117, 25);
            this.LblBookPage.TabIndex = 7;
            this.LblBookPage.Text = "Page";
            this.LblBookPage.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnAddToCart
            // 
            this.BtnAddToCart.BackgroundImage = global::OOP2Proje.Properties.Resources.musicadd;
            this.BtnAddToCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnAddToCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAddToCart.FlatAppearance.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.BtnAddToCart.FlatAppearance.BorderSize = 0;
            this.BtnAddToCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.BtnAddToCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddToCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnAddToCart.ImageKey = "(yok)";
            this.BtnAddToCart.Location = new System.Drawing.Point(319, 357);
            this.BtnAddToCart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnAddToCart.Name = "BtnAddToCart";
            this.BtnAddToCart.Size = new System.Drawing.Size(80, 60);
            this.BtnAddToCart.TabIndex = 8;
            this.BtnAddToCart.TabStop = false;
            this.BtnAddToCart.UseVisualStyleBackColor = true;
            this.BtnAddToCart.Click += new System.EventHandler(this.BtnAddToCart_Click);
            // 
            // PBoxBook
            // 
            this.PBoxBook.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PBoxBook.ErrorImage = null;
            this.PBoxBook.Location = new System.Drawing.Point(109, 2);
            this.PBoxBook.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBoxBook.Name = "PBoxBook";
            this.PBoxBook.Size = new System.Drawing.Size(180, 220);
            this.PBoxBook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBoxBook.TabIndex = 1;
            this.PBoxBook.TabStop = false;
            // 
            // UserControlBook
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.BtnAddToCart);
            this.Controls.Add(this.LblBookPage);
            this.Controls.Add(this.LblBookPublisher);
            this.Controls.Add(this.LblBookISBN);
            this.Controls.Add(this.LblBookAuthor);
            this.Controls.Add(this.LblBookPrice);
            this.Controls.Add(this.LblBookName);
            this.Controls.Add(this.PBoxBook);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UserControlBook";
            this.Size = new System.Drawing.Size(400, 418);
            ((System.ComponentModel.ISupportInitialize)(this.PBoxBook)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox PBoxBook;
        private System.Windows.Forms.Label LblBookName;
        private System.Windows.Forms.Label LblBookPrice;
        private System.Windows.Forms.Label LblBookAuthor;
        private System.Windows.Forms.Label LblBookISBN;
        private System.Windows.Forms.Label LblBookPublisher;
        private System.Windows.Forms.Label LblBookPage;
        private System.Windows.Forms.Button BtnAddToCart;
    }
}
