﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 31.05.2021
*  @brief   : This class includes Logger functions
*/
namespace OOP2Proje
{
    public class Logger
    {
        private static Logger logger = null;
        /// <summary>
        /// Get to logger if its null create new one.
        /// </summary>
        /// <returns>logger</returns>
        public static Logger Getlogger()
        {
            if (logger == null)
            {
                logger = new Logger();
            }
            return logger;
        }
        /// <summary>
        /// Write the logs on Log.txt file.
        /// </summary>
        /// <param name="username"></param>
        /// <param name="btn_name"></param>
        public void WriteLog(string username, string btn_name)
        {
            string str = "Kullanıcı Adı: " + username + "          " + "Müşteri ID: " + SingletonLoginedCustomer.getInstance().Customer.CustomerID + "          " + "Tıklanan Buton: " + btn_name + "          " + "Zaman: " + DateTime.Now;
            try
            {
                using (StreamWriter Sw = File.AppendText("Log.txt"))
                {
                    Sw.WriteLine(str);
                    Sw.Close();
                }
            }
            catch (Exception)
            {
                Console.WriteLine("Yazılacak Dosya Bulunamadı.");
            }
        }
    }
}
