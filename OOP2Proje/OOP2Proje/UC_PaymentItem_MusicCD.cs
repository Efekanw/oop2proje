﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Yunus Akyol
*  @number  : 152120181045
*  @mail    : yunusakyol5@gmail.com
*  @date    : 27.05.2021
*  @brief   : This class includes musicCD user control for payment form.
*/
namespace OOP2Proje
{
    public partial class UC_PaymentItem_MusicCD : UserControl
    {
        ItemToPurchase item = new ItemToPurchase();
        double lblprice;
        /// <summary>
        /// This function provides to set labels and picture box from given item.
        /// </summary>
        /// <param name="item">Takes item that implements in user control.</param>
        public UC_PaymentItem_MusicCD(ItemToPurchase item)
        {
            InitializeComponent();
            this.item = item;
            lblPaymentMusicCDName.Text = this.item.Product.Name;
            lblPaymentMusicCDCount.Text = this.item.Quantity.ToString();
            string temp = String.Format("{0:0.00}", this.item.Product.Price).ToString();
            lblPaymentMusicCDBirimFiyat.Text = temp + " ₺";
            lblprice = this.item.Product.Price * this.item.Quantity;
            string tempprice = String.Format("{0:0.00}", lblprice).ToString();
            lblPaymentMusicCDToplamFiyat.Text = tempprice + " ₺";
            System.IO.MemoryStream ms = new System.IO.MemoryStream(this.item.Product.Image);
            PBPaymentMusicCD.Image = System.Drawing.Image.FromStream(ms);
        }
    }
}
