﻿
namespace OOP2Proje
{
    partial class UC_PaidItem
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblBookCbirim = new System.Windows.Forms.Label();
            this.LblPaidCTot = new System.Windows.Forms.Label();
            this.LblBookCUP = new System.Windows.Forms.Label();
            this.LblPaidCCount = new System.Windows.Forms.Label();
            this.LblPaidCPrice = new System.Windows.Forms.Label();
            this.LblPaidCamo = new System.Windows.Forms.Label();
            this.PBPaidItemC = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBPaidItemC)).BeginInit();
            this.SuspendLayout();
            // 
            // LblBookCbirim
            // 
            this.LblBookCbirim.AutoSize = true;
            this.LblBookCbirim.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCbirim.Location = new System.Drawing.Point(219, 55);
            this.LblBookCbirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCbirim.Name = "LblBookCbirim";
            this.LblBookCbirim.Size = new System.Drawing.Size(16, 19);
            this.LblBookCbirim.TabIndex = 17;
            this.LblBookCbirim.Text = "0";
            // 
            // LblPaidCTot
            // 
            this.LblPaidCTot.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblPaidCTot.Location = new System.Drawing.Point(219, 85);
            this.LblPaidCTot.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblPaidCTot.Name = "LblPaidCTot";
            this.LblPaidCTot.Size = new System.Drawing.Size(64, 17);
            this.LblPaidCTot.TabIndex = 16;
            this.LblPaidCTot.Text = "0";
            this.LblPaidCTot.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // LblBookCUP
            // 
            this.LblBookCUP.AutoSize = true;
            this.LblBookCUP.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookCUP.Location = new System.Drawing.Point(109, 53);
            this.LblBookCUP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblBookCUP.Name = "LblBookCUP";
            this.LblBookCUP.Size = new System.Drawing.Size(90, 19);
            this.LblBookCUP.TabIndex = 15;
            this.LblBookCUP.Text = "Birim Fiyatı";
            // 
            // LblPaidCCount
            // 
            this.LblPaidCCount.AutoSize = true;
            this.LblPaidCCount.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblPaidCCount.Location = new System.Drawing.Point(219, 27);
            this.LblPaidCCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblPaidCCount.Name = "LblPaidCCount";
            this.LblPaidCCount.Size = new System.Drawing.Size(16, 19);
            this.LblPaidCCount.TabIndex = 14;
            this.LblPaidCCount.Text = "0";
            // 
            // LblPaidCPrice
            // 
            this.LblPaidCPrice.AutoSize = true;
            this.LblPaidCPrice.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblPaidCPrice.Location = new System.Drawing.Point(110, 83);
            this.LblPaidCPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblPaidCPrice.Name = "LblPaidCPrice";
            this.LblPaidCPrice.Size = new System.Drawing.Size(96, 19);
            this.LblPaidCPrice.TabIndex = 13;
            this.LblPaidCPrice.Text = "Toplam Fiyat";
            // 
            // LblPaidCamo
            // 
            this.LblPaidCamo.AutoSize = true;
            this.LblPaidCamo.Font = new System.Drawing.Font("Palatino Linotype", 9.163636F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblPaidCamo.Location = new System.Drawing.Point(109, 25);
            this.LblPaidCamo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblPaidCamo.Name = "LblPaidCamo";
            this.LblPaidCamo.Size = new System.Drawing.Size(40, 19);
            this.LblPaidCamo.TabIndex = 12;
            this.LblPaidCamo.Text = "Adet";
            // 
            // PBPaidItemC
            // 
            this.PBPaidItemC.ErrorImage = null;
            this.PBPaidItemC.Location = new System.Drawing.Point(13, 16);
            this.PBPaidItemC.Margin = new System.Windows.Forms.Padding(2);
            this.PBPaidItemC.Name = "PBPaidItemC";
            this.PBPaidItemC.Size = new System.Drawing.Size(92, 99);
            this.PBPaidItemC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBPaidItemC.TabIndex = 11;
            this.PBPaidItemC.TabStop = false;
            // 
            // UC_PaidItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.SpringGreen;
            this.Controls.Add(this.LblBookCbirim);
            this.Controls.Add(this.LblPaidCTot);
            this.Controls.Add(this.LblBookCUP);
            this.Controls.Add(this.LblPaidCCount);
            this.Controls.Add(this.LblPaidCPrice);
            this.Controls.Add(this.LblPaidCamo);
            this.Controls.Add(this.PBPaidItemC);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UC_PaidItem";
            this.Size = new System.Drawing.Size(285, 132);
            ((System.ComponentModel.ISupportInitialize)(this.PBPaidItemC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label LblBookCbirim;
        private System.Windows.Forms.Label LblPaidCTot;
        private System.Windows.Forms.Label LblBookCUP;
        private System.Windows.Forms.Label LblPaidCCount;
        private System.Windows.Forms.Label LblPaidCPrice;
        private System.Windows.Forms.Label LblPaidCamo;
        private System.Windows.Forms.PictureBox PBPaidItemC;
    }
}
