﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 25.05.2021
*  @brief   : This class includes User Control Magazine.
*/
namespace OOP2Proje
{
    public partial class UserControlMagazine : UserControl
    {
        public delegate void UpdataHandler(object sender, EventArgs e);
        DataBase db = new DataBase();
        Magazine mag;
        ItemToPurchase item = new ItemToPurchase();
        /// <summary>
        /// This constructor arranges according to the incoming magazine object.
        /// </summary>
        /// <param name="mag"></param>
        public UserControlMagazine(Magazine mag)
        {
            InitializeComponent();
            this.mag = mag;
            LblMagName.Text = this.mag.Name;
            LblMagIssue.Text = this.mag.Issue.ToString() + ".Baskı";
            string temp = String.Format("{0:0.00}", this.mag.Price).ToString();
            LblMagPrice.Text = temp + " ₺";
            System.IO.MemoryStream ms = new System.IO.MemoryStream(this.mag.Image);
            PBoxMag.Image = System.Drawing.Image.FromStream(ms);
        }
        /// <summary>
        /// Add to cart button click event.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnAddToCart_Click(object sender, EventArgs e)
        {
            item.Product = mag;
            item.ProductType = "Magazine";
            item.Cartid = mainStore.cartid;
            List<ItemToPurchase> items = new List<ItemToPurchase>();
            items = db.getItemsInCart(item.Cartid);
            if (items.Any(a => a.Product.Name == item.Product.Name))
            {
                item.Quantity++;
                db.updateItemInDB(item);
            }
            else
            {
                db.addproductToCartDB(item);
            }
            Label labelcount = (ParentForm.Controls["LblCount"] as Label);
            int count = Convert.ToInt32(labelcount.Text);
            count += 1;
            labelcount.Text = count.ToString();
        }
    }
}
