﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 30.05.2021
*  @brief   : This class generates chosen invoice sending type
*/
namespace OOP2Proje
{
    class InvoiceOperations
    {
        private Invoice invoice;
        /// <summary>
        /// Constructor for Invoice Operation
        /// </summary>
        /// <param name="_invoice">Takes invoice sending type</param>
        public InvoiceOperations(Invoice _invoice)
        {
            this.invoice = _invoice;
        }
        /// <summary>
        /// Main function for sending invoice
        /// </summary>
        public void sendInvoice()
        {
            this.invoice.sendInvoice();
        }
    }
}