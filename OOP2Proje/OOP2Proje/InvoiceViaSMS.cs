﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 30.05.2021
*  @brief   : Class for SMS strategy for sending invoice
*/
namespace OOP2Proje
{
    class InvoiceViaSMS : Invoice
    {
        /// <summary>
        /// Displays a message about invoice via SMS
        /// </summary>
        public void sendInvoice()
        {
            MessageBox.Show("Faturanız SMS aracılığıyla iletilmiştir !");
        }
    }
}
