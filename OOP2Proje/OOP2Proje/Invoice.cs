﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 30.05.2021
*  @brief   : Interface which includes sendInvoice function.
*/
namespace OOP2Proje
{
    interface Invoice
    {
        void sendInvoice();
    }
}