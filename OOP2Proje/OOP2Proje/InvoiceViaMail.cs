﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net.Mail;
using System.Windows.Forms;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 30.05.2021
*  @brief   : Class for mail strategy to sending invoice
*/
namespace OOP2Proje
{
    class InvoiceViaMail : Invoice
    {
        /// <summary>
        /// This function generates a mail operation using MailMessage library, and sends mail according to given inputs
        /// </summary>
        public void sendInvoice()
        {
            try
            {
                MailMessage mesaj = new MailMessage();
                mesaj.To.Add(SingletonLoginedCustomer.getInstance().Customer.Email);
                mesaj.From = new MailAddress("sigmakitap@outlook.com");
                mesaj.Subject = "SİGMA KİTABEVİ - FATURANIZ";
                mesaj.Body = "Faturanız ektedir. Sigma Kitabevini tercih ettiğiniz için teşekkür ederiz :)";
                SmtpClient istemci = new SmtpClient();
                istemci.Port = 587;
                istemci.Credentials = new System.Net.NetworkCredential("sigmakitap@outlook.com", "sigma5159");
                istemci.Host = "smtp.live.com";
                istemci.EnableSsl = true;
                Attachment atch = new Attachment("Invoice.pdf");
                mesaj.Attachments.Add(atch);
                istemci.Send(mesaj);
                MessageBox.Show("Faturanız E-mail yoluyla iletilmiştir !");
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
