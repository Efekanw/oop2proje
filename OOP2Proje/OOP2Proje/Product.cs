﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes Product abstract class.
*/
namespace OOP2Proje
{
    public abstract class Product
    {
        private string name;
        private double price;
        private int id;
        private byte[] image;
        /// <summary>
        /// Getter Setter for name
        /// </summary>
        public string Name
        {
            get
            {
                return name;
            }
            set
            {
                name = value;
            }
        }
        /// <summary>
        /// Getter Setter for price
        /// </summary>
        public double Price
        {
            get
            {
                return price;
            }
            set
            {
                price = value;
            }
        }
        /// <summary>
        /// Getter Setter for ID
        /// </summary>
        public int Id
        {
            get
            {
                return id;
            }
            set
            {
                id = value;
            }
        }
        /// <summary>
        /// Getter Setter for image
        /// </summary>
        public byte[] Image
        {
            get
            {
                return image;
            }
            set
            {
                image = value;
            }
        }
        /// <summary>
        /// Abstract printProperties function
        /// </summary>
        public abstract string printProperties();
    }
}
