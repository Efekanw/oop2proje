﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 30.05.2021
*  @brief   : This class is for payment form and sending invoice process  
*/
namespace OOP2Proje
{
    public partial class Payment : Form
    {
        List<ItemToPurchase> notPaidItems = new List<ItemToPurchase>();
        List<ItemToPurchase> paidItems = new List<ItemToPurchase>();
        List<Customer> customers = new List<Customer>();
        List<ShoppingCart> carts = new List<ShoppingCart>();
        public ShoppingCart cart = new ShoppingCart();
        DataBase db = new DataBase();
        PaymentOperations pay_op = null;
        /// <summary>
        /// Default constructor for Payment class
        /// </summary>
        public Payment()
        {
            InitializeComponent();
            timer_payment.Start();
        }
        /// <summary>
        /// This function brings customer Payment form to Cart form
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBack_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Sepete Dön sekmesine tıklandı");
            Cart cartform = new Cart();
            this.Hide();
            cartform.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// This functions fills all flowlayout panels for paid and not paid items in customer's cart
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Payment_Load(object sender, EventArgs e)
        {
            try
            {
                FLPaid.Controls.Clear();
                FLnotPaid.Controls.Clear();
                carts = db.getCartsPaymentFromDB(1, SingletonLoginedCustomer.getInstance().Customer.CustomerID);
                cart = db.getShoppingCartFromDB(mainStore.cartid);
                notPaidItems = db.getItemsInCart(cart._cartid);
                foreach (ItemToPurchase item in notPaidItems)
                {
                    if (item.Product is Book)
                    {
                        FLnotPaid.Controls.Add(new UC_PaymentItem_Book(item));
                    }
                    else if (item.Product is Magazine)
                    {
                        FLnotPaid.Controls.Add(new UC_PaymentItem_Magazine(item));
                    }
                    else if (item.Product is MusicCD)
                    {
                        FLnotPaid.Controls.Add(new UC_PaymentItem_MusicCD(item));
                    }
                }
                foreach (ShoppingCart cart in carts)
                {
                    FLPaid.Controls.Add(new UC_Cart(cart));
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function gets customer's phone number
        /// </summary>
        /// <returns>customer's phone number from textbox</returns>
        public string getCstTelNo()
        {
            return payTel_txtBX.Text;
        }
        /// <summary>
        /// This function sets visibility of phone number textbox
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void sms_radioBT_CheckedChanged(object sender, EventArgs e)
        {
            if (sms_radioBT.Checked)
            {
                payTel_txtBX.Visible = true;
            }
            else
            {
                payTel_txtBX.Visible = false;
            }
        }
        /// <summary>
        /// This function sets the way of payment type to credit card using strategy design pattern
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pay_credit_rdBT_CheckedChanged(object sender, EventArgs e)
        {
            if (pay_credit_rdBT.Checked)
            {
                pay_op = new PaymentOperations(new PaymentViaCredit(cart));
                invoiceTypeGrpBX.Visible = true;
                pay_cash_rdBT.Location = new Point(29, 200);
                credit_no_label.Visible = true;
                credit_no_txtBX.Visible = true;
                cvv_label.Visible = true;
                cvv_txtBX.Visible = true;
            }
            pay_op.make_payment();
        }
        /// <summary>
        /// This function sets the way of payment type to cash using strategy design pattern
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void pay_cash_rdBT_CheckedChanged(object sender, EventArgs e)
        {
            if (pay_cash_rdBT.Checked)
            {
                pay_op = new PaymentOperations(new PaymentViaCash(cart));
                invoiceTypeGrpBX.Visible = true;
                credit_no_label.Visible = false;
                credit_no_txtBX.Visible = false;
                cvv_label.Visible = false;
                cvv_txtBX.Visible = false;
                pay_cash_rdBT.Location = new Point(39, 193);
            }
            pay_op.make_payment();
        }
        /// <summary>
        /// This function checks the way of invoice sending type and calls necessary function via strategy pattern and also sends invoice with pdf
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Btn_order_placed_Click(object sender, EventArgs e)
        {
            if (!sms_radioBT.Checked && !mail_radioBT.Checked)
            {
                MessageBox.Show("Lütfen Fatura Yollama Türünüzü Seçiniz !!");
            }
            else
            {
                Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Ödeme Yap sekmesine tıklandı");
                InvoiceOperations inv_op = null;
                db.updateCartStatus();

                if (FLnotPaid.Controls.Count > 0)
                {
                    FLPaid.Controls.Add(new UC_Cart(cart));
                    FLnotPaid.Controls.Clear();
                }
                if (mail_radioBT.Checked)
                {
                    PDF pdf = new PDF(cart._payment_type);
                    pdf.create_PDF();
                    inv_op = new InvoiceOperations(new InvoiceViaMail());
                    System.Diagnostics.Process.Start("Invoice.pdf");
                }
                else if (sms_radioBT.Checked)
                {
                    inv_op = new InvoiceOperations(new InvoiceViaSMS());
                }
                inv_op.sendInvoice();
            }
        }
        /// <summary>
        /// This function shows the time and date of the day.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timer_payment_Tick(object sender, EventArgs e)
        {
            DateTime datetime = DateTime.Now;
            Lbl_PaymentDate.Text = datetime.ToLongDateString();
            Lbl_PaymentTimer.Text = datetime.ToLongTimeString();
        }
    }
}
