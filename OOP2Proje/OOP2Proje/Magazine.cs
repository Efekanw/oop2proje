﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes Magazine abstract class.
*/
namespace OOP2Proje
{
    public class Magazine : Product
    {
        private string type;
        private int issue;
        /// <summary>
        /// Getter Setter for issue
        /// </summary>
        public int Issue
        {
            get { return issue; }
            set { issue = value; }
        }
        /// <summary>
        /// Getter Setter for type
        /// </summary>
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        /// <summary>
        /// Abstract printProperties function
        /// </summary>
        /// <returns>type and issue</returns>
        public override string printProperties()
        {
            return "Türü: " + type + " " + "Sayısı: " + issue;
        }
    }
}
