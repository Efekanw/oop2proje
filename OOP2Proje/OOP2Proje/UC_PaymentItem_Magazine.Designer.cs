﻿
namespace OOP2Proje
{
    partial class UC_PaymentItem_Magazine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPaymentMagazineAdet = new System.Windows.Forms.Label();
            this.lblPaymentMagazineToplam = new System.Windows.Forms.Label();
            this.lblPaymentMagazineCount = new System.Windows.Forms.Label();
            this.lblPaymentMagazineBirim = new System.Windows.Forms.Label();
            this.lblPaymentMagazineToplamFiyat = new System.Windows.Forms.Label();
            this.lblPaymentMagazineBirimFiyat = new System.Windows.Forms.Label();
            this.lblPaymentMagazineName = new System.Windows.Forms.Label();
            this.PBPaymentMagazine = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBPaymentMagazine)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPaymentMagazineAdet
            // 
            this.lblPaymentMagazineAdet.AutoSize = true;
            this.lblPaymentMagazineAdet.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineAdet.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineAdet.Location = new System.Drawing.Point(155, 63);
            this.lblPaymentMagazineAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineAdet.Name = "lblPaymentMagazineAdet";
            this.lblPaymentMagazineAdet.Size = new System.Drawing.Size(41, 20);
            this.lblPaymentMagazineAdet.TabIndex = 12;
            this.lblPaymentMagazineAdet.Text = "Adet";
            // 
            // lblPaymentMagazineToplam
            // 
            this.lblPaymentMagazineToplam.AutoSize = true;
            this.lblPaymentMagazineToplam.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineToplam.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineToplam.Location = new System.Drawing.Point(366, 63);
            this.lblPaymentMagazineToplam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineToplam.Name = "lblPaymentMagazineToplam";
            this.lblPaymentMagazineToplam.Size = new System.Drawing.Size(96, 20);
            this.lblPaymentMagazineToplam.TabIndex = 13;
            this.lblPaymentMagazineToplam.Text = "Toplam Fiyat";
            // 
            // lblPaymentMagazineCount
            // 
            this.lblPaymentMagazineCount.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineCount.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineCount.Location = new System.Drawing.Point(155, 86);
            this.lblPaymentMagazineCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineCount.Name = "lblPaymentMagazineCount";
            this.lblPaymentMagazineCount.Size = new System.Drawing.Size(41, 30);
            this.lblPaymentMagazineCount.TabIndex = 14;
            this.lblPaymentMagazineCount.Text = "0";
            this.lblPaymentMagazineCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPaymentMagazineBirim
            // 
            this.lblPaymentMagazineBirim.AutoSize = true;
            this.lblPaymentMagazineBirim.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineBirim.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineBirim.Location = new System.Drawing.Point(257, 63);
            this.lblPaymentMagazineBirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineBirim.Name = "lblPaymentMagazineBirim";
            this.lblPaymentMagazineBirim.Size = new System.Drawing.Size(86, 20);
            this.lblPaymentMagazineBirim.TabIndex = 15;
            this.lblPaymentMagazineBirim.Text = "Birim Fiyatı";
            // 
            // lblPaymentMagazineToplamFiyat
            // 
            this.lblPaymentMagazineToplamFiyat.AutoSize = true;
            this.lblPaymentMagazineToplamFiyat.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineToplamFiyat.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineToplamFiyat.Location = new System.Drawing.Point(404, 93);
            this.lblPaymentMagazineToplamFiyat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineToplamFiyat.Name = "lblPaymentMagazineToplamFiyat";
            this.lblPaymentMagazineToplamFiyat.Size = new System.Drawing.Size(17, 20);
            this.lblPaymentMagazineToplamFiyat.TabIndex = 16;
            this.lblPaymentMagazineToplamFiyat.Text = "0";
            // 
            // lblPaymentMagazineBirimFiyat
            // 
            this.lblPaymentMagazineBirimFiyat.AutoSize = true;
            this.lblPaymentMagazineBirimFiyat.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineBirimFiyat.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineBirimFiyat.Location = new System.Drawing.Point(289, 93);
            this.lblPaymentMagazineBirimFiyat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineBirimFiyat.Name = "lblPaymentMagazineBirimFiyat";
            this.lblPaymentMagazineBirimFiyat.Size = new System.Drawing.Size(17, 20);
            this.lblPaymentMagazineBirimFiyat.TabIndex = 17;
            this.lblPaymentMagazineBirimFiyat.Text = "0";
            // 
            // lblPaymentMagazineName
            // 
            this.lblPaymentMagazineName.AutoEllipsis = true;
            this.lblPaymentMagazineName.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentMagazineName.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentMagazineName.Location = new System.Drawing.Point(121, 0);
            this.lblPaymentMagazineName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentMagazineName.Name = "lblPaymentMagazineName";
            this.lblPaymentMagazineName.Size = new System.Drawing.Size(383, 63);
            this.lblPaymentMagazineName.TabIndex = 21;
            this.lblPaymentMagazineName.Text = "Dergi";
            this.lblPaymentMagazineName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBPaymentMagazine
            // 
            this.PBPaymentMagazine.ErrorImage = null;
            this.PBPaymentMagazine.Location = new System.Drawing.Point(16, 16);
            this.PBPaymentMagazine.Margin = new System.Windows.Forms.Padding(2);
            this.PBPaymentMagazine.Name = "PBPaymentMagazine";
            this.PBPaymentMagazine.Size = new System.Drawing.Size(92, 100);
            this.PBPaymentMagazine.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBPaymentMagazine.TabIndex = 11;
            this.PBPaymentMagazine.TabStop = false;
            // 
            // UC_PaymentItem_Magazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblPaymentMagazineName);
            this.Controls.Add(this.PBPaymentMagazine);
            this.Controls.Add(this.lblPaymentMagazineAdet);
            this.Controls.Add(this.lblPaymentMagazineToplam);
            this.Controls.Add(this.lblPaymentMagazineCount);
            this.Controls.Add(this.lblPaymentMagazineBirimFiyat);
            this.Controls.Add(this.lblPaymentMagazineBirim);
            this.Controls.Add(this.lblPaymentMagazineToplamFiyat);
            this.Name = "UC_PaymentItem_Magazine";
            this.Size = new System.Drawing.Size(504, 130);
            ((System.ComponentModel.ISupportInitialize)(this.PBPaymentMagazine)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBPaymentMagazine;
        private System.Windows.Forms.Label lblPaymentMagazineAdet;
        private System.Windows.Forms.Label lblPaymentMagazineToplam;
        private System.Windows.Forms.Label lblPaymentMagazineCount;
        private System.Windows.Forms.Label lblPaymentMagazineBirim;
        private System.Windows.Forms.Label lblPaymentMagazineToplamFiyat;
        private System.Windows.Forms.Label lblPaymentMagazineBirimFiyat;
        private System.Windows.Forms.Label lblPaymentMagazineName;
    }
}
