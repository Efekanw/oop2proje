﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Yunus Akyol
*  @number  : 152120181045
*  @mail    : yunusakyol5@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes customers' informations.
*/
namespace OOP2Proje
{
    public class Customer
    {
        private int customerID;
        private string name;
        private string address;
        private string email;
        private string username;
        private string password;
        /// <summary>
        /// Default Constructor for User Class.
        /// </summary>
        public Customer() { }
        /// <summary>
        /// Constructor for user class with parameters.
        /// </summary>
        /// <param name="_name">Takes customer's name.</param>
        /// <param name="_address">Takes customer's address.</param>
        /// <param name="_email">Takes customer's email addres.</param>
        /// <param name="_username">Takes username for login.</param>
        /// <param name="_password">Takes password for login.</param>
        /// <param name="_adminAccess">Takes bool variable for checking if it is admin or not.</param>
        public Customer(string _name, string _address, string _email, string _username, string _password)
        {
            name = _name;
            address = _address;
            email = _email;
            username = _username;
            password = _password;
        }
        /// <summary>
        /// Getter setter for customer ID.
        /// </summary>
        public int CustomerID { get => customerID; set => customerID = value; }
        /// <summary>
        /// Getter setter for customer's name.
        /// </summary>
        public string Name { get => name; set => name = value; }
        /// <summary>
        /// Getter setter for customer's address.
        /// </summary>
        public string Address { get => address; set => address = value; }
        /// <summary>
        /// Getter setter for customer's email.
        /// </summary>
        public string Email { get => email; set => email = value; }
        /// <summary>
        /// Getter setter for customer's username.
        /// </summary>
        public string Username { get => username; set => username = value; }
        /// <summary>
        /// Getter setter for customer's password.
        /// </summary>
        public string Password { get => password; set => password = value; }
        /// <summary>
        /// This function provides to check if user is valid or not by comparing usernames and hashing passwords.
        /// </summary>
        /// <param name="_username">Takes username for compare.</param>
        /// <param name="_password">Takes password for compare.</param>
        /// <returns></returns>
        public bool isValid(string _username, string _password)
        {
            return (this.username == _username && this.password == Util.hashingPassword(_password));
        }
        public void printCustomerDetails()
        {
            //forma göre yapılacak
        }
        /// <summary>
        /// This function provides to set customer into the database.
        /// </summary>
        /// <param name="customer">Takes new customer for adding database.</param>
        public void saveCustomer(Customer customer)
        {
            DataBase db = new DataBase();
            db.saveCustomerToDB(customer);
        }
        public void printCustomerPurchases()
        {
            //purchase işlemlerine göre yapılacak
        }
    }
}
