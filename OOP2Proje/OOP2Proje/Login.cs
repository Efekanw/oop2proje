﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Yunus Akyol
*  @number  : 152120181045
*  @mail    : yunusakyol5@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes login form.
*/
namespace OOP2Proje
{
    public partial class Login : Form
    {
        public Login()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This function implements button click event for login screen.
        /// </summary>
        private void btnLogin_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtBoxLoginUsername.Text) || string.IsNullOrEmpty(txtBoxLoginPassword.Text))
                {
                    MessageBox.Show("Boş Alan Tespit Edildi !", "Boş Alan Bırakılamaz !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                Customer loginedUser = new Customer();
                loginedUser.Username = txtBoxLoginUsername.Text;
                loginedUser.Password = Util.hashingPassword(txtBoxLoginPassword.Text);
                if (!Register.userList.Any(user => user.Username == loginedUser.Username))
                {
                    MessageBox.Show("Girilen Kullanıcı Bulunamadı !", "Kullanıcı Bulunamadı !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                loginedUser = Register.userList.Find(user => user.Username == loginedUser.Username);
                if (!loginedUser.isValid(txtBoxLoginUsername.Text, txtBoxLoginPassword.Text))
                {
                    MessageBox.Show("Girilen Şifre Yanlış Lütfen Tekrar Deneyin !", "Şifre Yanlış !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                SingletonLoginedCustomer.getInstance().Customer = loginedUser;
                MessageBox.Show("Giriş Başarılı !", "Başarılı Giriş !", MessageBoxButtons.OK);
                mainStore mainStore = new mainStore();
                this.Hide();
                mainStore.ShowDialog();
                this.Close();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function provides to implement link click event for register screen.
        /// </summary>
        private void lnkLblSign_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            lnkLblSign.LinkVisited = true;
            convertFormLogin();
        }
        /// <summary>
        /// This function implements form closing event for login screen.
        /// </summary>
        private void Login_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }
        /// <summary>
        /// This function provides to close this form and opens register form.
        /// </summary>
        private void convertFormLogin()
        {
            Register register = new Register();
            this.Hide();
            register.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// This function clears text boxes.
        /// </summary>
        private void clearFields()
        {
            txtBoxLoginUsername.Text = "";
            txtBoxLoginPassword.Text = "";
        }
        /// <summary>
        /// This function provides to set datas in the database to the registered customers list.
        /// </summary>
        private void Login_Load(object sender, EventArgs e)
        {
            //Customer admin = new Customer("admin", "Denizli", "admin@gmail.com", "admin",Util.hashingPassword("123"));
            //admin.CustomerID = 0;
            DataBase db = new DataBase();
            //db.saveCustomerToDB(admin);
            Register.userList = db.getListOfCustomerFromDB();
        }
    }
}
