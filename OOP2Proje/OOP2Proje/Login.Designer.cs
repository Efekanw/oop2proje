﻿
namespace OOP2Proje
{
    partial class Login
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Login));
            this.lblLoginTitle = new System.Windows.Forms.Label();
            this.txtBoxLoginUsername = new System.Windows.Forms.TextBox();
            this.txtBoxLoginPassword = new System.Windows.Forms.TextBox();
            this.lblLoginUsername = new System.Windows.Forms.Label();
            this.lblLoginPassword = new System.Windows.Forms.Label();
            this.btnLogin = new System.Windows.Forms.Button();
            this.lnkLblSign = new System.Windows.Forms.LinkLabel();
            this.picBoxLogin = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogin)).BeginInit();
            this.SuspendLayout();
            // 
            // lblLoginTitle
            // 
            this.lblLoginTitle.AutoSize = true;
            this.lblLoginTitle.Font = new System.Drawing.Font("Palatino Linotype", 26.25F);
            this.lblLoginTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
            this.lblLoginTitle.Location = new System.Drawing.Point(253, 41);
            this.lblLoginTitle.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLoginTitle.Name = "lblLoginTitle";
            this.lblLoginTitle.Size = new System.Drawing.Size(394, 118);
            this.lblLoginTitle.TabIndex = 0;
            this.lblLoginTitle.Text = "SİGMA KİTABEVİ\r\n      GİRİŞ YAP";
            // 
            // txtBoxLoginUsername
            // 
            this.txtBoxLoginUsername.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxLoginUsername.Location = new System.Drawing.Point(404, 235);
            this.txtBoxLoginUsername.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxLoginUsername.Name = "txtBoxLoginUsername";
            this.txtBoxLoginUsername.Size = new System.Drawing.Size(132, 26);
            this.txtBoxLoginUsername.TabIndex = 1;
            // 
            // txtBoxLoginPassword
            // 
            this.txtBoxLoginPassword.Font = new System.Drawing.Font("Palatino Linotype", 8.25F);
            this.txtBoxLoginPassword.Location = new System.Drawing.Point(404, 292);
            this.txtBoxLoginPassword.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.txtBoxLoginPassword.Name = "txtBoxLoginPassword";
            this.txtBoxLoginPassword.PasswordChar = '*';
            this.txtBoxLoginPassword.Size = new System.Drawing.Size(132, 26);
            this.txtBoxLoginPassword.TabIndex = 2;
            // 
            // lblLoginUsername
            // 
            this.lblLoginUsername.AutoSize = true;
            this.lblLoginUsername.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblLoginUsername.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblLoginUsername.Location = new System.Drawing.Point(259, 234);
            this.lblLoginUsername.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLoginUsername.Name = "lblLoginUsername";
            this.lblLoginUsername.Size = new System.Drawing.Size(129, 26);
            this.lblLoginUsername.TabIndex = 3;
            this.lblLoginUsername.Text = "Kullanıcı Adı :";
            // 
            // lblLoginPassword
            // 
            this.lblLoginPassword.AutoSize = true;
            this.lblLoginPassword.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lblLoginPassword.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.lblLoginPassword.Location = new System.Drawing.Point(332, 290);
            this.lblLoginPassword.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lblLoginPassword.Name = "lblLoginPassword";
            this.lblLoginPassword.Size = new System.Drawing.Size(59, 26);
            this.lblLoginPassword.TabIndex = 4;
            this.lblLoginPassword.Text = "Şifre :";
            // 
            // btnLogin
            // 
            this.btnLogin.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.btnLogin.ForeColor = System.Drawing.Color.Green;
            this.btnLogin.Location = new System.Drawing.Point(416, 372);
            this.btnLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.btnLogin.Name = "btnLogin";
            this.btnLogin.Size = new System.Drawing.Size(100, 50);
            this.btnLogin.TabIndex = 5;
            this.btnLogin.Text = "Giriş";
            this.btnLogin.UseVisualStyleBackColor = true;
            this.btnLogin.Click += new System.EventHandler(this.btnLogin_Click);
            // 
            // lnkLblSign
            // 
            this.lnkLblSign.AutoSize = true;
            this.lnkLblSign.Font = new System.Drawing.Font("Palatino Linotype", 11.25F);
            this.lnkLblSign.LinkColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(128)))), ((int)(((byte)(255)))));
            this.lnkLblSign.Location = new System.Drawing.Point(397, 457);
            this.lnkLblSign.Margin = new System.Windows.Forms.Padding(4, 0, 4, 0);
            this.lnkLblSign.Name = "lnkLblSign";
            this.lnkLblSign.Size = new System.Drawing.Size(132, 26);
            this.lnkLblSign.TabIndex = 6;
            this.lnkLblSign.TabStop = true;
            this.lnkLblSign.Text = "Hesap Oluştur";
            this.lnkLblSign.LinkClicked += new System.Windows.Forms.LinkLabelLinkClickedEventHandler(this.lnkLblSign_LinkClicked);
            // 
            // picBoxLogin
            // 
            this.picBoxLogin.BackgroundImage = global::OOP2Proje.Properties.Resources.sigma;
            this.picBoxLogin.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.picBoxLogin.ErrorImage = null;
            this.picBoxLogin.InitialImage = null;
            this.picBoxLogin.Location = new System.Drawing.Point(41, 26);
            this.picBoxLogin.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.picBoxLogin.Name = "picBoxLogin";
            this.picBoxLogin.Size = new System.Drawing.Size(149, 148);
            this.picBoxLogin.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.picBoxLogin.TabIndex = 7;
            this.picBoxLogin.TabStop = false;
            // 
            // Login
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.ClientSize = new System.Drawing.Size(875, 482);
            this.Controls.Add(this.picBoxLogin);
            this.Controls.Add(this.lnkLblSign);
            this.Controls.Add(this.btnLogin);
            this.Controls.Add(this.lblLoginPassword);
            this.Controls.Add(this.lblLoginUsername);
            this.Controls.Add(this.txtBoxLoginPassword);
            this.Controls.Add(this.txtBoxLoginUsername);
            this.Controls.Add(this.lblLoginTitle);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(4, 4, 4, 4);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(893, 529);
            this.MinimumSize = new System.Drawing.Size(893, 529);
            this.Name = "Login";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Giriş Ekranı";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Login_FormClosing);
            this.Load += new System.EventHandler(this.Login_Load);
            ((System.ComponentModel.ISupportInitialize)(this.picBoxLogin)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label lblLoginTitle;
        private System.Windows.Forms.TextBox txtBoxLoginUsername;
        private System.Windows.Forms.TextBox txtBoxLoginPassword;
        private System.Windows.Forms.Label lblLoginUsername;
        private System.Windows.Forms.Label lblLoginPassword;
        private System.Windows.Forms.Button btnLogin;
        private System.Windows.Forms.LinkLabel lnkLblSign;
        private System.Windows.Forms.PictureBox picBoxLogin;
    }
}