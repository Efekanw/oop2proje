﻿
namespace OOP2Proje
{
    partial class mainStore
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(mainStore));
            this.BtnBook = new System.Windows.Forms.Button();
            this.FlowLMain = new System.Windows.Forms.FlowLayoutPanel();
            this.LblGoCart = new System.Windows.Forms.Label();
            this.GBMusic = new System.Windows.Forms.GroupBox();
            this.BtnMusicPop = new System.Windows.Forms.Button();
            this.BtnMusicArabesk = new System.Windows.Forms.Button();
            this.BtnMusicTSM = new System.Windows.Forms.Button();
            this.BtnMusicRock = new System.Windows.Forms.Button();
            this.GBMag = new System.Windows.Forms.GroupBox();
            this.BtnMagMizah = new System.Windows.Forms.Button();
            this.BtnMagKS = new System.Windows.Forms.Button();
            this.BtnMagEdebiyat = new System.Windows.Forms.Button();
            this.BtnMagCog = new System.Windows.Forms.Button();
            this.BtnMagModa = new System.Windows.Forms.Button();
            this.BtnMagSpor = new System.Windows.Forms.Button();
            this.BtnMagPsiko = new System.Windows.Forms.Button();
            this.BtnMagCocuk = new System.Windows.Forms.Button();
            this.BtnMagBT = new System.Windows.Forms.Button();
            this.BtnMagBilim = new System.Windows.Forms.Button();
            this.GBBook = new System.Windows.Forms.GroupBox();
            this.BtnBookPolisiye = new System.Windows.Forms.Button();
            this.BtnBookBilim = new System.Windows.Forms.Button();
            this.BtnBookKG = new System.Windows.Forms.Button();
            this.BtnBookRoman = new System.Windows.Forms.Button();
            this.BtnBookDT = new System.Windows.Forms.Button();
            this.BtnBookBK = new System.Windows.Forms.Button();
            this.BtnBookKorku = new System.Windows.Forms.Button();
            this.BtnBookCR = new System.Windows.Forms.Button();
            this.BtnBookDK = new System.Windows.Forms.Button();
            this.BtnBookFantastik = new System.Windows.Forms.Button();
            this.BtnBookDram = new System.Windows.Forms.Button();
            this.BtnBookCocuk = new System.Windows.Forms.Button();
            this.BtnBookDistopya = new System.Windows.Forms.Button();
            this.BtnBookFelsefe = new System.Windows.Forms.Button();
            this.BtnMagazine = new System.Windows.Forms.Button();
            this.BtnMusic = new System.Windows.Forms.Button();
            this.LblSatanlar = new System.Windows.Forms.Label();
            this.LblBookStore = new System.Windows.Forms.Label();
            this.LblSearch = new System.Windows.Forms.Label();
            this.TxtSearch = new System.Windows.Forms.TextBox();
            this.LblAmount = new System.Windows.Forms.Label();
            this.LblCount = new System.Windows.Forms.Label();
            this.BtnBack = new System.Windows.Forms.Button();
            this.LblDateTime = new System.Windows.Forms.Label();
            this.timerMain = new System.Windows.Forms.Timer(this.components);
            this.LblTime = new System.Windows.Forms.Label();
            this.PBCart = new System.Windows.Forms.PictureBox();
            this.PBSigma = new System.Windows.Forms.PictureBox();
            this.GBMusic.SuspendLayout();
            this.GBMag.SuspendLayout();
            this.GBBook.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PBCart)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBSigma)).BeginInit();
            this.SuspendLayout();
            // 
            // BtnBook
            // 
            this.BtnBook.BackColor = System.Drawing.Color.Bisque;
            this.BtnBook.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBook.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBook.Location = new System.Drawing.Point(-2, 120);
            this.BtnBook.Name = "BtnBook";
            this.BtnBook.Size = new System.Drawing.Size(273, 60);
            this.BtnBook.TabIndex = 1;
            this.BtnBook.Text = "Kitap";
            this.BtnBook.UseVisualStyleBackColor = false;
            this.BtnBook.Click += new System.EventHandler(this.BtnBook_Click);
            // 
            // FlowLMain
            // 
            this.FlowLMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.FlowLMain.AutoScroll = true;
            this.FlowLMain.Location = new System.Drawing.Point(270, 120);
            this.FlowLMain.Name = "FlowLMain";
            this.FlowLMain.Size = new System.Drawing.Size(1250, 600);
            this.FlowLMain.TabIndex = 3;
            // 
            // LblGoCart
            // 
            this.LblGoCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblGoCart.AutoSize = true;
            this.LblGoCart.BackColor = System.Drawing.Color.Transparent;
            this.LblGoCart.Font = new System.Drawing.Font("Palatino Linotype", 13.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblGoCart.ForeColor = System.Drawing.Color.Gold;
            this.LblGoCart.Location = new System.Drawing.Point(1208, 83);
            this.LblGoCart.Name = "LblGoCart";
            this.LblGoCart.Size = new System.Drawing.Size(117, 29);
            this.LblGoCart.TabIndex = 49;
            this.LblGoCart.Text = "Sepete Git";
            // 
            // GBMusic
            // 
            this.GBMusic.Controls.Add(this.BtnMusicPop);
            this.GBMusic.Controls.Add(this.BtnMusicArabesk);
            this.GBMusic.Controls.Add(this.BtnMusicTSM);
            this.GBMusic.Controls.Add(this.BtnMusicRock);
            this.GBMusic.Location = new System.Drawing.Point(-2, 303);
            this.GBMusic.Name = "GBMusic";
            this.GBMusic.Size = new System.Drawing.Size(274, 415);
            this.GBMusic.TabIndex = 39;
            this.GBMusic.TabStop = false;
            this.GBMusic.Visible = false;
            // 
            // BtnMusicPop
            // 
            this.BtnMusicPop.BackColor = System.Drawing.Color.Snow;
            this.BtnMusicPop.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusicPop.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMusicPop.Location = new System.Drawing.Point(0, 11);
            this.BtnMusicPop.Name = "BtnMusicPop";
            this.BtnMusicPop.Size = new System.Drawing.Size(137, 59);
            this.BtnMusicPop.TabIndex = 35;
            this.BtnMusicPop.Text = "Pop";
            this.BtnMusicPop.UseVisualStyleBackColor = false;
            // 
            // BtnMusicArabesk
            // 
            this.BtnMusicArabesk.BackColor = System.Drawing.Color.Snow;
            this.BtnMusicArabesk.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusicArabesk.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMusicArabesk.Location = new System.Drawing.Point(137, 67);
            this.BtnMusicArabesk.Name = "BtnMusicArabesk";
            this.BtnMusicArabesk.Size = new System.Drawing.Size(137, 59);
            this.BtnMusicArabesk.TabIndex = 38;
            this.BtnMusicArabesk.Text = "Arabesk";
            this.BtnMusicArabesk.UseVisualStyleBackColor = false;
            // 
            // BtnMusicTSM
            // 
            this.BtnMusicTSM.BackColor = System.Drawing.Color.Snow;
            this.BtnMusicTSM.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusicTSM.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMusicTSM.Location = new System.Drawing.Point(137, 11);
            this.BtnMusicTSM.Name = "BtnMusicTSM";
            this.BtnMusicTSM.Size = new System.Drawing.Size(137, 59);
            this.BtnMusicTSM.TabIndex = 36;
            this.BtnMusicTSM.Text = "Türk Sanat Müzikleri";
            this.BtnMusicTSM.UseVisualStyleBackColor = false;
            // 
            // BtnMusicRock
            // 
            this.BtnMusicRock.BackColor = System.Drawing.Color.Snow;
            this.BtnMusicRock.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusicRock.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMusicRock.Location = new System.Drawing.Point(0, 67);
            this.BtnMusicRock.Name = "BtnMusicRock";
            this.BtnMusicRock.Size = new System.Drawing.Size(137, 59);
            this.BtnMusicRock.TabIndex = 37;
            this.BtnMusicRock.Text = "Rock";
            this.BtnMusicRock.UseVisualStyleBackColor = false;
            // 
            // GBMag
            // 
            this.GBMag.Controls.Add(this.BtnMagMizah);
            this.GBMag.Controls.Add(this.BtnMagKS);
            this.GBMag.Controls.Add(this.BtnMagEdebiyat);
            this.GBMag.Controls.Add(this.BtnMagCog);
            this.GBMag.Controls.Add(this.BtnMagModa);
            this.GBMag.Controls.Add(this.BtnMagSpor);
            this.GBMag.Controls.Add(this.BtnMagPsiko);
            this.GBMag.Controls.Add(this.BtnMagCocuk);
            this.GBMag.Controls.Add(this.BtnMagBT);
            this.GBMag.Controls.Add(this.BtnMagBilim);
            this.GBMag.Location = new System.Drawing.Point(-2, 303);
            this.GBMag.Name = "GBMag";
            this.GBMag.Size = new System.Drawing.Size(274, 415);
            this.GBMag.TabIndex = 32;
            this.GBMag.TabStop = false;
            this.GBMag.Visible = false;
            // 
            // BtnMagMizah
            // 
            this.BtnMagMizah.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagMizah.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagMizah.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagMizah.Location = new System.Drawing.Point(0, 11);
            this.BtnMagMizah.Name = "BtnMagMizah";
            this.BtnMagMizah.Size = new System.Drawing.Size(137, 59);
            this.BtnMagMizah.TabIndex = 19;
            this.BtnMagMizah.Text = "Mizah";
            this.BtnMagMizah.UseVisualStyleBackColor = false;
            // 
            // BtnMagKS
            // 
            this.BtnMagKS.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagKS.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagKS.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagKS.Location = new System.Drawing.Point(137, 235);
            this.BtnMagKS.Name = "BtnMagKS";
            this.BtnMagKS.Size = new System.Drawing.Size(137, 59);
            this.BtnMagKS.TabIndex = 31;
            this.BtnMagKS.Text = "Kültür Sanat";
            this.BtnMagKS.UseVisualStyleBackColor = false;
            // 
            // BtnMagEdebiyat
            // 
            this.BtnMagEdebiyat.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagEdebiyat.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagEdebiyat.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagEdebiyat.Location = new System.Drawing.Point(137, 179);
            this.BtnMagEdebiyat.Name = "BtnMagEdebiyat";
            this.BtnMagEdebiyat.Size = new System.Drawing.Size(137, 59);
            this.BtnMagEdebiyat.TabIndex = 17;
            this.BtnMagEdebiyat.Text = "Edebiyat";
            this.BtnMagEdebiyat.UseVisualStyleBackColor = false;
            // 
            // BtnMagCog
            // 
            this.BtnMagCog.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagCog.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagCog.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagCog.Location = new System.Drawing.Point(137, 11);
            this.BtnMagCog.Name = "BtnMagCog";
            this.BtnMagCog.Size = new System.Drawing.Size(137, 59);
            this.BtnMagCog.TabIndex = 30;
            this.BtnMagCog.Text = "Coğrafya";
            this.BtnMagCog.UseVisualStyleBackColor = false;
            // 
            // BtnMagModa
            // 
            this.BtnMagModa.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagModa.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagModa.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagModa.Location = new System.Drawing.Point(0, 179);
            this.BtnMagModa.Name = "BtnMagModa";
            this.BtnMagModa.Size = new System.Drawing.Size(137, 59);
            this.BtnMagModa.TabIndex = 18;
            this.BtnMagModa.Text = "Moda";
            this.BtnMagModa.UseVisualStyleBackColor = false;
            // 
            // BtnMagSpor
            // 
            this.BtnMagSpor.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagSpor.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagSpor.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagSpor.Location = new System.Drawing.Point(137, 123);
            this.BtnMagSpor.Name = "BtnMagSpor";
            this.BtnMagSpor.Size = new System.Drawing.Size(137, 59);
            this.BtnMagSpor.TabIndex = 20;
            this.BtnMagSpor.Text = "Spor";
            this.BtnMagSpor.UseVisualStyleBackColor = false;
            // 
            // BtnMagPsiko
            // 
            this.BtnMagPsiko.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagPsiko.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagPsiko.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagPsiko.Location = new System.Drawing.Point(0, 67);
            this.BtnMagPsiko.Name = "BtnMagPsiko";
            this.BtnMagPsiko.Size = new System.Drawing.Size(137, 59);
            this.BtnMagPsiko.TabIndex = 29;
            this.BtnMagPsiko.Text = "Psikoloji";
            this.BtnMagPsiko.UseVisualStyleBackColor = false;
            // 
            // BtnMagCocuk
            // 
            this.BtnMagCocuk.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagCocuk.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagCocuk.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagCocuk.Location = new System.Drawing.Point(0, 235);
            this.BtnMagCocuk.Name = "BtnMagCocuk";
            this.BtnMagCocuk.Size = new System.Drawing.Size(137, 59);
            this.BtnMagCocuk.TabIndex = 25;
            this.BtnMagCocuk.Text = "Çocuk";
            this.BtnMagCocuk.UseVisualStyleBackColor = false;
            // 
            // BtnMagBT
            // 
            this.BtnMagBT.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagBT.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagBT.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagBT.Location = new System.Drawing.Point(137, 67);
            this.BtnMagBT.Name = "BtnMagBT";
            this.BtnMagBT.Size = new System.Drawing.Size(137, 59);
            this.BtnMagBT.TabIndex = 28;
            this.BtnMagBT.Text = "Bilim ve Teknoloji";
            this.BtnMagBT.UseVisualStyleBackColor = false;
            // 
            // BtnMagBilim
            // 
            this.BtnMagBilim.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagBilim.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagBilim.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagBilim.Location = new System.Drawing.Point(0, 123);
            this.BtnMagBilim.Name = "BtnMagBilim";
            this.BtnMagBilim.Size = new System.Drawing.Size(137, 59);
            this.BtnMagBilim.TabIndex = 27;
            this.BtnMagBilim.Text = "Bilim";
            this.BtnMagBilim.UseVisualStyleBackColor = false;
            // 
            // GBBook
            // 
            this.GBBook.Controls.Add(this.BtnBookPolisiye);
            this.GBBook.Controls.Add(this.BtnBookBilim);
            this.GBBook.Controls.Add(this.BtnBookKG);
            this.GBBook.Controls.Add(this.BtnBookRoman);
            this.GBBook.Controls.Add(this.BtnBookDT);
            this.GBBook.Controls.Add(this.BtnBookBK);
            this.GBBook.Controls.Add(this.BtnBookKorku);
            this.GBBook.Controls.Add(this.BtnBookCR);
            this.GBBook.Controls.Add(this.BtnBookDK);
            this.GBBook.Controls.Add(this.BtnBookFantastik);
            this.GBBook.Controls.Add(this.BtnBookDram);
            this.GBBook.Controls.Add(this.BtnBookCocuk);
            this.GBBook.Controls.Add(this.BtnBookDistopya);
            this.GBBook.Controls.Add(this.BtnBookFelsefe);
            this.GBBook.Location = new System.Drawing.Point(-2, 303);
            this.GBBook.Name = "GBBook";
            this.GBBook.Size = new System.Drawing.Size(274, 415);
            this.GBBook.TabIndex = 33;
            this.GBBook.TabStop = false;
            this.GBBook.Visible = false;
            // 
            // BtnBookPolisiye
            // 
            this.BtnBookPolisiye.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookPolisiye.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookPolisiye.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookPolisiye.Location = new System.Drawing.Point(0, 11);
            this.BtnBookPolisiye.Name = "BtnBookPolisiye";
            this.BtnBookPolisiye.Size = new System.Drawing.Size(137, 59);
            this.BtnBookPolisiye.TabIndex = 6;
            this.BtnBookPolisiye.Text = "Polisiye";
            this.BtnBookPolisiye.UseVisualStyleBackColor = false;
            // 
            // BtnBookBilim
            // 
            this.BtnBookBilim.Location = new System.Drawing.Point(7, 429);
            this.BtnBookBilim.Name = "BtnBookBilim";
            this.BtnBookBilim.Size = new System.Drawing.Size(127, 24);
            this.BtnBookBilim.TabIndex = 13;
            this.BtnBookBilim.Text = "Bilim";
            this.BtnBookBilim.UseVisualStyleBackColor = true;
            // 
            // BtnBookKG
            // 
            this.BtnBookKG.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookKG.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookKG.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookKG.Location = new System.Drawing.Point(0, 67);
            this.BtnBookKG.Name = "BtnBookKG";
            this.BtnBookKG.Size = new System.Drawing.Size(137, 59);
            this.BtnBookKG.TabIndex = 7;
            this.BtnBookKG.Text = "Kişisel Gelişim";
            this.BtnBookKG.UseVisualStyleBackColor = false;
            // 
            // BtnBookRoman
            // 
            this.BtnBookRoman.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookRoman.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookRoman.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookRoman.Location = new System.Drawing.Point(0, 347);
            this.BtnBookRoman.Name = "BtnBookRoman";
            this.BtnBookRoman.Size = new System.Drawing.Size(137, 59);
            this.BtnBookRoman.TabIndex = 16;
            this.BtnBookRoman.Text = "Roman";
            this.BtnBookRoman.UseVisualStyleBackColor = false;
            // 
            // BtnBookDT
            // 
            this.BtnBookDT.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookDT.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookDT.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookDT.Location = new System.Drawing.Point(0, 235);
            this.BtnBookDT.Name = "BtnBookDT";
            this.BtnBookDT.Size = new System.Drawing.Size(137, 59);
            this.BtnBookDT.TabIndex = 0;
            this.BtnBookDT.Text = "Dünya Tarihi";
            this.BtnBookDT.UseVisualStyleBackColor = false;
            // 
            // BtnBookBK
            // 
            this.BtnBookBK.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookBK.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookBK.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookBK.Location = new System.Drawing.Point(137, 291);
            this.BtnBookBK.Name = "BtnBookBK";
            this.BtnBookBK.Size = new System.Drawing.Size(137, 59);
            this.BtnBookBK.TabIndex = 14;
            this.BtnBookBK.Text = "Bilim Kurgu";
            this.BtnBookBK.UseVisualStyleBackColor = false;
            // 
            // BtnBookKorku
            // 
            this.BtnBookKorku.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookKorku.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookKorku.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookKorku.Location = new System.Drawing.Point(137, 67);
            this.BtnBookKorku.Name = "BtnBookKorku";
            this.BtnBookKorku.Size = new System.Drawing.Size(137, 59);
            this.BtnBookKorku.TabIndex = 8;
            this.BtnBookKorku.Text = "Korku";
            this.BtnBookKorku.UseVisualStyleBackColor = false;
            // 
            // BtnBookCR
            // 
            this.BtnBookCR.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookCR.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookCR.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookCR.Location = new System.Drawing.Point(0, 291);
            this.BtnBookCR.Name = "BtnBookCR";
            this.BtnBookCR.Size = new System.Drawing.Size(137, 59);
            this.BtnBookCR.TabIndex = 15;
            this.BtnBookCR.Text = "Çizgi Roman";
            this.BtnBookCR.UseVisualStyleBackColor = false;
            // 
            // BtnBookDK
            // 
            this.BtnBookDK.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookDK.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookDK.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookDK.Location = new System.Drawing.Point(137, 123);
            this.BtnBookDK.Name = "BtnBookDK";
            this.BtnBookDK.Size = new System.Drawing.Size(137, 59);
            this.BtnBookDK.TabIndex = 9;
            this.BtnBookDK.Text = "Dünya Klasikleri";
            this.BtnBookDK.UseVisualStyleBackColor = false;
            // 
            // BtnBookFantastik
            // 
            this.BtnBookFantastik.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookFantastik.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookFantastik.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookFantastik.Location = new System.Drawing.Point(137, 235);
            this.BtnBookFantastik.Name = "BtnBookFantastik";
            this.BtnBookFantastik.Size = new System.Drawing.Size(137, 59);
            this.BtnBookFantastik.TabIndex = 12;
            this.BtnBookFantastik.Text = "Fantastik";
            this.BtnBookFantastik.UseVisualStyleBackColor = false;
            // 
            // BtnBookDram
            // 
            this.BtnBookDram.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookDram.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookDram.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookDram.Location = new System.Drawing.Point(0, 123);
            this.BtnBookDram.Name = "BtnBookDram";
            this.BtnBookDram.Size = new System.Drawing.Size(137, 59);
            this.BtnBookDram.TabIndex = 4;
            this.BtnBookDram.Text = "Dram";
            this.BtnBookDram.UseVisualStyleBackColor = false;
            // 
            // BtnBookCocuk
            // 
            this.BtnBookCocuk.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookCocuk.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookCocuk.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookCocuk.Location = new System.Drawing.Point(137, 11);
            this.BtnBookCocuk.Name = "BtnBookCocuk";
            this.BtnBookCocuk.Size = new System.Drawing.Size(137, 59);
            this.BtnBookCocuk.TabIndex = 11;
            this.BtnBookCocuk.Text = "Çocuk";
            this.BtnBookCocuk.UseVisualStyleBackColor = false;
            // 
            // BtnBookDistopya
            // 
            this.BtnBookDistopya.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookDistopya.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookDistopya.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookDistopya.Location = new System.Drawing.Point(0, 179);
            this.BtnBookDistopya.Name = "BtnBookDistopya";
            this.BtnBookDistopya.Size = new System.Drawing.Size(137, 59);
            this.BtnBookDistopya.TabIndex = 5;
            this.BtnBookDistopya.Text = "Distopya";
            this.BtnBookDistopya.UseVisualStyleBackColor = false;
            // 
            // BtnBookFelsefe
            // 
            this.BtnBookFelsefe.BackColor = System.Drawing.Color.Bisque;
            this.BtnBookFelsefe.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnBookFelsefe.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnBookFelsefe.Location = new System.Drawing.Point(137, 179);
            this.BtnBookFelsefe.Name = "BtnBookFelsefe";
            this.BtnBookFelsefe.Size = new System.Drawing.Size(137, 59);
            this.BtnBookFelsefe.TabIndex = 10;
            this.BtnBookFelsefe.Text = "Felsefe";
            this.BtnBookFelsefe.UseVisualStyleBackColor = false;
            // 
            // BtnMagazine
            // 
            this.BtnMagazine.BackColor = System.Drawing.Color.MistyRose;
            this.BtnMagazine.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagazine.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMagazine.Location = new System.Drawing.Point(-2, 178);
            this.BtnMagazine.Name = "BtnMagazine";
            this.BtnMagazine.Size = new System.Drawing.Size(273, 60);
            this.BtnMagazine.TabIndex = 17;
            this.BtnMagazine.Text = "Dergi";
            this.BtnMagazine.UseVisualStyleBackColor = false;
            this.BtnMagazine.Click += new System.EventHandler(this.BtnMagazine_Click);
            // 
            // BtnMusic
            // 
            this.BtnMusic.BackColor = System.Drawing.Color.Snow;
            this.BtnMusic.Font = new System.Drawing.Font("Palatino Linotype", 16.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusic.ForeColor = System.Drawing.Color.IndianRed;
            this.BtnMusic.Location = new System.Drawing.Point(-2, 237);
            this.BtnMusic.Name = "BtnMusic";
            this.BtnMusic.Size = new System.Drawing.Size(273, 60);
            this.BtnMusic.TabIndex = 34;
            this.BtnMusic.Text = "Müzik";
            this.BtnMusic.UseVisualStyleBackColor = false;
            this.BtnMusic.Click += new System.EventHandler(this.BtnMusic_Click);
            // 
            // LblSatanlar
            // 
            this.LblSatanlar.AutoSize = true;
            this.LblSatanlar.BackColor = System.Drawing.Color.Transparent;
            this.LblSatanlar.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LblSatanlar.Font = new System.Drawing.Font("Palatino Linotype", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblSatanlar.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblSatanlar.Location = new System.Drawing.Point(262, 13);
            this.LblSatanlar.Name = "LblSatanlar";
            this.LblSatanlar.Size = new System.Drawing.Size(215, 45);
            this.LblSatanlar.TabIndex = 40;
            this.LblSatanlar.Text = "Çok Satanlar";
            this.LblSatanlar.Click += new System.EventHandler(this.LblSatanlar_Click);
            // 
            // LblBookStore
            // 
            this.LblBookStore.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblBookStore.BackColor = System.Drawing.Color.Transparent;
            this.LblBookStore.Font = new System.Drawing.Font("Palatino Linotype", 40.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblBookStore.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblBookStore.Location = new System.Drawing.Point(648, 5);
            this.LblBookStore.Name = "LblBookStore";
            this.LblBookStore.Size = new System.Drawing.Size(554, 112);
            this.LblBookStore.TabIndex = 41;
            this.LblBookStore.Text = "Sigma Kitabevi";
            this.LblBookStore.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblSearch
            // 
            this.LblSearch.AutoSize = true;
            this.LblSearch.BackColor = System.Drawing.Color.Transparent;
            this.LblSearch.Font = new System.Drawing.Font("Palatino Linotype", 19.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblSearch.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblSearch.Location = new System.Drawing.Point(262, 72);
            this.LblSearch.Name = "LblSearch";
            this.LblSearch.Size = new System.Drawing.Size(122, 45);
            this.LblSearch.TabIndex = 43;
            this.LblSearch.Text = "Arama";
            // 
            // TxtSearch
            // 
            this.TxtSearch.BackColor = System.Drawing.Color.IndianRed;
            this.TxtSearch.Font = new System.Drawing.Font("Palatino Linotype", 10.2F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.TxtSearch.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.TxtSearch.Location = new System.Drawing.Point(409, 83);
            this.TxtSearch.Name = "TxtSearch";
            this.TxtSearch.Size = new System.Drawing.Size(100, 30);
            this.TxtSearch.TabIndex = 44;
            this.TxtSearch.TextChanged += new System.EventHandler(this.TxtSearch_TextChanged);
            // 
            // LblAmount
            // 
            this.LblAmount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblAmount.AutoSize = true;
            this.LblAmount.BackColor = System.Drawing.Color.Transparent;
            this.LblAmount.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblAmount.ForeColor = System.Drawing.Color.Gold;
            this.LblAmount.Location = new System.Drawing.Point(1323, 0);
            this.LblAmount.Name = "LblAmount";
            this.LblAmount.Size = new System.Drawing.Size(179, 41);
            this.LblAmount.TabIndex = 47;
            this.LblAmount.Text = "Ürün Adedi";
            // 
            // LblCount
            // 
            this.LblCount.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblCount.AutoSize = true;
            this.LblCount.BackColor = System.Drawing.Color.Transparent;
            this.LblCount.Font = new System.Drawing.Font("Palatino Linotype", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblCount.ForeColor = System.Drawing.Color.Gold;
            this.LblCount.Location = new System.Drawing.Point(1396, 41);
            this.LblCount.Name = "LblCount";
            this.LblCount.Size = new System.Drawing.Size(33, 41);
            this.LblCount.TabIndex = 48;
            this.LblCount.Text = "0";
            // 
            // BtnBack
            // 
            this.BtnBack.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.BtnBack.BackgroundImage = global::OOP2Proje.Properties.Resources.aaa;
            this.BtnBack.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnBack.FlatAppearance.BorderSize = 0;
            this.BtnBack.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnBack.Location = new System.Drawing.Point(5, 0);
            this.BtnBack.Name = "BtnBack";
            this.BtnBack.Size = new System.Drawing.Size(172, 116);
            this.BtnBack.TabIndex = 0;
            this.BtnBack.UseVisualStyleBackColor = false;
            this.BtnBack.Click += new System.EventHandler(this.BtnBack_Click);
            // 
            // LblDateTime
            // 
            this.LblDateTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblDateTime.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblDateTime.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblDateTime.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LblDateTime.Location = new System.Drawing.Point(1343, 81);
            this.LblDateTime.Name = "LblDateTime";
            this.LblDateTime.Size = new System.Drawing.Size(177, 17);
            this.LblDateTime.TabIndex = 50;
            this.LblDateTime.Text = "0";
            this.LblDateTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // timerMain
            // 
            this.timerMain.Tick += new System.EventHandler(this.timerMain_Tick);
            // 
            // LblTime
            // 
            this.LblTime.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.LblTime.Font = new System.Drawing.Font("Palatino Linotype", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblTime.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblTime.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.LblTime.Location = new System.Drawing.Point(1343, 101);
            this.LblTime.Name = "LblTime";
            this.LblTime.Size = new System.Drawing.Size(177, 17);
            this.LblTime.TabIndex = 51;
            this.LblTime.Text = "0";
            this.LblTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBCart
            // 
            this.PBCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PBCart.BackColor = System.Drawing.Color.Transparent;
            this.PBCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PBCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PBCart.ErrorImage = null;
            this.PBCart.Image = global::OOP2Proje.Properties.Resources.goldcart;
            this.PBCart.Location = new System.Drawing.Point(1208, 0);
            this.PBCart.Name = "PBCart";
            this.PBCart.Size = new System.Drawing.Size(109, 82);
            this.PBCart.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBCart.TabIndex = 46;
            this.PBCart.TabStop = false;
            this.PBCart.Click += new System.EventHandler(this.PBCart_Click);
            // 
            // PBSigma
            // 
            this.PBSigma.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.PBSigma.BackColor = System.Drawing.Color.Transparent;
            this.PBSigma.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.PBSigma.ErrorImage = null;
            this.PBSigma.Image = global::OOP2Proje.Properties.Resources.sigma_svg;
            this.PBSigma.Location = new System.Drawing.Point(543, 0);
            this.PBSigma.Name = "PBSigma";
            this.PBSigma.Size = new System.Drawing.Size(127, 123);
            this.PBSigma.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBSigma.TabIndex = 45;
            this.PBSigma.TabStop = false;
            // 
            // mainStore
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.ClientSize = new System.Drawing.Size(1520, 720);
            this.Controls.Add(this.LblDateTime);
            this.Controls.Add(this.LblTime);
            this.Controls.Add(this.LblGoCart);
            this.Controls.Add(this.LblCount);
            this.Controls.Add(this.LblAmount);
            this.Controls.Add(this.PBCart);
            this.Controls.Add(this.PBSigma);
            this.Controls.Add(this.LblSearch);
            this.Controls.Add(this.GBMag);
            this.Controls.Add(this.BtnBack);
            this.Controls.Add(this.GBMusic);
            this.Controls.Add(this.TxtSearch);
            this.Controls.Add(this.GBBook);
            this.Controls.Add(this.LblSatanlar);
            this.Controls.Add(this.BtnMusic);
            this.Controls.Add(this.LblBookStore);
            this.Controls.Add(this.BtnMagazine);
            this.Controls.Add(this.FlowLMain);
            this.Controls.Add(this.BtnBook);
            this.ForeColor = System.Drawing.SystemColors.ActiveCaptionText;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "mainStore";
            this.Text = "Sigma Kitabevi";
            this.Load += new System.EventHandler(this.mainStore_Load);
            this.GBMusic.ResumeLayout(false);
            this.GBMag.ResumeLayout(false);
            this.GBBook.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PBCart)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PBSigma)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Button BtnBook;
        private System.Windows.Forms.FlowLayoutPanel FlowLMain;
        private System.Windows.Forms.Button BtnBookDT;
        private System.Windows.Forms.Button BtnBookDram;
        private System.Windows.Forms.Button BtnBookDistopya;
        private System.Windows.Forms.Button BtnBookPolisiye;
        private System.Windows.Forms.Button BtnBookKG;
        private System.Windows.Forms.Button BtnBookKorku;
        private System.Windows.Forms.Button BtnBookDK;
        private System.Windows.Forms.Button BtnBookFelsefe;
        private System.Windows.Forms.Button BtnBookCocuk;
        private System.Windows.Forms.Button BtnBookFantastik;
        private System.Windows.Forms.Button BtnBookBilim;
        private System.Windows.Forms.Button BtnBookBK;
        private System.Windows.Forms.Button BtnBookCR;
        private System.Windows.Forms.Button BtnBookRoman;
        private System.Windows.Forms.Button BtnMagazine;
        private System.Windows.Forms.Button BtnMagCog;
        private System.Windows.Forms.Button BtnMagPsiko;
        private System.Windows.Forms.Button BtnMagBT;
        private System.Windows.Forms.Button BtnMagBilim;
        private System.Windows.Forms.Button BtnMagCocuk;
        private System.Windows.Forms.Button BtnMagSpor;
        private System.Windows.Forms.Button BtnMagMizah;
        private System.Windows.Forms.Button BtnMagModa;
        private System.Windows.Forms.Button BtnMagEdebiyat;
        private System.Windows.Forms.Button BtnMagKS;
        private System.Windows.Forms.GroupBox GBMag;
        private System.Windows.Forms.GroupBox GBBook;
        private System.Windows.Forms.Button BtnMusic;
        private System.Windows.Forms.Button BtnMusicPop;
        private System.Windows.Forms.Button BtnMusicTSM;
        private System.Windows.Forms.Button BtnMusicRock;
        private System.Windows.Forms.Button BtnMusicArabesk;
        private System.Windows.Forms.GroupBox GBMusic;
        private System.Windows.Forms.Label LblSatanlar;
        private System.Windows.Forms.Label LblBookStore;
        private System.Windows.Forms.Label LblSearch;
        private System.Windows.Forms.TextBox TxtSearch;
        private System.Windows.Forms.Button BtnBack;
        private System.Windows.Forms.PictureBox PBSigma;
        private System.Windows.Forms.PictureBox PBCart;
        private System.Windows.Forms.Label LblAmount;
        private System.Windows.Forms.Label LblCount;
        private System.Windows.Forms.Label LblGoCart;
        private System.Windows.Forms.Label LblDateTime;
        private System.Windows.Forms.Timer timerMain;
        private System.Windows.Forms.Label LblTime;
    }
}