﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Net.Mail;
/**
*  @author  : Yunus Akyol
*  @number  : 152120181045
*  @mail    : yunusakyol5@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes register form.
*/
namespace OOP2Proje
{
    public partial class Register : Form
    {
        public static List<Customer> userList = new List<Customer>();
        public Register()
        {
            InitializeComponent();
        }
        /// <summary>
        /// This function provides to implement register button click event. 
        /// </summary>
        private void btnRegister_Click(object sender, EventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(txtBoxRegisterName.Text) || string.IsNullOrEmpty(txtBoxRegisterAddress.Text) || string.IsNullOrEmpty(txtBoxRegisterEmail.Text) || string.IsNullOrEmpty(txtBoxRegisterUsername.Text) || string.IsNullOrEmpty(txtBoxRegisterPassword.Text) || string.IsNullOrEmpty(txtBoxRegisterConfirmPassword.Text))
                {
                    MessageBox.Show("Boş Alan Tespit Edildi !", "Boş Alan Bırakılamaz !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                if (txtBoxRegisterPassword.Text != txtBoxRegisterConfirmPassword.Text)
                {
                    MessageBox.Show("Şifrelerim Uyuşmadığı Tespit Edildi !", "Şifreler Uyuşmuyor !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                if (!isValidEmail(txtBoxRegisterEmail.Text))
                {
                    MessageBox.Show("Email Adresi Geçerli Değil Lütfen Tekrar Deneyin !", "Biçimsiz Email !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                Customer tempUser = new Customer(txtBoxRegisterName.Text, txtBoxRegisterAddress.Text, txtBoxRegisterEmail.Text, txtBoxRegisterUsername.Text, Util.hashingPassword(txtBoxRegisterPassword.Text));
                if (userList.Any(user => user.Username == tempUser.Username))
                {
                    MessageBox.Show("Kullanıcı Adının Zaten Kayıtlı Olduğu Tespit Edildi; !", "Kayıtlı Kullanıcı Adı !", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    clearFields();
                    return;
                }
                userList.Add(tempUser);
                tempUser.CustomerID = userList.IndexOf(tempUser);
                tempUser.saveCustomer(tempUser);
                MessageBox.Show("Kayıt Başarılı !", "Başarılı !", MessageBoxButtons.OK);
                convertFormRegister();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// This function provides to implement link click event for register screen.
        /// </summary>
        private void lnkLblRegister_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            lnkLblRegister.LinkVisited = true;
            convertFormRegister();
        }
        /// <summary>
        /// This function implements form closing event for closing.
        /// </summary>
        private void Register_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(1);
        }
        /// <summary>
        /// This function provides to close this form and opens login form.
        /// </summary>
        private void convertFormRegister()
        {
            Login login = new Login();
            this.Hide();
            login.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// This function clears text boxes.
        /// </summary>
        private void clearFields()
        {
            txtBoxRegisterName.Text = "";
            txtBoxRegisterAddress.Text = "";
            txtBoxRegisterEmail.Text = "";
            txtBoxRegisterUsername.Text = "";
            txtBoxRegisterPassword.Text = "";
            txtBoxRegisterConfirmPassword.Text = "";
        }
        /// <summary>
        /// This function checks if email is valid or not.
        /// </summary>
        /// <param name="email"></param>
        /// <returns>True if email is valid or false if it is not valid.</returns>
        public static bool isValidEmail(string email)
        {
            try
            {
                MailAddress mail = new MailAddress(email);
                return true;
            }
            catch
            {
                return false;
            }
        }
        /// <summary>
        /// This function provides to set datas in the database to the registered customers list.
        /// </summary>
        private void Register_Load(object sender, EventArgs e)
        {
            DataBase db = new DataBase();
            userList = db.getListOfCustomerFromDB();
        }
    }
}
