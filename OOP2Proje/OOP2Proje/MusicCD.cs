﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes MusicCD abstract class.
*/
namespace OOP2Proje
{
    public class MusicCD : Product
    {
        private string singer;
        private string type;
        /// <summary>
        /// Getter Setter for singer
        /// </summary>
        public string Singer
        {
            get { return singer; }
            set { singer = value; }
        }
        /// <summary>
        /// Getter Setter for type
        /// </summary>
        public string Type
        {
            get { return type; }
            set { type = value; }
        }
        /// <summary>
        /// Abstract printProperties function
        /// </summary>
        /// <returns>singer and type</returns>
        public override string printProperties()
        {
            return "Şarkıcı: " + singer + " " + "Türü: " + type;
        }
    }
}
