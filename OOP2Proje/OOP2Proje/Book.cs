﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 24.05.2021
*  @brief   : This class includes Book abstract class.
*/
namespace OOP2Proje
{
    public class Book : Product
    {
        private string isbn;
        private string author;
        private string publisher;
        private int page;
        /// <summary>
        /// Getter Setter for isbn
        /// </summary>
        public string Isbn
        {
            get
            {
                return isbn;
            }
            set
            {
                isbn = value;
            }
        }
        /// <summary>
        /// Getter Setter for author
        /// </summary>
        public string Author
        {
            get
            {
                return author;
            }
            set
            {
                author = value;
            }
        }
        /// <summary>
        /// Getter Setter for publisher
        /// </summary>
        public string Publisher
        {
            get
            {
                return publisher;
            }
            set
            {
                publisher = value;
            }
        }
        /// <summary>
        /// Getter Setter for page
        /// </summary>
        public int Page
        {
            get
            {
                return page;
            }
            set
            {
                page = value;
            }
        }
        /// <summary>
        /// Abstract printProperties function
        /// </summary>
        /// <returns>name, author and price</returns>
        public override string printProperties()
        {
            return "Adı: " + Name + " " + "Yazar: " + author + " " + "Fiyat: " + Price;
        }
    }
}
