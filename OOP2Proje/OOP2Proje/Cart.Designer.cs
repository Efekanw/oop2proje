﻿
namespace OOP2Proje
{
    partial class Cart
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Cart));
            this.FLCart = new System.Windows.Forms.FlowLayoutPanel();
            this.BtnMainStore = new System.Windows.Forms.Button();
            this.BtnPay = new System.Windows.Forms.Button();
            this.LblTotPrice = new System.Windows.Forms.Label();
            this.LblTF = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.PnlCartSO = new System.Windows.Forms.Panel();
            this.timerSepet = new System.Windows.Forms.Timer(this.components);
            this.LblTimerSepetDate = new System.Windows.Forms.Label();
            this.LblTimerSepetTime = new System.Windows.Forms.Label();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.PnlCartSO.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.SuspendLayout();
            // 
            // FLCart
            // 
            this.FLCart.AutoScroll = true;
            this.FLCart.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.FLCart.Location = new System.Drawing.Point(10, 10);
            this.FLCart.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.FLCart.Name = "FLCart";
            this.FLCart.Size = new System.Drawing.Size(528, 559);
            this.FLCart.TabIndex = 0;
            // 
            // BtnMainStore
            // 
            this.BtnMainStore.BackColor = System.Drawing.Color.LemonChiffon;
            this.BtnMainStore.Font = new System.Drawing.Font("Palatino Linotype", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMainStore.ForeColor = System.Drawing.Color.Teal;
            this.BtnMainStore.Location = new System.Drawing.Point(543, 10);
            this.BtnMainStore.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMainStore.Name = "BtnMainStore";
            this.BtnMainStore.Size = new System.Drawing.Size(160, 66);
            this.BtnMainStore.TabIndex = 1;
            this.BtnMainStore.Text = "Mağazaya \r\nDön";
            this.BtnMainStore.UseVisualStyleBackColor = false;
            this.BtnMainStore.Click += new System.EventHandler(this.BtnMainStore_Click);
            // 
            // BtnPay
            // 
            this.BtnPay.BackColor = System.Drawing.Color.LemonChiffon;
            this.BtnPay.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnPay.ForeColor = System.Drawing.Color.Teal;
            this.BtnPay.Location = new System.Drawing.Point(0, 60);
            this.BtnPay.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnPay.Name = "BtnPay";
            this.BtnPay.Size = new System.Drawing.Size(297, 51);
            this.BtnPay.TabIndex = 2;
            this.BtnPay.Text = "Alışverişi Tamamla";
            this.BtnPay.UseVisualStyleBackColor = false;
            this.BtnPay.Click += new System.EventHandler(this.BtnPay_Click);
            // 
            // LblTotPrice
            // 
            this.LblTotPrice.AutoSize = true;
            this.LblTotPrice.BackColor = System.Drawing.Color.Snow;
            this.LblTotPrice.Font = new System.Drawing.Font("Palatino Linotype", 15.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblTotPrice.ForeColor = System.Drawing.Color.Black;
            this.LblTotPrice.Location = new System.Drawing.Point(543, 541);
            this.LblTotPrice.Name = "LblTotPrice";
            this.LblTotPrice.Size = new System.Drawing.Size(141, 28);
            this.LblTotPrice.TabIndex = 3;
            this.LblTotPrice.Text = "Toplam Fiyat";
            // 
            // LblTF
            // 
            this.LblTF.BackColor = System.Drawing.Color.Snow;
            this.LblTF.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblTF.ForeColor = System.Drawing.Color.Black;
            this.LblTF.Location = new System.Drawing.Point(708, 541);
            this.LblTF.Name = "LblTF";
            this.LblTF.Size = new System.Drawing.Size(132, 27);
            this.LblTF.TabIndex = 4;
            this.LblTF.Text = "0";
            this.LblTF.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(7, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(128, 26);
            this.label1.TabIndex = 5;
            this.label1.Text = "Sipariş Özeti";
            // 
            // PnlCartSO
            // 
            this.PnlCartSO.BackColor = System.Drawing.Color.Snow;
            this.PnlCartSO.Controls.Add(this.label1);
            this.PnlCartSO.Controls.Add(this.BtnPay);
            this.PnlCartSO.Location = new System.Drawing.Point(543, 423);
            this.PnlCartSO.Name = "PnlCartSO";
            this.PnlCartSO.Size = new System.Drawing.Size(297, 115);
            this.PnlCartSO.TabIndex = 10;
            // 
            // timerSepet
            // 
            this.timerSepet.Tick += new System.EventHandler(this.timerSepet_Tick);
            // 
            // LblTimerSepetDate
            // 
            this.LblTimerSepetDate.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblTimerSepetDate.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblTimerSepetDate.Location = new System.Drawing.Point(710, 13);
            this.LblTimerSepetDate.Name = "LblTimerSepetDate";
            this.LblTimerSepetDate.Size = new System.Drawing.Size(137, 66);
            this.LblTimerSepetDate.TabIndex = 11;
            this.LblTimerSepetDate.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblTimerSepetTime
            // 
            this.LblTimerSepetTime.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblTimerSepetTime.ForeColor = System.Drawing.Color.WhiteSmoke;
            this.LblTimerSepetTime.Location = new System.Drawing.Point(710, 79);
            this.LblTimerSepetTime.Name = "LblTimerSepetTime";
            this.LblTimerSepetTime.Size = new System.Drawing.Size(137, 66);
            this.LblTimerSepetTime.TabIndex = 12;
            this.LblTimerSepetTime.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Image = global::OOP2Proje.Properties.Resources.sepet;
            this.pictureBox2.Location = new System.Drawing.Point(543, 167);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(294, 240);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox2.TabIndex = 8;
            this.pictureBox2.TabStop = false;
            // 
            // Cart
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.ClientSize = new System.Drawing.Size(848, 583);
            this.Controls.Add(this.LblTimerSepetTime);
            this.Controls.Add(this.LblTimerSepetDate);
            this.Controls.Add(this.LblTF);
            this.Controls.Add(this.LblTotPrice);
            this.Controls.Add(this.PnlCartSO);
            this.Controls.Add(this.pictureBox2);
            this.Controls.Add(this.BtnMainStore);
            this.Controls.Add(this.FLCart);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(864, 622);
            this.MinimumSize = new System.Drawing.Size(864, 622);
            this.Name = "Cart";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sepetim";
            this.Load += new System.EventHandler(this.Cart_Load);
            this.PnlCartSO.ResumeLayout(false);
            this.PnlCartSO.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel FLCart;
        private System.Windows.Forms.Button BtnMainStore;
        private System.Windows.Forms.Button BtnPay;
        private System.Windows.Forms.Label LblTotPrice;
        private System.Windows.Forms.Label LblTF;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Panel PnlCartSO;
        private System.Windows.Forms.Timer timerSepet;
        private System.Windows.Forms.Label LblTimerSepetDate;
        private System.Windows.Forms.Label LblTimerSepetTime;
    }
}