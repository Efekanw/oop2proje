﻿
namespace OOP2Proje
{
    partial class UserControlMagazine
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblMagIssue = new System.Windows.Forms.Label();
            this.LblMagPrice = new System.Windows.Forms.Label();
            this.LblMagName = new System.Windows.Forms.Label();
            this.BtnAddToCart = new System.Windows.Forms.Button();
            this.PBoxMag = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBoxMag)).BeginInit();
            this.SuspendLayout();
            // 
            // LblMagIssue
            // 
            this.LblMagIssue.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblMagIssue.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagIssue.ForeColor = System.Drawing.Color.Black;
            this.LblMagIssue.Location = new System.Drawing.Point(0, 282);
            this.LblMagIssue.Name = "LblMagIssue";
            this.LblMagIssue.Size = new System.Drawing.Size(400, 34);
            this.LblMagIssue.TabIndex = 15;
            this.LblMagIssue.Text = "Issue";
            this.LblMagIssue.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMagPrice
            // 
            this.LblMagPrice.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.LblMagPrice.BackColor = System.Drawing.Color.MistyRose;
            this.LblMagPrice.Font = new System.Drawing.Font("Palatino Linotype", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagPrice.ForeColor = System.Drawing.Color.Black;
            this.LblMagPrice.Location = new System.Drawing.Point(3, 373);
            this.LblMagPrice.Name = "LblMagPrice";
            this.LblMagPrice.Size = new System.Drawing.Size(136, 39);
            this.LblMagPrice.TabIndex = 11;
            this.LblMagPrice.Text = "Price";
            this.LblMagPrice.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMagName
            // 
            this.LblMagName.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.LblMagName.AutoEllipsis = true;
            this.LblMagName.Font = new System.Drawing.Font("Palatino Linotype", 13.8F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagName.ForeColor = System.Drawing.Color.Black;
            this.LblMagName.Location = new System.Drawing.Point(0, 225);
            this.LblMagName.Name = "LblMagName";
            this.LblMagName.Padding = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.LblMagName.Size = new System.Drawing.Size(400, 57);
            this.LblMagName.TabIndex = 10;
            this.LblMagName.Text = "Name";
            this.LblMagName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnAddToCart
            // 
            this.BtnAddToCart.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Right)));
            this.BtnAddToCart.BackgroundImage = global::OOP2Proje.Properties.Resources.kisspng_computer_icons_trade_basket_add_to_cart_button_5ac189387dd9d7_3184996315226330165155_1;
            this.BtnAddToCart.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnAddToCart.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnAddToCart.FlatAppearance.BorderColor = System.Drawing.Color.MistyRose;
            this.BtnAddToCart.FlatAppearance.BorderSize = 0;
            this.BtnAddToCart.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(157)))), ((int)(((byte)(70)))), ((int)(((byte)(70)))));
            this.BtnAddToCart.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnAddToCart.Font = new System.Drawing.Font("Microsoft Sans Serif", 3F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnAddToCart.Location = new System.Drawing.Point(319, 357);
            this.BtnAddToCart.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.BtnAddToCart.Name = "BtnAddToCart";
            this.BtnAddToCart.Size = new System.Drawing.Size(80, 60);
            this.BtnAddToCart.TabIndex = 16;
            this.BtnAddToCart.TabStop = false;
            this.BtnAddToCart.UseVisualStyleBackColor = false;
            this.BtnAddToCart.Click += new System.EventHandler(this.BtnAddToCart_Click);
            // 
            // PBoxMag
            // 
            this.PBoxMag.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.PBoxMag.ErrorImage = null;
            this.PBoxMag.Location = new System.Drawing.Point(109, 2);
            this.PBoxMag.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.PBoxMag.Name = "PBoxMag";
            this.PBoxMag.Size = new System.Drawing.Size(180, 220);
            this.PBoxMag.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBoxMag.TabIndex = 9;
            this.PBoxMag.TabStop = false;
            // 
            // UserControlMagazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(8F, 16F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.Controls.Add(this.BtnAddToCart);
            this.Controls.Add(this.LblMagIssue);
            this.Controls.Add(this.LblMagPrice);
            this.Controls.Add(this.LblMagName);
            this.Controls.Add(this.PBoxMag);
            this.Margin = new System.Windows.Forms.Padding(3, 2, 3, 2);
            this.Name = "UserControlMagazine";
            this.Size = new System.Drawing.Size(400, 418);
            ((System.ComponentModel.ISupportInitialize)(this.PBoxMag)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button BtnAddToCart;
        private System.Windows.Forms.Label LblMagIssue;
        private System.Windows.Forms.Label LblMagPrice;
        private System.Windows.Forms.Label LblMagName;
        private System.Windows.Forms.PictureBox PBoxMag;
    }
}
