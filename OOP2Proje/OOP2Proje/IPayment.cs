﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 29.05.2021
*  @brief   : This class includes Payment Interface
*/
namespace OOP2Proje
{
    /// <summary>
    /// Includes interface's functions
    /// </summary>
    interface IPayment
    {
        void make_payment();
    }
}
