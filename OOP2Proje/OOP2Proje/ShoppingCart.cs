﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Hasan Can Görmez
*  @number  : 152120181035
*  @mail    : hasan_39_2000@outlook.com
*  @date    : 28.05.2021
*  @brief   : This class includes shopping cart's functions.
*/
namespace OOP2Proje
{
    public class ShoppingCart
    {
        private int cartid;
        private int cst_id;
        private string payment_type;
        private double overall_price;
        private int amount;
        public static List<ItemToPurchase> itemsToPurchase = new List<ItemToPurchase>();
        /// <summary>
        /// getter setter for shopping cart
        /// </summary>
        public int _cst_id { get { return cst_id; } set { cst_id = value; } }
        /// <summary>
        /// getter setter for payment type
        /// </summary>
        public string _payment_type { get { return payment_type; } set { payment_type = value; } }
        /// <summary>
        /// getter setter for price
        /// </summary>
        public double _price { get { return overall_price; } set { overall_price = value; } }
        /// <summary>
        /// getter setter for cart id
        /// </summary>
        public int _cartid { get { return cartid; } set { cartid = value; } }
        /// <summary>
        /// getter setter for amount
        /// </summary>
        public int _amount { get { return amount; } set { amount = value; } }
        /// <summary>
        /// Default constructor for Shopping Cart
        /// </summary>
        public ShoppingCart(){}
        /// <summary>
        /// Constructor with parameters
        /// </summary>
        /// <param name="_cst_id">takes customer's id</param>
        /// <param name="_paymentType">takes payment type</param>
        /// <param name="bruh">takes list of item to purchase</param>
        public ShoppingCart(int _cst_id, string _paymentType, List<ItemToPurchase> bruh)
        {
            cst_id = _cst_id;
            payment_type = _paymentType;
            itemsToPurchase = bruh;
            overall_price = 0;
        }
        /// <summary>
        /// This function prints product's info
        /// </summary>
        /// <returns>Overall Info</returns>
        public string printProducts()
        {
            string Overall_Info = "";
            foreach (var item in itemsToPurchase)
            {
                Overall_Info = Overall_Info + "Name : " + item.Product.Name + Environment.NewLine +
                "ID : " + item.Product.Id + Environment.NewLine + "Price : " + item.Product.Price +
                Environment.NewLine + "Amount : " + item.Quantity;
            }
            return Overall_Info;
        }
        /// <summary>
        /// this function adds specific item to shopping cart
        /// </summary>
        /// <param name="item_to_add">takes item</param>
        public void Add_Product_To_Cart(ItemToPurchase item_to_add)
        {
            try
            {
                foreach (ItemToPurchase item in itemsToPurchase)
                {
                    if (item.Product.Name == item_to_add.Product.Name)
                    {
                        item.Quantity++;
                        this.overall_price = this.overall_price + item_to_add.Product.Price;
                        return;
                    }
                }
                itemsToPurchase.Add(item_to_add);
                this.overall_price = this.overall_price + item_to_add.Product.Price;
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// this function removes specific item from shopping cart
        /// </summary>
        /// <param name="item_to_remove">takes item to remove</param>
        public void Remove_Product_From_Cart(ItemToPurchase item_to_remove)
        {
            try
            {
                if (itemsToPurchase.Count > 0)
                {
                    itemsToPurchase.Remove(item_to_remove);
                    this.overall_price = this.overall_price - item_to_remove.Product.Price;
                    item_to_remove.Quantity--;
                }
                else if (item_to_remove.Quantity < 1)
                {
                    itemsToPurchase.Remove(item_to_remove);
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// this function completes the order
        /// </summary>
        public void Place_Order()
        {
            overall_price = 0;
            itemsToPurchase.Clear();
        }
        /// <summary>
        /// this function cancels the order
        /// </summary>
        public void Cancel_Order()
        {
            overall_price = 0;
            itemsToPurchase.Clear();
        }
    }
}

