﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 28.05.2021
*  @brief   : This class includes User Control ShoppingItem Book.
*/
namespace OOP2Proje
{
    public partial class UC_ShoppingItem_Book : UserControl
    {
        ItemToPurchase item = new ItemToPurchase();
        DataBase db = new DataBase();
        ShoppingCart temp = new ShoppingCart();
        int tempamount;
        double lblprice;
        /// <summary>
        /// This constructor arranges according to incoming shopping item object.
        /// </summary>
        /// <param name="item"></param>
        public UC_ShoppingItem_Book(ItemToPurchase item)
        {
            InitializeComponent();
            this.item = item;
            LblCartBookName.Text = this.item.Product.Name;
            LblBookCCount.Text = this.item.Quantity.ToString();
            LblBookCbirim.Text = this.item.Product.Price.ToString() + " ₺";
            lblprice = this.item.Product.Price * this.item.Quantity;
            string tempprice = String.Format("{0:0.00}", lblprice).ToString();
            LblBookCTot.Text = tempprice + " ₺";
            System.IO.MemoryStream ms = new System.IO.MemoryStream(this.item.Product.Image);
            PBBookC.Image = System.Drawing.Image.FromStream(ms);
        }
        /// <summary>
        /// Increases the number of items.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBookCAdd_Click(object sender, EventArgs e)
        {
            Label TotPriceB = (ParentForm.Controls["LblTF"] as Label);
            item.Quantity++;
            db.updateItemInDB(item);
            temp = db.getShoppingCartFromDB(mainStore.cartid);
            temp._price = Math.Round(temp._price, 2, MidpointRounding.ToEven);
            double lbltmp = this.item.Product.Price * 1;
            temp._price += Math.Round(lbltmp, 2, MidpointRounding.ToEven);
            temp._amount += 1;
            db.updateShoppingCart(temp);
            lblprice = this.item.Product.Price * this.item.Quantity;
            string tempprice = String.Format("{0:0.00}", lblprice).ToString();
            LblBookCTot.Text = tempprice + " ₺";
            LblBookCCount.Text = this.item.Quantity.ToString();
            TotPriceB.Text = db.getTotPriceOfCart(mainStore.cartid).ToString() + " ₺";
        }
        /// <summary>
        /// Decreases the number of items.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBookCRemove_Click(object sender, EventArgs e)
        {
            Label TotPriceB = (ParentForm.Controls["LblTF"] as Label);

            if (item.Quantity <= 1)
            {
                DeleteUCBook();
            }
            else
            {
                item.Quantity--;
                db.updateItemInDB(item);
                temp._cartid = mainStore.cartid;
                temp = db.getShoppingCartFromDB(mainStore.cartid);
                temp._price = Math.Round(temp._price, 2, MidpointRounding.ToEven);
                double lbltmp = this.item.Product.Price * 1;
                temp._price -= Math.Round(lbltmp, 2, MidpointRounding.ToEven);
                temp._amount -= 1;
                db.updateShoppingCart(temp);
                lblprice = this.item.Product.Price * this.item.Quantity;
                string tempprice = String.Format("{0:0.00}", lblprice).ToString();
                LblBookCTot.Text = tempprice + " ₺";
                LblBookCCount.Text = this.item.Quantity.ToString();
            }
            TotPriceB.Text = db.getTotPriceOfCart(mainStore.cartid).ToString() + " ₺";
        }
        /// <summary>
        /// Deletes the item in shopping cart.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnBookCDelete_Click(object sender, EventArgs e)
        {
            DeleteUCBook();
        }
        /// <summary>
        /// The function written in case the item is deleted.
        /// </summary>
        private void DeleteUCBook()
        {
            Label TotPriceB = (ParentForm.Controls["LblTF"] as Label);
            db.deleteItemInDB(item);
            temp._cartid = mainStore.cartid;
            temp = db.getShoppingCartFromDB(mainStore.cartid);
            tempamount = Convert.ToInt32(LblBookCCount.Text);
            double lbltmp = this.item.Product.Price * tempamount;
            temp._price = Math.Round(temp._price, 2, MidpointRounding.ToEven);
            temp._price -= Math.Round(lbltmp, 2, MidpointRounding.ToEven);
            lblprice = this.item.Product.Price * this.item.Quantity;
            string tempprice = String.Format("{0:0.00}", lblprice).ToString();
            LblBookCTot.Text = tempprice + " ₺";
            temp._amount -= this.item.Quantity;
            this.item.Quantity = 0;
            db.updateShoppingCart(temp);
            if (this.PBBookC.Image != null)
            {
                Image img = this.PBBookC.Image;
                this.PBBookC.Image = null;
                img.Dispose();
            }
            this.Parent.Controls.Remove(this);
            TotPriceB.Text = db.getTotPriceOfCart(mainStore.cartid).ToString() + " ₺";
        }
    }
}
