﻿
namespace OOP2Proje
{
    partial class UC_ShoppingItem_Music
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblMusicCamo = new System.Windows.Forms.Label();
            this.LblMusicCPrice = new System.Windows.Forms.Label();
            this.LblMusicCCount = new System.Windows.Forms.Label();
            this.LblMusicCUP = new System.Windows.Forms.Label();
            this.LblMusicCTot = new System.Windows.Forms.Label();
            this.LblMusicCbirim = new System.Windows.Forms.Label();
            this.BtnMusicCAdd = new System.Windows.Forms.Button();
            this.BtnMusicCRemove = new System.Windows.Forms.Button();
            this.LblCartMusicName = new System.Windows.Forms.Label();
            this.PBMusicC = new System.Windows.Forms.PictureBox();
            this.BtnMusicCDelete = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.PBMusicC)).BeginInit();
            this.SuspendLayout();
            // 
            // LblMusicCamo
            // 
            this.LblMusicCamo.AutoSize = true;
            this.LblMusicCamo.BackColor = System.Drawing.Color.Transparent;
            this.LblMusicCamo.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicCamo.Location = new System.Drawing.Point(155, 63);
            this.LblMusicCamo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMusicCamo.Name = "LblMusicCamo";
            this.LblMusicCamo.Size = new System.Drawing.Size(41, 20);
            this.LblMusicCamo.TabIndex = 12;
            this.LblMusicCamo.Text = "Adet";
            // 
            // LblMusicCPrice
            // 
            this.LblMusicCPrice.AutoSize = true;
            this.LblMusicCPrice.BackColor = System.Drawing.Color.Transparent;
            this.LblMusicCPrice.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicCPrice.Location = new System.Drawing.Point(366, 63);
            this.LblMusicCPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMusicCPrice.Name = "LblMusicCPrice";
            this.LblMusicCPrice.Size = new System.Drawing.Size(96, 20);
            this.LblMusicCPrice.TabIndex = 13;
            this.LblMusicCPrice.Text = "Toplam Fiyat";
            // 
            // LblMusicCCount
            // 
            this.LblMusicCCount.BackColor = System.Drawing.Color.Transparent;
            this.LblMusicCCount.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicCCount.Location = new System.Drawing.Point(155, 86);
            this.LblMusicCCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMusicCCount.Name = "LblMusicCCount";
            this.LblMusicCCount.Size = new System.Drawing.Size(36, 30);
            this.LblMusicCCount.TabIndex = 14;
            this.LblMusicCCount.Text = "0";
            this.LblMusicCCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMusicCUP
            // 
            this.LblMusicCUP.AutoSize = true;
            this.LblMusicCUP.BackColor = System.Drawing.Color.Transparent;
            this.LblMusicCUP.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicCUP.Location = new System.Drawing.Point(257, 63);
            this.LblMusicCUP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMusicCUP.Name = "LblMusicCUP";
            this.LblMusicCUP.Size = new System.Drawing.Size(86, 20);
            this.LblMusicCUP.TabIndex = 15;
            this.LblMusicCUP.Text = "Birim Fiyatı";
            // 
            // LblMusicCTot
            // 
            this.LblMusicCTot.BackColor = System.Drawing.Color.Transparent;
            this.LblMusicCTot.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicCTot.Location = new System.Drawing.Point(370, 94);
            this.LblMusicCTot.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMusicCTot.Name = "LblMusicCTot";
            this.LblMusicCTot.Size = new System.Drawing.Size(92, 20);
            this.LblMusicCTot.TabIndex = 16;
            this.LblMusicCTot.Text = "0";
            this.LblMusicCTot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMusicCbirim
            // 
            this.LblMusicCbirim.BackColor = System.Drawing.Color.Transparent;
            this.LblMusicCbirim.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMusicCbirim.Location = new System.Drawing.Point(261, 94);
            this.LblMusicCbirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMusicCbirim.Name = "LblMusicCbirim";
            this.LblMusicCbirim.Size = new System.Drawing.Size(82, 20);
            this.LblMusicCbirim.TabIndex = 17;
            this.LblMusicCbirim.Text = "0";
            this.LblMusicCbirim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnMusicCAdd
            // 
            this.BtnMusicCAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMusicCAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusicCAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnMusicCAdd.Location = new System.Drawing.Point(195, 86);
            this.BtnMusicCAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMusicCAdd.Name = "BtnMusicCAdd";
            this.BtnMusicCAdd.Size = new System.Drawing.Size(30, 32);
            this.BtnMusicCAdd.TabIndex = 18;
            this.BtnMusicCAdd.Text = "+";
            this.BtnMusicCAdd.UseVisualStyleBackColor = true;
            this.BtnMusicCAdd.Click += new System.EventHandler(this.BtnMusicCAdd_Click);
            // 
            // BtnMusicCRemove
            // 
            this.BtnMusicCRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMusicCRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMusicCRemove.ForeColor = System.Drawing.Color.Red;
            this.BtnMusicCRemove.Location = new System.Drawing.Point(121, 86);
            this.BtnMusicCRemove.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMusicCRemove.Name = "BtnMusicCRemove";
            this.BtnMusicCRemove.Size = new System.Drawing.Size(30, 32);
            this.BtnMusicCRemove.TabIndex = 19;
            this.BtnMusicCRemove.Text = "-";
            this.BtnMusicCRemove.UseVisualStyleBackColor = true;
            this.BtnMusicCRemove.Click += new System.EventHandler(this.BtnMusicCRemove_Click);
            // 
            // LblCartMusicName
            // 
            this.LblCartMusicName.AutoEllipsis = true;
            this.LblCartMusicName.BackColor = System.Drawing.Color.Transparent;
            this.LblCartMusicName.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblCartMusicName.ForeColor = System.Drawing.Color.Black;
            this.LblCartMusicName.Location = new System.Drawing.Point(121, 0);
            this.LblCartMusicName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblCartMusicName.Name = "LblCartMusicName";
            this.LblCartMusicName.Size = new System.Drawing.Size(383, 63);
            this.LblCartMusicName.TabIndex = 21;
            this.LblCartMusicName.Text = "Müzik";
            this.LblCartMusicName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBMusicC
            // 
            this.PBMusicC.ErrorImage = null;
            this.PBMusicC.Location = new System.Drawing.Point(16, 16);
            this.PBMusicC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PBMusicC.Name = "PBMusicC";
            this.PBMusicC.Size = new System.Drawing.Size(92, 100);
            this.PBMusicC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBMusicC.TabIndex = 11;
            this.PBMusicC.TabStop = false;
            // 
            // BtnMusicCDelete
            // 
            this.BtnMusicCDelete.BackgroundImage = global::OOP2Proje.Properties.Resources.trash_music;
            this.BtnMusicCDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnMusicCDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMusicCDelete.FlatAppearance.BorderSize = 0;
            this.BtnMusicCDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMusicCDelete.Location = new System.Drawing.Point(474, 98);
            this.BtnMusicCDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMusicCDelete.Name = "BtnMusicCDelete";
            this.BtnMusicCDelete.Size = new System.Drawing.Size(30, 32);
            this.BtnMusicCDelete.TabIndex = 20;
            this.BtnMusicCDelete.UseVisualStyleBackColor = true;
            this.BtnMusicCDelete.Click += new System.EventHandler(this.BtnMusicCDelete_Click);
            // 
            // UC_ShoppingItem_Music
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.LblCartMusicName);
            this.Controls.Add(this.PBMusicC);
            this.Controls.Add(this.BtnMusicCDelete);
            this.Controls.Add(this.LblMusicCamo);
            this.Controls.Add(this.BtnMusicCRemove);
            this.Controls.Add(this.LblMusicCPrice);
            this.Controls.Add(this.BtnMusicCAdd);
            this.Controls.Add(this.LblMusicCCount);
            this.Controls.Add(this.LblMusicCbirim);
            this.Controls.Add(this.LblMusicCUP);
            this.Controls.Add(this.LblMusicCTot);
            this.Name = "UC_ShoppingItem_Music";
            this.Size = new System.Drawing.Size(504, 130);
            ((System.ComponentModel.ISupportInitialize)(this.PBMusicC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBMusicC;
        private System.Windows.Forms.Label LblMusicCamo;
        private System.Windows.Forms.Label LblMusicCPrice;
        private System.Windows.Forms.Label LblMusicCCount;
        private System.Windows.Forms.Label LblMusicCUP;
        private System.Windows.Forms.Label LblMusicCTot;
        private System.Windows.Forms.Label LblMusicCbirim;
        private System.Windows.Forms.Button BtnMusicCAdd;
        private System.Windows.Forms.Button BtnMusicCRemove;
        private System.Windows.Forms.Button BtnMusicCDelete;
        private System.Windows.Forms.Label LblCartMusicName;
    }
}
