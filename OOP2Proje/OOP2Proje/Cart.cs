﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Yusuf Kenan AKSAN
*  @number  : 152120181012
*  @mail    : ykenanaksan@gmail.com
*  @date    : 27.05.2021
*  @brief   : This class includes Cart Form.
*/
namespace OOP2Proje
{
    public partial class Cart : Form
    {
        private UC_ShoppingItem_Book ucbook;
        private UC_ShoppingItem_Magazine ucmagazine;
        private UC_ShoppingItem_Music ucmusic;
        List<ItemToPurchase> itemsInCart = new List<ItemToPurchase>();
        DataBase db = new DataBase();
        double lblprice;
        /// <summary>
        /// Constructor of Cart
        /// </summary>
        public Cart()
        {
            InitializeComponent();
            timerSepet.Start();
        }
        /// <summary>
        /// Cart loading in the first opening of the form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cart_Load(object sender, EventArgs e)
        {
            FLCart.Controls.Clear();
            if (!db.getCartStatus())
            {
                itemsInCart = db.getItemsInCart(mainStore.cartid);
                foreach (ItemToPurchase item in itemsInCart)
                {
                    if (item.Product is Book)
                    {
                        ucbook = new UC_ShoppingItem_Book(item);
                        FLCart.Controls.Add(ucbook);
                    }
                    if (item.Product is Magazine)
                    {
                        ucmagazine = new UC_ShoppingItem_Magazine(item);
                        FLCart.Controls.Add(ucmagazine);
                    }
                    if (item.Product is MusicCD)
                    {
                        ucmusic = new UC_ShoppingItem_Music(item);
                        FLCart.Controls.Add(ucmusic);
                    }
                }
            }
            lblprice = db.getTotPriceOfCart(mainStore.cartid);
            string tempprice = String.Format("{0:0.00}", lblprice).ToString();
            LblTF.Text = tempprice + " ₺";
        }
        /// <summary>
        /// Returns to Mainstore.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnMainStore_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Mağazaya dön sekmesine tıklandı");
            mainStore mainform = new mainStore();
            this.Hide();
            mainform.ShowDialog();
            this.Close();

        }
        /// <summary>
        /// Directs to Payment Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BtnPay_Click(object sender, EventArgs e)
        {
            Logger.Getlogger().WriteLog(SingletonLoginedCustomer.getInstance().Customer.Name, "Alışverişi Tamamla sekmesine tıklandı");
            Payment paymentform = new Payment();
            this.Hide();
            paymentform.ShowDialog();
            this.Close();
        }
        /// <summary>
        /// Timer for the Cart Form.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void timerSepet_Tick(object sender, EventArgs e)
        {
            DateTime datetime = DateTime.Now;
            LblTimerSepetDate.Text = datetime.ToLongDateString();
            LblTimerSepetTime.Text = datetime.ToLongTimeString();
        }
    }
}
