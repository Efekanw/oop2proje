﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using iTextSharp.text;
using iTextSharp.text.pdf;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 29.05.2021
*  @brief   : This class includes PDF design
*/
namespace OOP2Proje
{
    public class PDF
    {
        private string payment_type;
        /// <summary>
        /// Constructor which takes payment type and equals it to our payment type
        /// </summary>
        /// <param name="payment_type">Takes payment type</param>
        public PDF(string payment_type)
        {
            this.payment_type = payment_type;
        }
        /// <summary>
        /// Creates PDF and designs it
        /// </summary>
        public void create_PDF()
        {
            using (System.IO.MemoryStream memoryStream = new System.IO.MemoryStream())
            {
                List<ItemToPurchase> pdf_item = new List<ItemToPurchase>();
                DataBase db = new DataBase();
                pdf_item = db.getItemsInCart(mainStore.cartid);
                Document document = new Document(PageSize.A4, 10, 10, 10, 10);
                PdfWriter.GetInstance(document, new FileStream("Invoice.pdf", FileMode.Create));
                document.Open();
                string headertext = "SIGMA KITABEVI";
                double all_price = 0;
                Paragraph Header = new Paragraph();
                Header.SpacingBefore = 10;
                Header.SpacingAfter = 10;
                Header.Alignment = Element.ALIGN_CENTER;
                Header.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 20f, BaseColor.BLACK);
                Header.Add(headertext);
                document.Add(Header);
                Paragraph spacer = new Paragraph("")
                {
                    SpacingBefore = 30f,
                    SpacingAfter = 30f
                };
                Paragraph minispacer = new Paragraph("")
                {
                    SpacingBefore = 10f,
                    SpacingAfter = 10f
                };
                document.Add(minispacer);
                var timeTable = new PdfPTable(new[] { 1f })
                {
                    HorizontalAlignment = 2,
                    WidthPercentage = 20,
                    DefaultCell = { MinimumHeight = 10f, }
                };
                timeTable.AddCell("Date");
                timeTable.AddCell(DateTime.Now.ToString());
                document.Add(timeTable);
                document.Add(minispacer);
                Paragraph elements1 = new Paragraph("Kimden : " +
                Environment.NewLine + "Sigma Kitabevi" +
                Environment.NewLine + "Eskisehir Büyükdere Mah. Özgürler Sok. No:12" +
                Environment.NewLine + "0262 255 55 55" +
                Environment.NewLine + "sigmakitabevi@outlook.com\n\n" +
                Environment.NewLine);
                Paragraph elements2 = new Paragraph("Kime : " +
                Environment.NewLine + "Musteri No : " + SingletonLoginedCustomer.getInstance().Customer.CustomerID +
                Environment.NewLine + "Musteri ismi : " + SingletonLoginedCustomer.getInstance().Customer.Name +
                Environment.NewLine + "Musteri Adresi : " + SingletonLoginedCustomer.getInstance().Customer.Address +
                Environment.NewLine + "Odeme Tipi : " + this.payment_type);
                elements1.Alignment = Element.ALIGN_LEFT;
                elements2.Alignment = Element.ALIGN_LEFT;
                document.Add(elements1);
                document.Add(elements2);
                document.Add(spacer);
                string[] headers = new string[5];
                headers[0] = "ID";
                headers[1] = "Product Name";
                headers[2] = "Quantity";
                headers[3] = "Price";
                headers[4] = "Total Price";
                PdfPTable table = new PdfPTable(new[] { .75f, 2.5f, 1f, 1f, 1.5f })
                {
                    HorizontalAlignment = 1,
                    WidthPercentage = 75,
                    DefaultCell = { MinimumHeight = 22f, }
                };
                string text = "Sepetinizdeki Urunler";
                Paragraph middle = new Paragraph();
                middle.SpacingBefore = 10;
                middle.SpacingAfter = 10;
                middle.Alignment = Element.ALIGN_CENTER;
                middle.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 20f, BaseColor.BLACK);
                middle.Add(text);
                document.Add(middle);
                document.Add(minispacer);
                for (int i = 0; i < headers.Length; i++)
                {
                    PdfPCell cell = new PdfPCell(new PdfPCell());
                    cell.AddElement(new Chunk(headers[i]));
                    table.AddCell(cell);
                }
                for (int i = 0; i < pdf_item.Count; i++)
                {
                    table.AddCell(i.ToString());
                    table.AddCell(pdf_item[i].Product.Name);
                    table.AddCell(pdf_item[i].Quantity.ToString());
                    table.AddCell(pdf_item[i].Product.Price.ToString());
                    table.AddCell((pdf_item[i].Quantity * pdf_item[i].Product.Price).ToString());
                    all_price += pdf_item[i].Quantity * pdf_item[i].Product.Price;
                }
                document.Add(table);
                document.Add(minispacer);
                string text1 = "Fatura ucretiniz : " + all_price.ToString();
                Paragraph last = new Paragraph();
                last.SpacingBefore = 10;
                last.SpacingAfter = 10;
                last.Alignment = Element.ALIGN_CENTER;
                last.Font = FontFactory.GetFont(FontFactory.TIMES_BOLD, 20f, BaseColor.BLACK);
                last.Add(text1);
                document.Add(last);
                document.Add(minispacer);
                document.Close();
            }
        }
    }
}
