﻿
namespace OOP2Proje
{
    partial class UC_PaymentItem_Book
    {
        /// <summary> 
        ///Gerekli tasarımcı değişkeni.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        ///Kullanılan tüm kaynakları temizleyin.
        /// </summary>
        ///<param name="disposing">yönetilen kaynaklar dispose edilmeliyse doğru; aksi halde yanlış.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Bileşen Tasarımcısı üretimi kod

        /// <summary> 
        /// Tasarımcı desteği için gerekli metot - bu metodun 
        ///içeriğini kod düzenleyici ile değiştirmeyin.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblPaymetBookAdet = new System.Windows.Forms.Label();
            this.lblPaymentBookToplam = new System.Windows.Forms.Label();
            this.lblPaymentBookCount = new System.Windows.Forms.Label();
            this.lblPaymentBookBirim = new System.Windows.Forms.Label();
            this.lblPaymentBookToplamFiyat = new System.Windows.Forms.Label();
            this.lblPaymentBookBirimFiyat = new System.Windows.Forms.Label();
            this.lblPaymentBookName = new System.Windows.Forms.Label();
            this.PBPaymentBook = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBPaymentBook)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPaymetBookAdet
            // 
            this.lblPaymetBookAdet.AutoSize = true;
            this.lblPaymetBookAdet.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymetBookAdet.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymetBookAdet.ForeColor = System.Drawing.Color.Black;
            this.lblPaymetBookAdet.Location = new System.Drawing.Point(155, 63);
            this.lblPaymetBookAdet.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymetBookAdet.Name = "lblPaymetBookAdet";
            this.lblPaymetBookAdet.Size = new System.Drawing.Size(41, 20);
            this.lblPaymetBookAdet.TabIndex = 1;
            this.lblPaymetBookAdet.Text = "Adet";
            // 
            // lblPaymentBookToplam
            // 
            this.lblPaymentBookToplam.AutoSize = true;
            this.lblPaymentBookToplam.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentBookToplam.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentBookToplam.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentBookToplam.Location = new System.Drawing.Point(366, 63);
            this.lblPaymentBookToplam.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentBookToplam.Name = "lblPaymentBookToplam";
            this.lblPaymentBookToplam.Size = new System.Drawing.Size(96, 20);
            this.lblPaymentBookToplam.TabIndex = 2;
            this.lblPaymentBookToplam.Text = "Toplam Fiyat";
            // 
            // lblPaymentBookCount
            // 
            this.lblPaymentBookCount.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentBookCount.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentBookCount.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentBookCount.Location = new System.Drawing.Point(155, 86);
            this.lblPaymentBookCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentBookCount.Name = "lblPaymentBookCount";
            this.lblPaymentBookCount.Size = new System.Drawing.Size(36, 30);
            this.lblPaymentBookCount.TabIndex = 3;
            this.lblPaymentBookCount.Text = "0";
            this.lblPaymentBookCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblPaymentBookBirim
            // 
            this.lblPaymentBookBirim.AutoSize = true;
            this.lblPaymentBookBirim.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentBookBirim.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentBookBirim.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentBookBirim.Location = new System.Drawing.Point(257, 63);
            this.lblPaymentBookBirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentBookBirim.Name = "lblPaymentBookBirim";
            this.lblPaymentBookBirim.Size = new System.Drawing.Size(86, 20);
            this.lblPaymentBookBirim.TabIndex = 4;
            this.lblPaymentBookBirim.Text = "Birim Fiyatı";
            // 
            // lblPaymentBookToplamFiyat
            // 
            this.lblPaymentBookToplamFiyat.AutoSize = true;
            this.lblPaymentBookToplamFiyat.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentBookToplamFiyat.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentBookToplamFiyat.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentBookToplamFiyat.Location = new System.Drawing.Point(404, 93);
            this.lblPaymentBookToplamFiyat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentBookToplamFiyat.Name = "lblPaymentBookToplamFiyat";
            this.lblPaymentBookToplamFiyat.Size = new System.Drawing.Size(17, 20);
            this.lblPaymentBookToplamFiyat.TabIndex = 5;
            this.lblPaymentBookToplamFiyat.Text = "0";
            // 
            // lblPaymentBookBirimFiyat
            // 
            this.lblPaymentBookBirimFiyat.AutoSize = true;
            this.lblPaymentBookBirimFiyat.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentBookBirimFiyat.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentBookBirimFiyat.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentBookBirimFiyat.Location = new System.Drawing.Point(289, 93);
            this.lblPaymentBookBirimFiyat.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentBookBirimFiyat.Name = "lblPaymentBookBirimFiyat";
            this.lblPaymentBookBirimFiyat.Size = new System.Drawing.Size(17, 20);
            this.lblPaymentBookBirimFiyat.TabIndex = 6;
            this.lblPaymentBookBirimFiyat.Text = "0";
            // 
            // lblPaymentBookName
            // 
            this.lblPaymentBookName.AutoEllipsis = true;
            this.lblPaymentBookName.BackColor = System.Drawing.Color.Transparent;
            this.lblPaymentBookName.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.lblPaymentBookName.ForeColor = System.Drawing.Color.Black;
            this.lblPaymentBookName.Location = new System.Drawing.Point(121, 0);
            this.lblPaymentBookName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.lblPaymentBookName.Name = "lblPaymentBookName";
            this.lblPaymentBookName.Size = new System.Drawing.Size(383, 63);
            this.lblPaymentBookName.TabIndex = 10;
            this.lblPaymentBookName.Text = "Kitap";
            this.lblPaymentBookName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // PBPaymentBook
            // 
            this.PBPaymentBook.ErrorImage = null;
            this.PBPaymentBook.Location = new System.Drawing.Point(16, 16);
            this.PBPaymentBook.Margin = new System.Windows.Forms.Padding(2);
            this.PBPaymentBook.Name = "PBPaymentBook";
            this.PBPaymentBook.Size = new System.Drawing.Size(92, 100);
            this.PBPaymentBook.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBPaymentBook.TabIndex = 0;
            this.PBPaymentBook.TabStop = false;
            // 
            // UC_PaymentItem_Book
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Bisque;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.lblPaymentBookName);
            this.Controls.Add(this.lblPaymentBookBirimFiyat);
            this.Controls.Add(this.lblPaymentBookToplamFiyat);
            this.Controls.Add(this.lblPaymentBookBirim);
            this.Controls.Add(this.lblPaymentBookCount);
            this.Controls.Add(this.lblPaymentBookToplam);
            this.Controls.Add(this.lblPaymetBookAdet);
            this.Controls.Add(this.PBPaymentBook);
            this.ForeColor = System.Drawing.Color.Black;
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "UC_PaymentItem_Book";
            this.Size = new System.Drawing.Size(506, 132);
            ((System.ComponentModel.ISupportInitialize)(this.PBPaymentBook)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBPaymentBook;
        private System.Windows.Forms.Label lblPaymetBookAdet;
        private System.Windows.Forms.Label lblPaymentBookToplam;
        private System.Windows.Forms.Label lblPaymentBookCount;
        private System.Windows.Forms.Label lblPaymentBookBirim;
        private System.Windows.Forms.Label lblPaymentBookToplamFiyat;
        private System.Windows.Forms.Label lblPaymentBookBirimFiyat;
        private System.Windows.Forms.Label lblPaymentBookName;
    }
}
