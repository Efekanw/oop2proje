﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
/**
 * @author : Efekan Sargın 
 * @number : 152120181049
 * @mail : efekansarginn@hotmail.com
 * @date : 24.05.2021
 * @brief : This class includes Database's functions
 */
namespace OOP2Proje
{
    class DataBase
    {
        private string connectstr = "server=DESKTOP-R7E1LGS\\SQLEXPRESS;database=SigmaKitabeviDB;User Id=sa;Password=123";
        public static SqlConnection conn;
        private SqlCommand cmd;
        private SqlDataReader reader;
        List<Product> listofBooks = new List<Product>();
        List<Product> listofMagazines = new List<Product>();
        List<Product> listofMusicCDs = new List<Product>();
        List<Product> bestSellers = new List<Product>();
        List<Customer> listofCustomers = new List<Customer>();
        List<ShoppingCart> listofshoppingcarts = new List<ShoppingCart>();
        List<ItemToPurchase> listofItemtopurchase = new List<ItemToPurchase>();
        /// <summary>
        /// Connects to Database
        /// </summary>
        public void connectToDB()
        {
            conn = new SqlConnection(connectstr);
            conn.Open();
        }
        /// <summary>
        /// Disconnects to Database
        /// </summary>
        public void disconnectToDB()
        {
            conn.Close();
        }
        /// <summary>
        /// Gets List of products for inital store screen
        /// </summary>
        /// <returns>Returns bestsellers of store</returns>
        public List<Product> initialScreen()
        {
            try
            {
                bestSellers.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT TOP 6 * FROM books ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Book productBook = new Book();
                                readBookData(ref productBook);
                                bestSellers.Add(productBook);
                            }
                        }
                        cmd.CommandText = "SELECT TOP 4 * FROM magazines ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Magazine productMag = new Magazine();
                                readMagazineData(ref productMag);
                                bestSellers.Add(productMag);
                            }
                        }
                        cmd.CommandText = "SELECT TOP 2 * FROM musicCD ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                MusicCD productMusic = new MusicCD();
                                readMusicCDData(ref productMusic);
                                bestSellers.Add(productMusic);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return bestSellers;
        }
        /// <summary>
        /// Fills book list from Database
        /// </summary>
        public void setListBooksFromDB()
        {
            try
            {
                listofBooks.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM books ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Book book = new Book();
                                readBookData(ref book);
                                listofBooks.Add(book);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Fills musicCD list from Database
        /// </summary>
        public void setListMusicCDFromDB()
        {
            try
            {
                listofMusicCDs.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM musicCD ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                MusicCD music = new MusicCD();
                                readMusicCDData(ref music);
                                listofMusicCDs.Add(music);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Fills magazines list from Database
        /// </summary>
        public void setListofMagazinesFromDB()
        {
            try
            {
                listofMagazines.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM magazines ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Magazine magazine = new Magazine();
                                readMagazineData(ref magazine);
                                listofMagazines.Add(magazine);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Returns book list
        /// </summary>
        /// <returns>Book list</returns>
        public List<Product> getListofBooks()
        {
            return listofBooks;
        }
        /// <summary>
        /// Returns musicCD list
        /// </summary>
        /// <returns>MuiscCD list</returns>
        public List<Product> getListofMusicCD()
        {
            return listofMusicCDs;
        }
        /// <summary>
        /// Returns magazines list
        /// </summary>
        /// <returns>Magazine list</returns>
        public List<Product> getListofMagazines()
        {
            return listofMagazines;
        }
        /// <summary>
        /// Fills book list from database according to book type
        /// </summary>
        /// <param name="type">Book type</param>
        public void setBooksByType(string type)
        {
            try
            {
                listofBooks.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM books WHERE type = @type";
                        cmd.Parameters.Add("@type", SqlDbType.NVarChar).Value = type;
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Book book = new Book();
                                readBookData(ref book);
                                listofBooks.Add(book);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Fills musicCD list from database according to musicCD type
        /// </summary>
        /// <param name="type">Music Type</param>
        public void setMusicByType(string type)
        {
            try
            {
                listofMusicCDs.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM musicCD WHERE type = @type";
                        cmd.Parameters.Add("@type", SqlDbType.NVarChar).Value = type;
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                MusicCD music = new MusicCD();
                                readMusicCDData(ref music);
                                listofMusicCDs.Add(music);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Fills magazine list from database according to magazine type
        /// </summary>
        /// <param name="type">Magazine type</param>
        public void setMagazinesByType(string type)
        {
            try
            {
                listofMagazines.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM magazines  WHERE type = @type";
                        cmd.Parameters.Add("@type", SqlDbType.NVarChar).Value = type;
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Magazine magazine = new Magazine();
                                readMagazineData(ref magazine);
                                listofMagazines.Add(magazine);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Reads book's data from Database
        /// </summary>
        /// <param name="book">ref Book</param>
        private void readBookData(ref Book book)
        {
            book.Id = reader.GetInt32(0);
            book.Name = reader.GetString(1);
            book.Price = (double)reader.GetDouble(2);
            book.Isbn = reader.GetString(3);
            book.Author = reader.GetString(4);
            book.Publisher = reader.GetString(5);
            book.Page = reader.GetInt32(6);
            book.Image = (byte[])reader["image"];

        }
        /// <summary>
        /// Reads magazine's data from Database
        /// </summary>
        /// <param name="magazine">ref Magazine</param>
        private void readMagazineData(ref Magazine magazine)
        {
            magazine.Id = reader.GetInt32(0);
            magazine.Name = reader.GetString(1);
            magazine.Price = reader.GetDouble(2);
            magazine.Issue = reader.GetInt32(3);
            magazine.Type = reader.GetString(4);
            magazine.Image = (byte[])reader["image"];
        }
        /// <summary>
        /// Reads musicCD's data from Database
        /// </summary>
        /// <param name="music">ref Music</param>
        private void readMusicCDData(ref MusicCD music)
        {
            music.Id = reader.GetInt32(0);
            music.Name = reader.GetString(1);
            music.Price = reader.GetDouble(2);
            music.Singer = reader.GetString(3);
            music.Image = (byte[])reader["image"];
        }
        /// <summary>
        /// Saves customers into Database
        /// </summary>
        /// <param name="customer">Customer object</param>
        public void saveCustomerToDB(Customer customer)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO customers (customerid,name,address,email,username,password) VALUES (@id,@name,@address,@email,@username,@password)";
                        cmd.Parameters.AddWithValue("@id", customer.CustomerID);
                        cmd.Parameters.AddWithValue("@name", customer.Name);
                        cmd.Parameters.AddWithValue("@address", customer.Address);
                        cmd.Parameters.AddWithValue("@email", customer.Email);
                        cmd.Parameters.AddWithValue("@username", customer.Username);
                        cmd.Parameters.AddWithValue("@password", customer.Password);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Reads customers datas from Database and return list of customers
        /// </summary>
        /// <returns>Customer List</returns>
        public List<Customer> getListOfCustomerFromDB()
        {
            try
            {
                listofCustomers.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM customers ORDER BY customerid";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                Customer customer = new Customer();
                                customer.CustomerID = reader.GetInt32(0);
                                customer.Name = reader.GetString(1);
                                customer.Address = reader.GetString(2);
                                customer.Email = reader.GetString(3);
                                customer.Username = reader.GetString(4);
                                customer.Password = reader.GetString(5);
                                listofCustomers.Add(customer);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return listofCustomers;
        }
        /// <summary>
        /// Insert item into Cart table in Database
        /// </summary>
        /// <param name="item">Takes item</param>
        public void addproductToCartDB(ItemToPurchase item)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO products (cartid,producttype,productname,productimage,productprice,amount) VALUES (@cartid,@producttype,@productname,@productimage,@productprice,@amount)";
                        cmd.Parameters.AddWithValue("@cartid", item.Cartid);
                        cmd.Parameters.AddWithValue("@producttype", item.ProductType);
                        cmd.Parameters.AddWithValue("@productname", item.Product.Name);
                        cmd.Parameters.AddWithValue("@productimage", item.Product.Image);
                        cmd.Parameters.AddWithValue("@productprice", item.Product.Price);
                        cmd.Parameters.AddWithValue("@amount", item.Quantity);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Gets items in the matching shopping cart id from Database
        /// </summary>
        /// <param name="cartid">Shopping cart's id</param>
        /// <returns>List of items in shopping cart</returns>
        public List<ItemToPurchase> getItemsInCart(int cartid)
        {
            try
            {
                listofItemtopurchase.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM products WHERE cartid=@cartid";
                        cmd.Parameters.Add("@cartid", SqlDbType.Int).Value = cartid;
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ItemToPurchase item = new ItemToPurchase();
                                item.Cartid = reader.GetInt32(0);
                                if (reader.GetString(1) == "Book")
                                {
                                    item.ProductType = "Book";
                                    item.Product = new Book();
                                }
                                else if (reader.GetString(1) == "Magazine")
                                {
                                    item.ProductType = "Magazine";
                                    item.Product = new Magazine();
                                }
                                else if (reader.GetString(1) == "MusicCD")
                                {
                                    item.ProductType = "MusicCD";
                                    item.Product = new MusicCD();
                                }
                                item.Product.Name = reader.GetString(2);
                                item.Product.Image = (byte[])reader["productimage"];
                                item.Product.Price = reader.GetDouble(4);
                                item.Quantity = reader.GetInt32(5);
                                listofItemtopurchase.Add(item);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return listofItemtopurchase;
        }
        /// <summary>
        /// Update item which in function parametres in Database
        /// </summary>
        /// <param name="item">Takes item</param>
        public void updateItemInDB(ItemToPurchase item)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE products set amount=@amount where productname=@name";
                        cmd.Parameters.AddWithValue("@amount", item.Quantity);
                        cmd.Parameters.AddWithValue("@name", item.Product.Name);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Gets shopping cart from Database according to cart id
        /// </summary>
        /// <param name="cartid">Shopping cart's id</param>
        /// <returns>Shopping cart</returns>
        public ShoppingCart getShoppingCartFromDB(int cartid)
        {
            ShoppingCart cart = new ShoppingCart();
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM carts WHERE id=@id";
                        cmd.Parameters.AddWithValue("@id", cartid);
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                cart._cartid = reader.GetInt32(0);
                                cart._cst_id = reader.GetInt32(1);
                                cart._amount = reader.GetInt32(2);
                                cart._price = reader.GetDouble(3);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return cart;
        }
        /// <summary>
        /// Updates shopping cart in Database
        /// </summary>
        /// <param name="cart">Takes Shopping cart object</param>
        public void updateShoppingCart(ShoppingCart cart)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE carts set productamount=@productamount,totalprice=@totalprice where id=@id";
                        cmd.Parameters.AddWithValue("@productamount", cart._amount);
                        cmd.Parameters.AddWithValue("@totalprice", cart._price);
                        cmd.Parameters.AddWithValue("@id", cart._cartid);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Returns total price of shopping cart which equals to cartid
        /// </summary>
        /// <param name="cartid">Shopping cart' id</param>
        /// <returns>Total price of shopping cart</returns>
        public double getTotPriceOfCart(int cartid)
        {
            double totPrice = 0;
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM  carts  where id=@id";
                        cmd.Parameters.AddWithValue("@id", cartid);
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                totPrice = reader.GetDouble(3);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return totPrice;
        }
        /// <summary>
        /// Delete item which in function parameter in Database
        /// </summary>
        /// <param name="item">Takes item</param>
        public void deleteItemInDB(ItemToPurchase item)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "DELETE FROM products where productname=@name";
                        cmd.Parameters.AddWithValue("@name", item.Product.Name);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Returns shopping cart status
        /// </summary>
        /// <returns>Status of shopping cart</returns>
        public bool getCartStatus()
        {
            bool status = false;
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM  carts  where id=@id";
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = mainStore.cartid;
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                status = (bool)reader["cartstatus"];
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return status;
        }
        /// <summary>
        /// Gets shopping carts according to customer id and carts' status
        /// </summary>
        /// <param name="status">Status of shopping carts</param>
        /// <returns>List of shopping cart</returns>
        public List<ShoppingCart> getCartsPaymentFromDB(int status, int customerid)
        {
            try
            {
                listofshoppingcarts.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM carts WHERE (customerid=@customerid AND cartstatus=@cartstatus )";
                        cmd.Parameters.AddWithValue("@cartstatus", status);
                        cmd.Parameters.AddWithValue("@customerid", customerid);
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ShoppingCart cart = new ShoppingCart();
                                cart._cartid = reader.GetInt32(0);
                                cart._cst_id = reader.GetInt32(1);
                                if (!reader.IsDBNull(2))
                                {
                                    cart._amount = reader.GetInt32(2);

                                }
                                if (!reader.IsDBNull(3))
                                {
                                    cart._price = reader.GetDouble(3);
                                }
                                listofshoppingcarts.Add(cart);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return listofshoppingcarts;
        }
        /// <summary>
        /// Update shopping cart status. If payment has been made , the cart status will 1(true)
        /// </summary>
        public void updateCartStatus()
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "UPDATE carts set cartstatus=@cartstatus where id=@id";
                        cmd.Parameters.AddWithValue("@id", mainStore.cartid);
                        cmd.Parameters.AddWithValue("@cartstatus", 1);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
        /// <summary>
        /// Gets all carts from database 
        /// </summary>
        /// <returns>List of shopping  cart</returns>
        public List<ShoppingCart> getAllCarts()
        {
            try
            {
                listofshoppingcarts.Clear();
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM carts ORDER BY id";
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                ShoppingCart cart = new ShoppingCart();                      
                                cart._cartid = reader.GetInt32(0);
                                cart._cst_id = reader.GetInt32(1);
                                if (!reader.IsDBNull(2))
                                {
                                    cart._amount = reader.GetInt32(2);

                                }
                                if (!reader.IsDBNull(3))
                                {
                                    cart._price = reader.GetDouble(3);
                                }                       
                                listofshoppingcarts.Add(cart);
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return listofshoppingcarts;
        }
        /// <summary>
        /// Checks if the cart is already in the shopping cart
        /// </summary>
        /// <param name="id">Shopping cart's id</param>
        /// <returns>Validity of shopping cart id</returns>
        public bool checkCartid(int id)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "SELECT * FROM carts WHERE id=@id";
                        cmd.Parameters.AddWithValue("@id", id);
                        using (reader = cmd.ExecuteReader())
                        {
                            while (reader.Read())
                            {
                                return true;
                            }
                        }
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
            return false;
        }
        /// <summary>
        /// Adds shopping cart to Database
        /// </summary>
        /// <param name="cart">Shopping cart object</param>
        public void addtoCartDB(ShoppingCart cart)
        {
            try
            {
                connectToDB();
                using (conn)
                {
                    using (cmd = conn.CreateCommand())
                    {
                        cmd.CommandText = "INSERT INTO carts (id,cartstatus,customerid) VALUES (@id,@cartstatus,@customerid)";
                        cmd.Parameters.AddWithValue("@id", cart._cartid);
                        cmd.Parameters.AddWithValue("@cartstatus", 0);
                        cmd.Parameters.AddWithValue("@customerid", cart._cst_id);
                        cmd.ExecuteNonQuery();
                    }
                }
                disconnectToDB();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
