﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
/**
*  @author  : Alim ARSLAN
*  @number  : 152120181010
*  @mail    : alimarslan003@gmail.com
*  @date    : 29.05.2021
*  @brief   : This class includes PaidItem's features
*/
namespace OOP2Proje
{
    public partial class UC_PaidItem : UserControl
    {
        ItemToPurchase item = new ItemToPurchase();
        /// <summary>
        /// Constructor which shows items' features
        /// </summary>
        /// <param name="item">Takes item</param>
        public UC_PaidItem(ItemToPurchase item)
        {
            InitializeComponent();
            this.item = item;
            LblPaidCCount.Text = this.item.Quantity.ToString();
            string temp = String.Format("{0:0.00}", this.item.Product.Price).ToString();
            LblBookCbirim.Text = temp + " ₺";
            string tempx = String.Format("{0:0.00}", (this.item.Product.Price * this.item.Quantity)).ToString();
            LblPaidCTot.Text = tempx + " ₺";
            System.IO.MemoryStream ms = new System.IO.MemoryStream(this.item.Product.Image);
            PBPaidItemC.Image = System.Drawing.Image.FromStream(ms);
        }
    }
}
