﻿
namespace OOP2Proje
{
    partial class UC_ShoppingItem_Magazine
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LblMagazineCamo = new System.Windows.Forms.Label();
            this.LblMagazineCPrice = new System.Windows.Forms.Label();
            this.LblMagazineCCount = new System.Windows.Forms.Label();
            this.LblMagazineCUP = new System.Windows.Forms.Label();
            this.LblMagazineCTot = new System.Windows.Forms.Label();
            this.LblMagazineCbirim = new System.Windows.Forms.Label();
            this.BtnMagazineCAdd = new System.Windows.Forms.Button();
            this.BtnMagazineCRemove = new System.Windows.Forms.Button();
            this.LblCartMagazineName = new System.Windows.Forms.Label();
            this.BtnMagazineCDelete = new System.Windows.Forms.Button();
            this.PBMagazineC = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PBMagazineC)).BeginInit();
            this.SuspendLayout();
            // 
            // LblMagazineCamo
            // 
            this.LblMagazineCamo.AutoSize = true;
            this.LblMagazineCamo.BackColor = System.Drawing.Color.Transparent;
            this.LblMagazineCamo.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagazineCamo.Location = new System.Drawing.Point(155, 63);
            this.LblMagazineCamo.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMagazineCamo.Name = "LblMagazineCamo";
            this.LblMagazineCamo.Size = new System.Drawing.Size(41, 20);
            this.LblMagazineCamo.TabIndex = 12;
            this.LblMagazineCamo.Text = "Adet";
            // 
            // LblMagazineCPrice
            // 
            this.LblMagazineCPrice.AutoSize = true;
            this.LblMagazineCPrice.BackColor = System.Drawing.Color.Transparent;
            this.LblMagazineCPrice.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagazineCPrice.Location = new System.Drawing.Point(366, 63);
            this.LblMagazineCPrice.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMagazineCPrice.Name = "LblMagazineCPrice";
            this.LblMagazineCPrice.Size = new System.Drawing.Size(96, 20);
            this.LblMagazineCPrice.TabIndex = 13;
            this.LblMagazineCPrice.Text = "Toplam Fiyat";
            // 
            // LblMagazineCCount
            // 
            this.LblMagazineCCount.BackColor = System.Drawing.Color.Transparent;
            this.LblMagazineCCount.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagazineCCount.Location = new System.Drawing.Point(155, 86);
            this.LblMagazineCCount.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMagazineCCount.Name = "LblMagazineCCount";
            this.LblMagazineCCount.Size = new System.Drawing.Size(41, 30);
            this.LblMagazineCCount.TabIndex = 14;
            this.LblMagazineCCount.Text = "0";
            this.LblMagazineCCount.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMagazineCUP
            // 
            this.LblMagazineCUP.AutoSize = true;
            this.LblMagazineCUP.BackColor = System.Drawing.Color.Transparent;
            this.LblMagazineCUP.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagazineCUP.Location = new System.Drawing.Point(257, 63);
            this.LblMagazineCUP.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMagazineCUP.Name = "LblMagazineCUP";
            this.LblMagazineCUP.Size = new System.Drawing.Size(86, 20);
            this.LblMagazineCUP.TabIndex = 15;
            this.LblMagazineCUP.Text = "Birim Fiyatı";
            // 
            // LblMagazineCTot
            // 
            this.LblMagazineCTot.BackColor = System.Drawing.Color.Transparent;
            this.LblMagazineCTot.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagazineCTot.Location = new System.Drawing.Point(370, 93);
            this.LblMagazineCTot.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMagazineCTot.Name = "LblMagazineCTot";
            this.LblMagazineCTot.Size = new System.Drawing.Size(92, 20);
            this.LblMagazineCTot.TabIndex = 16;
            this.LblMagazineCTot.Text = "0";
            this.LblMagazineCTot.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // LblMagazineCbirim
            // 
            this.LblMagazineCbirim.BackColor = System.Drawing.Color.Transparent;
            this.LblMagazineCbirim.Font = new System.Drawing.Font("Palatino Linotype", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblMagazineCbirim.Location = new System.Drawing.Point(261, 93);
            this.LblMagazineCbirim.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblMagazineCbirim.Name = "LblMagazineCbirim";
            this.LblMagazineCbirim.Size = new System.Drawing.Size(82, 20);
            this.LblMagazineCbirim.TabIndex = 17;
            this.LblMagazineCbirim.Text = "0";
            this.LblMagazineCbirim.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnMagazineCAdd
            // 
            this.BtnMagazineCAdd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMagazineCAdd.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagazineCAdd.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(192)))), ((int)(((byte)(0)))));
            this.BtnMagazineCAdd.Location = new System.Drawing.Point(195, 86);
            this.BtnMagazineCAdd.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMagazineCAdd.Name = "BtnMagazineCAdd";
            this.BtnMagazineCAdd.Size = new System.Drawing.Size(30, 30);
            this.BtnMagazineCAdd.TabIndex = 18;
            this.BtnMagazineCAdd.Text = "+";
            this.BtnMagazineCAdd.UseVisualStyleBackColor = true;
            this.BtnMagazineCAdd.Click += new System.EventHandler(this.BtnMagazineCAdd_Click);
            // 
            // BtnMagazineCRemove
            // 
            this.BtnMagazineCRemove.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMagazineCRemove.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.BtnMagazineCRemove.ForeColor = System.Drawing.Color.Red;
            this.BtnMagazineCRemove.Location = new System.Drawing.Point(121, 86);
            this.BtnMagazineCRemove.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMagazineCRemove.Name = "BtnMagazineCRemove";
            this.BtnMagazineCRemove.Size = new System.Drawing.Size(30, 30);
            this.BtnMagazineCRemove.TabIndex = 19;
            this.BtnMagazineCRemove.Text = "-";
            this.BtnMagazineCRemove.UseVisualStyleBackColor = true;
            this.BtnMagazineCRemove.Click += new System.EventHandler(this.BtnMagazineCRemove_Click);
            // 
            // LblCartMagazineName
            // 
            this.LblCartMagazineName.AutoEllipsis = true;
            this.LblCartMagazineName.BackColor = System.Drawing.Color.Transparent;
            this.LblCartMagazineName.Font = new System.Drawing.Font("Palatino Linotype", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.LblCartMagazineName.Location = new System.Drawing.Point(121, 0);
            this.LblCartMagazineName.Margin = new System.Windows.Forms.Padding(2, 0, 2, 0);
            this.LblCartMagazineName.Name = "LblCartMagazineName";
            this.LblCartMagazineName.Size = new System.Drawing.Size(383, 63);
            this.LblCartMagazineName.TabIndex = 21;
            this.LblCartMagazineName.Text = "Dergi";
            this.LblCartMagazineName.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // BtnMagazineCDelete
            // 
            this.BtnMagazineCDelete.BackgroundImage = global::OOP2Proje.Properties.Resources.trash_magazine;
            this.BtnMagazineCDelete.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.BtnMagazineCDelete.Cursor = System.Windows.Forms.Cursors.Hand;
            this.BtnMagazineCDelete.FlatAppearance.BorderSize = 0;
            this.BtnMagazineCDelete.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.BtnMagazineCDelete.Location = new System.Drawing.Point(474, 98);
            this.BtnMagazineCDelete.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.BtnMagazineCDelete.Name = "BtnMagazineCDelete";
            this.BtnMagazineCDelete.Size = new System.Drawing.Size(30, 32);
            this.BtnMagazineCDelete.TabIndex = 20;
            this.BtnMagazineCDelete.UseVisualStyleBackColor = true;
            this.BtnMagazineCDelete.Click += new System.EventHandler(this.BtnMagazineCDelete_Click);
            // 
            // PBMagazineC
            // 
            this.PBMagazineC.ErrorImage = null;
            this.PBMagazineC.Location = new System.Drawing.Point(16, 16);
            this.PBMagazineC.Margin = new System.Windows.Forms.Padding(2, 2, 2, 2);
            this.PBMagazineC.Name = "PBMagazineC";
            this.PBMagazineC.Size = new System.Drawing.Size(92, 100);
            this.PBMagazineC.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PBMagazineC.TabIndex = 11;
            this.PBMagazineC.TabStop = false;
            // 
            // UC_ShoppingItem_Magazine
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.MistyRose;
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.LblCartMagazineName);
            this.Controls.Add(this.PBMagazineC);
            this.Controls.Add(this.BtnMagazineCDelete);
            this.Controls.Add(this.LblMagazineCamo);
            this.Controls.Add(this.BtnMagazineCRemove);
            this.Controls.Add(this.LblMagazineCPrice);
            this.Controls.Add(this.BtnMagazineCAdd);
            this.Controls.Add(this.LblMagazineCCount);
            this.Controls.Add(this.LblMagazineCbirim);
            this.Controls.Add(this.LblMagazineCUP);
            this.Controls.Add(this.LblMagazineCTot);
            this.Name = "UC_ShoppingItem_Magazine";
            this.Size = new System.Drawing.Size(504, 130);
            ((System.ComponentModel.ISupportInitialize)(this.PBMagazineC)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox PBMagazineC;
        private System.Windows.Forms.Label LblMagazineCamo;
        private System.Windows.Forms.Label LblMagazineCPrice;
        private System.Windows.Forms.Label LblMagazineCCount;
        private System.Windows.Forms.Label LblMagazineCUP;
        private System.Windows.Forms.Label LblMagazineCTot;
        private System.Windows.Forms.Label LblMagazineCbirim;
        private System.Windows.Forms.Button BtnMagazineCAdd;
        private System.Windows.Forms.Button BtnMagazineCRemove;
        private System.Windows.Forms.Button BtnMagazineCDelete;
        private System.Windows.Forms.Label LblCartMagazineName;
    }
}
